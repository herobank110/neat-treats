# Neat Treats

Online cake order system for a Cardiff based cake bakery.

## Prerequisites

* [Windows OS](https://www.microsoft.com/en-gb/windows) (version: either 7, 8,
8.1, 10, 2012, 2012r2 or 2016; either 32-bit or 64-bit)

Needed for installing XAMPP and running the setup batch script.

* [Visual C++ Runtime](https://support.microsoft.com/en-gb/help/2977003/the-latest-supported-visual-c-downloads)
(version: either 2017 or 2019; either 32-bit or 64-bit depending on OS)

Needed for executing PHP code.

* [XAMPP](https://www.apachefriends.org/download.html) (version: 7.3.13,
modules: Apache, PHP, MySQL, Fake Sendmail)

Needed for self-hosting the website, executing PHP code, managing a MySQL
(MariaDB) database and sending emails.

* [Chrome](https://www.google.com/intl/en_uk/chrome/) or
[Firefox](https://www.mozilla.org/en-US/firefox/new/), or other modern browser
(recommended: Chrome)

Needed for viewing and operating the HyperText markup interface.

## Installing my System

Please note that in the following instructions, the token `<xampp>` is used to
refer to your XAMPP installation directory. Please replace any instances with
your actual installation directory.

1. Download and unzip the included project files.

2. Move the unzipped project folder into the folder: `<xampp>\htdocs\`.

3. Execute the included `setup.bat` in the project folder to create the database.
Alternatively, read the Setup database manually section in the included System
Setup document.

4. To setup email service, set the following options in the following files. Also
note that some options may be commented out with a preceding `;` semicolon. If
this is the case, remove the semicolon so they are used.

File: `<xampp>\php\php.ini` Section: `[mail function]`
```ini
SMTP=smtp.gmail.com
smtp_port=587
sendmail_from=neattreats.sender@gmail.com
sendmail_path="<xampp>\sendmail\sendmail.exe -t"
```

File: `<xampp>\sendmail\sendmail.ini` Section: `[sendmail]`
```ini
smtp_server=smtp.gmail.com
smtp_port=587
smtp_ssl=tls
error_logfile=error.log
debug_logfile=debug.log
auth_username=neattreats.sender@gmail.com
auth_password=DOyhiDNN
```

## Running my System

1. Open XAMPP Control utility in your XAMPP installation folder:
(`<xampp>\xampp-control.exe`).

2. Start the following services: Apache, MySQL.

3. Open your browser and type into the address bar the following URL:
`http://localhost/<project>/Source` where `<project>` is the relative path of
the extracted project folder inside the `<xampp>\htdocs` folder from the htdocs
folder.

## Additional Notes

The above details as well as additional setup notes are available in the
accompanying System Setup document. The System Setup document also has the
table of errors with potential fixes to any errors.

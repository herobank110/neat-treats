@echo off
REM Set up for the Neat Treats website to be run once when first installed.
echo. [note] Running setup for Neat Treats website

REM Establish the project has been installed to the right directory.
set projectrel=%cd:*\htdocs\=%
if "%cd%" == "%projectrel%" (
  echo. [fatal] No htdocs folder! Consult the Neat Treats setup ^
document with the error code: 1
  pause
  exit /b 1
) else (
  echo. [note] Htdocs folder found!
  echo. [debug] projectrel = %projectrel%
)


REM Install the database and load initial data.

call :tohtdocs
cd ../mysql/bin

echo. [note] Connecting to database. This may take a moment...
REM This will connect to database server and echo ' [note] Connected OK'
mysql -u root --execute "select ' [note] Connected OK';" --batch --silent
if %errorlevel% GEQ 1 (
  echo. [fatal] Failed to connect to database. Consult the Neat Treats setup ^
document with the following error code: error 2
  pause
  exit \b 2
)

echo. [note] Setting up database with table structures
mysql -u root < ..\..\htdocs\%projectrel%\Source\assets\setup\setup_database.sql
if %errorlevel% GEQ 1 (
  echo. [fatal] Failed to set up database. Consult the Neat Treats setup ^
document with the following error code: error 3
  pause
  exit \b 3
)

echo. [note] Filling database with initial data
mysql -u root < ..\..\htdocs\%projectrel%\Source\assets\setup\example_data.sql
if %errorlevel% GEQ 1 (
  echo. [fatal] Failed to insert values into the database. Consult the Neat Treats setup ^
document with the following error code: error 4
  pause
  exit \b 4
)


echo. [note] Successfully set up Neat Treats website
REM Wait for user to enter something to close program.
pause



:: Go to the htdocs folder
:: eg, `call :tohtdocs`
:tohtdocs
call cd %%cd:\%projectrel%=%%
exit /b 0

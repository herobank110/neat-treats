"""
Snippets of HTML with formatting rules
to insert csv data into to build up a form.
"""

HTML_TEMPLATES = {
    "FIELD":
        "<!-- {label} -->\n"      \
        "<div>\n"                  \
        "{FIELD_ELEMENTS_LABEL}"    \
        "{FIELD_ELEMENTS_INPUT}"     \
        "{FIELD_ELEMENTS_HINT}"       \
        "{FIELD_ELEMENTS_ERROR}"       \
        "</div>\n",

    "FIELD_ELEMENTS":
    [
        {
        "suffix": "_LABEL",
        "template":
            "<label for=\"{field_name}_input\">{label}</label>\n",
        "requires": ["label"]

        # Available options: suffix, template, requires, default,
        # to_snake_case, to_pascal_case, to_camel_case, to_upper_case,
        # to_lower_case, to_title_case
        },

        {
        "suffix":"_INPUT",
        "template":
            "<input id=\"{field_name}_input\" name=\"{field_name}\" " \
            "type=\"{element_type}\" {is_optional} title=\"{tooltip}\" "\
            "pattern=\"{pattern}\">\n",
        "requires": [],
        "to_snake_case": ["is_optional"],
        "default": {"pattern": "*"}
        },

        {
        "suffix": "_HINT",
        "template": "<span>{hint_message}</span>\n",
        "requires": ["hint_message"],
        },

        {
        "suffix": "_ERROR",
        "template": "<span id=\"{field_name}_error\"></span>\n",
        "requires": ["field_name"],
        },
    ],
}

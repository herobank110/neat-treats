from mysql.connector import connect


class FancyPrinter(object):
    """
    Class with static methods for printing complex types like `dict` or `list`
    in fancy ways.

    All fancy string formatting functions have a param `output`. If set to None,
    the formatted contents will be returned, but not printed. If left at True,
    the contents will be printed.
    """

    @staticmethod
    def multi_pairs_table_from_dict(in_dict, col_sz=None, just=None, output=True):
        """
        Returns IN_DICT into pairs tables, where each key is the table name
        and each value is the table list. See table_from_pairs.
        """
        if output:
            return print(FancyPrinter.multi_pairs_table_from_dict(in_dict, col_sz=col_sz, just=just, output=False))

        return "\n\n".join(
            "{name} table:\n"\
            "{table}"\
            .format(name=name,
            table=FancyPrinter.table_from_pairs(table, col_sz=col_sz, just=just, output=False))
            for name, table in in_dict.items())

    @staticmethod
    def multi_rows_table_from_dict(in_dict, col_sz=None, just=None, output=True):
        """
        Returns IN_DICT into rows tables, where each key is the table name
        and each value is the table list (rows are dicts). See table_from_rows.
        """
        if output:
            return print(FancyPrinter.multi_rows_table_from_dict(in_dict, col_sz=col_sz, just=just, output=False))

        return "\n\n".join(
            "{name} table:\n"\
            "{table}"\
            .format(name=name,
            table=FancyPrinter.table_from_rows(table, col_sz=col_sz, just=just, output=False))
            for name, table in in_dict.items())

    @staticmethod
    def multi_dict_table_from_dict(in_dict, col_sz=None, just=None, output=True):
        """
        Returns IN_DICT into dict tables, where each key is the table name
        and each value is the table list (rows are dicts). See table_from_dict.
        """
        if output:
            return print(FancyPrinter.multi_dict_table_from_dict(in_dict, col_sz, just, output=False))

        return "\n\n".join(
            "{name} table:\n"\
            "{table}"\
            .format(name=name,
            table=FancyPrinter.table_from_dict(table, col_sz=col_sz, just=just, output=False))
            for name, table in in_dict.items())

    @staticmethod
    def table_from_pairs(in_list, col_sz=None, just=None, output=True, max_col=40):
        """
        Returns IN_LIST turned into tables using header-value pair tuples, with
        each column having COL_SZ width.

        Each item in IN_LIST is a row.
        Each row has an ordered series of paired tuples, where
        index 0 is the header name and index 1 is the value.

        Should be in this format:

        ```[
        [(header1, row1val1), (header2, row1val2) ...],
        [(header1, row2val1), (header2, row2val2) ...],
        ...
        ]```

        COL_SZ is static column lengths. Default will decide best size.
        JUST is one of str.center, str.ljust or str.rjust. Default is center.
        """
        if output:
            return print(FancyPrinter.table_from_pairs(in_list, col_sz=col_sz, just=just, output=False))

        if just is None:
            # Use central justify by default.
            just = str.center

        if col_sz is None:
            # Optional first pass - determine column sizes
            col_sz = [0 for column in in_list[0]]
            for i, row in enumerate(in_list):
                for j, col in enumerate(row):
                    # Add 2 space padding.
                    col_sz[j] = max(col_sz[j], *(len(str(val)) + 2 for val in col))
                    if col_sz[j] > max_col: col_sz[j] = max_col
            return FancyPrinter.table_from_pairs(in_list, col_sz=col_sz, just=just, output=output)
        else:
            try:
                col_sz[0]
            except TypeError:
                # A single value was given. Use constant values for each column.
                col_sz = [col_sz for column in in_list[0]]

        return " ┌{div_top}┐\n"\
               " │{headers}│\n"\
               " ├{divider}┤\n"\
               " │{content}│\n"\
               " └{div_bot}┘"\
            .format(
            headers="│".join([just(str(x[0])[:col_sz[i]], col_sz[i]) for i, x in enumerate(in_list[0])]),
            div_top="┬".join("─"*col_sz[i] for i in range(len(in_list[0]))),
            divider="┼".join("─"*col_sz[i] for i in range(len(in_list[0]))),
            div_bot="┴".join("─"*col_sz[i] for i in range(len(in_list[0]))),
            content="│\n │".join(
                "│".join([just(str(x[1])[:col_sz[i]], col_sz[i]) for i, x in enumerate(row)]
                ) for row in in_list))

    @staticmethod
    def table_from_rows(in_list, col_sz=None, just=None, output=True, max_col=40):
        """
        Returns IN_LIST turned into tables using each item as a row, with
        each column having COL_SZ width.

        IN_LIST is a 2d list where the indexes of each item is its place in the
        table. The first row is treated as a header row.

        Should be in this format:

        ```[
        [header1, header2, ... ],
        [row1val1, row1val2, ... ],
        [row2val1, row2val2, ... ]
        ...
        ]```

        COL_SZ is static column lengths. Default will decide best size.
        JUST is one of str.center, str.ljust or str.rjust. Default is center.
        """
        if output:
            return print(FancyPrinter.table_from_rows(in_list, col_sz=col_sz, just=just, output=False))

        if just is None:
            # Use central justify by default.
            just = str.center

        if col_sz is None:
            # Optional first pass - determine column sizes
            col_sz = [0 for column in in_list[0]]
            for i, row in enumerate(in_list):
                for j, col in enumerate(row):
                    if i == 0:
                        # Add 2 space padding.
                        col_sz[j] = max(col_sz[j], len(str(col)) + 2)
                        if col_sz[j] > max_col: col_sz[j] = max_col
                    else:
                        col_sz[j] = max(col_sz[j], len(str(col)) + 2)
                        if col_sz[j] > max_col: col_sz[j] = max_col
            return FancyPrinter.table_from_rows(in_list, col_sz=col_sz, just=just, output=output)
        else:
            try:
                col_sz[0]
            except TypeError:
                # A single value was given. Use constant values for each column.
                col_sz = [col_sz for column in in_list[0]]

        return "┌{div_top}┐\n"\
               "│{headers}│\n"\
               "├{divider}┤\n"\
               "│{content}│\n"\
               "└{div_bot}┘"\
            .format(
            headers="│".join([just(str(x)[:col_sz[i]], col_sz[i]) for i, x in enumerate(in_list[0])]),
            div_top="┬".join("─"*col_sz[i] for i in range(len(in_list[0]))),
            divider="┼".join("─"*col_sz[i] for i in range(len(in_list[0]))),
            div_bot="┴".join("─"*col_sz[i] for i in range(len(in_list[0]))),
            content="│\n│".join(
                "│".join([just(str(x)[:col_sz[i]], col_sz[i]) for i, x in enumerate(row)]
                ) for i, row in enumerate(in_list) if i > 0))

    @staticmethod
    def table_from_dict(in_list, col_sz=None, just=None, output=True, max_col=40):
        """
        Returns IN_LIST formatted into a table with each column having COL_SZ
        width.

        Each item in IN_LIST is a row.
        The first row has the heading names, in displayed order. It should be
        an ordered iterable, such as a list or tuple.
        All subsequent rows are dictionaries, where the keys are headings and
        values are values.

        Should be in this format:

        ```[
        [heading1, heading2, ... ],
        {heading1:row1val1, heading2:row1val2, ... },
        {heading1:row2val1, heading2:row2val2, ... },
        ...
        ]```

        COL_SZ is static column lengths. Default will decide best size.
        JUST is one of str.center, str.ljust or str.rjust. Default is center.
        """
        if output:
            return print(FancyPrinter.table_from_dict(in_list, col_sz=col_sz, just=just, output=False))

        if just not in (str.ljust, str.center, str.rjust):
            # Use central justify by default.
            just = str.center

        if col_sz is None:
            # Optional first pass - determine column sizes
            col_sz = [0 for column in in_list[0]]
            for i, row in enumerate(in_list):
                for j, col in enumerate(row):
                    if i == 0:
                        # Add 2 space padding.
                        col_sz[j] = max(col_sz[j], len(str(col)) + 2)
                        if col_sz[j] > max_col: col_sz[j] = max_col
                    else:
                        col_sz[j] = max(col_sz[j], len(str(row[col])) + 2)
                        if col_sz[j] > max_col: col_sz[j] = max_col
            return FancyPrinter.table_from_dict(in_list, col_sz=col_sz, just=just, output=output)
        else:
            try:
                col_sz[0]
            except TypeError:
                # A single value was given. Use constant values for each column.
                col_sz = [col_sz for column in in_list[0]]

        # Second pass - determine string to print
        return "┌{div_top}┐\n"\
               "│{headers}│\n"\
               "├{divider}┤\n"\
               "│{content}│\n"\
               "└{div_bot}┘"\
            .format(
            headers="│".join([just(str(x)[:col_sz[i]], col_sz[i]) for i, x in enumerate(in_list[0])]),
            div_top="┬".join("─"*col_sz[i] for i in range(len(in_list[0]))),
            divider="┼".join("─"*col_sz[i] for i in range(len(in_list[0]))),
            div_bot="┴".join("─"*col_sz[i] for i in range(len(in_list[0]))),
            content="│\n│".join(
                "│".join([just(str(row[x])[:col_sz[i]], col_sz[i]) for i, x in enumerate(in_list[0])]
                ) for i, row in enumerate(in_list) if i > 0))


def make_select_query(table_name):
    return "select * from %s" % table_name

db = connect(host="localhost", user="root")
a = db.cmd_query("use neattreats;")

while 1:
    inp = input(" Enter table name:\n  > ")
    if inp == "quit": break

    FancyPrinter.table_from_pairs(
        (

            lambda cols: tuple(
                map(
                    lambda row: tuple(
                        (col, str(val)[12:-2])
                        for col, val in zip(cols, row)
                    ),
                    db.get_rows()[0] or [[""]*len(cols)]
                )
            )
        )(
            tuple(
                map(
                    lambda x: x[0],
                    db.cmd_query(make_select_query(inp))["columns"]
                )
            )
        )
    )


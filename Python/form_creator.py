"""
Script to simplify making form elements in HTML.

Effective for <input> fields. Bespoke inputs
such as <button> or <select> should be written
manually.
"""

from collections import namedtuple
import csv, itertools

from html_templates import HTML_TEMPLATES
import case_swap

## Filename of csv file to load form elements from.
csv_path = "Neat Treats - Forms Elements - Customer Update.csv"

## Filename of json file to load html templates from.
templates_path = "html_templates.json"

## All possible column headings for csv file.
MAX_OPTIONS = {
    "label": "",
    "field_name": "",
    "is_optional": "",
    "element_type": "",
    "fieldset": "",
    "d_b_table": "",
    "d_b_field": "",
    "validation": "",
    "pattern": "",
    "error_message": "",
    "hint_message": "",
    "placeholder": "",
    "tooltip": "",
    }

def read_form_fields_csv(in_filename, out_rows):
    """
    Read the csv data from the file into dictionaries
    so data can be access easily.

    :param in_filename: (str) Name of csv file to read.

    :param out_rows: (list) List of row dictionaries
        with csv column headings as the keys.
    """

    with open(in_filename) as fp:
        # Construct iterator from csv to read each row in file.
        csv_iter = iter(csv.reader(fp))

        # Read column headings from first row.
        try:
            first_row = next(csv_iter)
        except StopIteration: return
        else:
            column_headings = [case_swap.to_snake(_) for _ in first_row]

        # Read data from all remaining rows.
        for lineno in itertools.count():

            # Get the next row in the file.
            try: row = next(csv_iter)

            # There are no more rows in the csv file.
            except StopIteration: return

            else:
                # Convert row data into a dictionary so it can
                # be accessed using column heading names.
                row_data = MAX_OPTIONS.copy()
                for i, it in enumerate(row):
                    row_data[column_headings[i]] = it

                # Add it to the list of rows.
                out_rows.append(row_data)

def format_row_for_element(in_row, element_data):
    """
    Format the row dictionary using rules from
    the element data.
    """
    # Replace any null fields with defaults.
    defaults = element_data.get("default")
    if defaults:
        for field in defaults:
            current_data = in_row.get(field)
            if not current_data:
                in_row[field] = defaults[field]

    # Set label to first non-empty value.
    first_non_empty_value = ""
    for value in in_row.values():
        if value:
            first_non_empty_value = value
            break
    if not in_row.get("label"):
        in_row["label"] = first_non_empty_value

    # Convert any required fields into snake case.
    to_snake_case = element_data.get("to_snake_case")
    if to_snake_case:
        for field in to_snake_case:
            current_data = in_row.get(field)
            if current_data:
                in_row[field] = case_swap.to_snake(current_data)

    # Convert any required fields into pascal case.
    to_pascal_case = element_data.get("to_pascal_case")
    if to_pascal_case:
        for field in to_pascal_case:
            current_data = in_row.get(field)
            if current_data:
                in_row[field] = case_swap.to_pascal(current_data)

    # Convert any required fields into camel case.
    to_camel_case = element_data.get("to_camel_case")
    if to_camel_case:
        for field in to_camel_case:
            current_data = in_row.get(field)
            if current_data:
                in_row[field] = case_swap.to_camel(current_data)

    # Convert any required fields into upper case.
    to_upper_case = element_data.get("to_upper_case")
    if to_upper_case:
        for field in to_upper_case:
            current_data = in_row.get(field)
            if current_data:
                in_row[field] = current_data.upper()

    # Convert any required fields into lower case.
    to_lower_case = element_data.get("to_lower_case")
    if to_lower_case:
        for field in to_lower_case:
            current_data = in_row.get(field)
            if current_data:
                in_row[field] = current_data.lower()

    # Convert any required fields into title case.
    to_title_case = element_data.get("to_title_case")
    if to_title_case:
        for field in to_title_case:
            current_data = in_row.get(field)
            if current_data:
                in_row[field] = current_data.title()

def construct_form_field(in_row):
    """
    Construct HTML elements to showcase a form
    field.

    :param in_row: (dict) Field attributes loaded
        from csv file.

    :return: (str) Formatted HTML form item.
    """
    # Dictionary of formatted substrings to go into the field template.
    field_substr = {}
    for element_data in HTML_TEMPLATES["FIELD_ELEMENTS"]:

        # Only show this element if data was provided
        # in the row.
        requirements = element_data.get("requires")
        should_show_element = not requirements \
            or all(in_row.get(_) for _ in requirements)

        # Format the text to put in this element.
        format_row_for_element(in_row, element_data)

        formatted_substr =  \
            "" if not should_show_element \
            else element_data["template"].format(**in_row)

        full_field_element_name = "FIELD_ELEMENTS" + element_data["suffix"]
        field_substr[full_field_element_name] = formatted_substr

    in_row.update(field_substr)
    field_html = HTML_TEMPLATES["FIELD"].format(**in_row)

    return field_html


def main():

    # Load the form fields data from the csv file.
    form_fields_data = []
    read_form_fields_csv(csv_path, form_fields_data)

    #print(form_fields_data)

    # Construct string for each.
    form_fields_html = [construct_form_field(row) for row in form_fields_data]

    # Concatenate entire string and print for usage.
    print("".join(form_fields_html))

if __name__ == "__main__":
    main()

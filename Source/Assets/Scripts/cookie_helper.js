/**
 * Get the value of a stored cookie.
 * @param {string} name Name of the cookie
 * @return {string} Value of cookie, or null if not found.
 */
function getCookie(name) {
  var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
  return v ? v[2] : null;
}

/**
 * Set or update the value of a cookie.
 * @param {string} name Name of the cookie.
 * @param {*} value Value to store in the cookie.
 * @param {number} days Days to hold the cookie for before expiring.
 */
function setCookie(name, value, days) {
  var d = new Date;
  d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
  document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
}

/**
 * Delete a cookie with the given name.
 * @param {string} name Name of stored cookie.
 */
function deleteCookie(name) { setCookie(name, '', -1); }

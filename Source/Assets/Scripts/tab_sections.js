function getSiblings(el, filter) {
	var siblings = [];
	el = el.parentNode.firstChild;
	do { if (!filter || filter(el)) siblings.push(el); } while (el = el.nextElementSibling);
	return siblings;
}

// example filter function
function exampleFilter(el) {
	return elem.nodeName.toLowerCase() == 'a';
}

function setVisibleTab(elemname) {

	var x = document.getElementById(elemname);

	// hide existing elements in group
	var sibs = getSiblings(x);
	for (let i = 1; i < sibs.length; i++) {
		sibs[i].style.display = "none";
	}

	// use preset tabs in group (for IE compatibility)
	//alert(tabGroup);
	for (let i = 0; i < tabGroup.length; i++) {
		document.getElementById(tabGroup[i]).style.display = "none";
	}


	// show new element
	x.style.display = "block";

	return false;
}

tabGroup = []

function addTabToGroup(tabName) {
	tabGroup.push(tabName);
}
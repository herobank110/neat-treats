/**
 * Contains functions to open and close a cancel-menu
 * 
 * Cancel Menus act as confirmation for a complex action which may
 * be impossible to reverse.
 */

/**
 * Show the cancel order prompt to let the user cancel an order.
 */
function showCancelMenu(menuID) {
    var cancelMenu = document.getElementById(menuID);
    if (cancelMenu == null) {
      // Cancel the cancellation.
      return;
    }
    cancelMenu.style.display = "block";
  }
  
  /**
   * Hide the cancel order prompt menu.
   */
  function hideCancelMenu(menuID) {
    var cancelMenu = document.getElementById(menuID);
    if (cancelMenu == null) {
      // Cancel the cancellation.
      return;
    }
    cancelMenu.style.display = "none";
  }
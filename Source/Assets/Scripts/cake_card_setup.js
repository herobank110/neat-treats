/**
 * Setup the cake cards to have a drop shadow effect
 * when hovered.
 */

/** All cake cards found on the page. */
const cakeCards = document.querySelectorAll(".cake-card");

cakeCards.forEach(card => {
    // Add the elevation shadow when hovered.
    card.addEventListener("mouseover", () => {
        // Default card elevation is z2
        card.classList.add("mdc-elevation--z3");
    })
    
    // Remove the elevation shadow when unhovered.
    card.addEventListener("mouseleave", () => {
        // Revert to default card elevation (z2)
        card.classList.remove("mdc-elevation--z3");
    })
})

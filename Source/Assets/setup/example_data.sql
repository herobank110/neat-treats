USE NeatTreats;

-- Auto increment IDs will be start from 1. Set manually to ensure compatibility.
-- In other words, if an ID is manually set to 0, it will be auto-incremented.

-- Ignore prototype stage demo items
/* INSERT INTO Ingredient (IngredientID, Name)
VALUES (1, 'Example Ingredient');

INSERT INTO IngredientCollection (IngredientCollectionID, IngredientID, Quantity)
VALUES (1, 1, 1);

INSERT INTO Cake (CakeID, Name, Price, IngredientCollectionID)
VALUES (4, 'Vanilla Cake', 12.34, 1);

INSERT INTO Cake (CakeID, Name, Price, IngredientCollectionID)
VALUES (9, 'Chocolate Cake', 8.58, 1); */

/* Software development section */

/**
 * Ingredients and cakes
 */

-- Add base ingredient types.
INSERT INTO Ingredient 
  (IngredientID, Name)
  VALUES
    (1, "Flour"),
    (2, "Sugar"),
    (3, "Butter"),
    (4, "Egg"),
    (5, "Milk"),
    (6, "Vanilla Extract"),
    (7, "Icing Sugar"),
    (8, "Cocoa"),
    (9, "Banana"),
    (10, "Apple"),
    (11, "Lemon"),
    (12, "Strawberry"),
    (13, "Banana Jam"),
    (14, "Apple Jam"),
    (15, "Lemon Jam"),
    (16, "Strawberry Jam"),
    (17, "Toffee"),
    (18, "Caramel"),
    (19, "Fudge"),
    (20, "Chocolate Sprinkles"),
    (21, "Red Food Colouring"),
    (22, "Yellow Food Colouring"),
    (23, "Blue Food Colouring");

-- Add the branch's inventory values.
INSERT INTO IngredientCollection
  (IngredientCollectionID, IngredientID, Quantity, MaxQuantity)
  VALUES
    (1, 1, 20.000, 20.000),
    (1, 2, 20.000, 20.000),
    (1, 3, 20.000, 20.000),
    (1, 4, 160.000, 160.000),
    (1, 5, 4.000, 4.000),
    (1, 6, 0.750, 0.750),
    (1, 7, 15.000, 15.000),
    (1, 8, 1.500, 1.500),
    (1, 9, 7.000, 7.000),
    (1, 10, 10.000, 10.000),
    (1, 11, 12.500, 12.500),
    (1, 12, 4.000, 4.000),
    (1, 13, 5.000, 5.000),
    (1, 14, 5.000, 5.000),
    (1, 15, 4.000, 4.000),
    (1, 16, 6.000, 6.000),
    (1, 17, 6.000, 6.000),
    (1, 18, 6.000, 6.000),
    (1, 19, 6.500, 6.500),
    (1, 20, 1.500, 1.500),
    (1, 21, 0.050, 0.050),
    (1, 22, 0.050, 0.050),
    (1, 23, 0.050, 0.050);

-- Add the cake recipes
INSERT INTO IngredientCollection
  (IngredientCollectionID, IngredientID, Quantity)
  VALUES
    (2, 1, 0.250),
    (2, 2, 0.250),
    (2, 3, 0.400),
    (2, 4, 4.000),
    (2, 5, 0.005),
    (2, 6, 0.004),
    (2, 16, 0.050),
    (2, 7, 0.010),
    (3, 1, 0.250),
    (3, 2, 0.250),
    (3, 3, 0.400),
    (3, 4, 4.000),
    (3, 5, 0.005),
    (3, 8, 0.100),
    (3, 20, 0.100),
    (3, 7, 0.010),
    (4, 1, 0.250),
    (4, 2, 0.250),
    (4, 7, 0.010),
    (4, 9, 0.550),
    (4, 13, 0.260),
    (4, 17, 0.120),
    (5, 1, 0.250),
    (5, 2, 0.250),
    (5, 3, 0.400),
    (5, 4, 4.000),
    (5, 5, 0.005),
    (5, 10, 1.250),
    (5, 6, 0.004),
    (5, 18, 0.120),
    (6, 1, 0.250),
    (6, 2, 0.250),
    (6, 7, 0.010),
    (6, 11, 0.950),
    (6, 19, 0.150),
    (6, 15, 0.260),
    (7, 1, 0.250),
    (7, 2, 0.250),
    (7, 3, 0.400),
    (7, 4, 4.000),
    (7, 5, 0.005),
    (7, 23, 0.006),
    (7, 22, 0.006),
    (7, 7, 0.022),
    (7, 16, 0.050),
    (7, 6, 0.004),
    (8, 1, 0.250),
    (8, 2, 0.250),
    (8, 7, 0.010),
    (8, 12, 0.650),
    (8, 16, 0.075),
    (8, 6, 0.005),
    (8, 21, 0.006),
    (9, 1, 0.250),
    (9, 2, 0.250),
    (9, 3, 0.400),
    (9, 4, 4.000),
    (9, 5, 0.005),
    (9, 20, 0.100),
    (9, 21, 0.006),
    (9, 22, 0.006),
    (9, 23, 0.006),
    (9, 6, 0.004);

-- Add the cake types
INSERT INTO Cake
  (CakeID, IngredientCollectionID, Price, Name, IsVegan)
  VALUES
    (1, 2, 9.90, "Vanilla Sponge", FALSE),
    (2, 3, 9.35, "Chocolate Sponge", FALSE),
    (3, 4, 12.60, "Banana Toffee Square", TRUE),
    (4, 5, 12.00, "Apple Caramel Square", FALSE),
    (5, 6, 12.75, "Lemon Fudge Square", TRUE),
    (6, 7, 15.95, "Football Scene", FALSE),
    (7, 7, 15.00, "Rugby Scene", FALSE),
    (8, 7, 15.70, "Cricket Scene", FALSE),
    (9, 8, 12.45, "Strawberry Surprise", TRUE),
    (10, 9, 19.25, "Colourful Surprise", FALSE);

/**
* Staff and company
*/

-- Add company roles.
INSERT INTO Role
  (RoleID, Name)
  VALUES
    (1, 'Manager'),
    (2, 'Baker'),
    (3, 'Cashier'),
    (4, 'root');

-- Add address of the branch.
INSERT INTO Address
  (AddressID, Line1, Line2, Postcode)
  VALUES
    (1, '30 Bakers Row', 'Cardiff', 'CF10 1AL');

-- Add the actual branch (there's only one)
INSERT INTO Branch
  (BranchID, AddressID, IngredientCollectionID, Name)
  VALUES
    (1, 1, 1, 'Cardiff Central');

-- Add staff personal details.
INSERT INTO Person
  (PersonID, Title, FirstName, SecondName)
  VALUES
    (1, 'mr', 'Tom', 'Smith'),
    (2, 'mr', 'Jordon', 'Shannon'),
    (3, 'miss', 'Gracie', 'Griffin'),
    (4, 'mrs', 'Helen', 'Herman'),
    (5, 'mr', 'Josh', 'Wilkins'),
    (6, 'mr', 'Admin', 'Administrator');

-- Add staff employee details
INSERT INTO Employee
  (EmployeeID, PersonID, RoleID, BranchID, StartDate, PassCode)
  VALUES
    (5637, 1, 1, 1, STR_TO_DATE('20/04/2018', '%d/%m/%Y'), 'PASSWORD'),
    (2284, 2, 2, 1, STR_TO_DATE('25/04/2018', '%d/%m/%Y'), 'PASSWORD'),
    (3231, 3, 2, 1, STR_TO_DATE('25/04/2019', '%d/%m/%Y'), 'PASSWORD'),
    (2374, 4, 2, 1, STR_TO_DATE('25/04/2020', '%d/%m/%Y'), 'PASSWORD'),
    (5995, 5, 3, 1, STR_TO_DATE('25/04/2020', '%d/%m/%Y'), 'PASSWORD'),
    (9245, 6, 4, 1, STR_TO_DATE('25/04/2020', '%d/%m/%Y'), 'PASSWORD');

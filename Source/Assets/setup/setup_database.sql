/*
 This file contains instructions for creating the database, to be used
 during the installation process to create the necessary database and
 setup the tables. The database will be empty after this installation
 and data can be safely added afterwards.
 */
-- Create the new database
CREATE DATABASE NeatTreats;

USE NeatTreats;

/*
 Add the required tables.
 
 This will not add any data yet, and only set up the table structures.
 */
-- Create the person table.
CREATE TABLE Person (
    PersonID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    FirstName VARCHAR(125),
    SecondName VARCHAR(125)
) ENGINE = InnoDB;

-- Create the customer table.
CREATE TABLE Customer (
    CustomerID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    PersonID INT(11) UNSIGNED NOT NULL,
    Email VARCHAR(255),
    Password VARCHAR(64),
    FOREIGN KEY (PersonID) REFERENCES Person (PersonID)
) ENGINE = InnoDB;

-- Create the ingredient table.
CREATE TABLE Ingredient (
    IngredientID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(125)
) ENGINE = InnoDB;

-- Create the ingredient collection table.
CREATE TABLE IngredientCollection (
    IngredientCollectionID INT(11) UNSIGNED NOT NULL,
    IngredientID INT(11) UNSIGNED NOT NULL,
    Quantity FLOAT(12),
    MaxQuantity FLOAT(12),
    INDEX (IngredientCollectionID, IngredientID), 
    FOREIGN KEY (IngredientID) REFERENCES Ingredient (IngredientID)
) ENGINE = InnoDB;

-- Create the address table.
CREATE TABLE Address (
    AddressID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Line1 VARCHAR(125),
    Line2 VARCHAR(125),
    PostCode VARCHAR(8)
) ENGINE = InnoDB;

-- Create the branch table.
CREATE TABLE Branch (
    BranchID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    AddressID INT(11) UNSIGNED NOT NULL,
    IngredientCollectionID INT(11) UNSIGNED NOT NULL,
    Name VARCHAR(125),
    FOREIGN KEY (AddressID) REFERENCES Address (AddressID),
    FOREIGN KEY (IngredientCollectionID) REFERENCES IngredientCollection (IngredientCollectionID)
) ENGINE = InnoDB;

-- Create the role table.
CREATE TABLE Role (
    RoleID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(125)
) ENGINE = InnoDB;

-- Create the employee table.
CREATE TABLE Employee (
    EmployeeID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    PersonID INT(11) UNSIGNED NOT NULL,
    RoleID INT(11) UNSIGNED NOT NULL,
    BranchID INT(11) UNSIGNED NOT NULL,
    StartDate DATE,
    PassCode CHAR(8),
    FOREIGN KEY (PersonID) REFERENCES Person (PersonID),
    FOREIGN KEY (RoleID) REFERENCES Role (RoleID),
    FOREIGN KEY (BranchID) REFERENCES Branch (BranchID)
) ENGINE = InnoDB;

-- Create the cake table.
CREATE TABLE Cake (
    CakeID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    IngredientCollectionID INT(11) UNSIGNED NOT NULL,
    Price FLOAT(12),
    Name VARCHAR(125),
    FOREIGN KEY (IngredientCollectionID) REFERENCES IngredientCollection (IngredientCollectionID)
) ENGINE = InnoDB;

-- Create the cart table.
CREATE TABLE Cart (
    CartID INT(11) UNSIGNED NOT NULL,
    CakeID INT(11) UNSIGNED NOT NULL,
    Quantity INT(4) UNSIGNED,
    INDEX COMPOUND(CartID, CakeID)
) ENGINE = InnoDB;

-- Create the order table.
CREATE TABLE CustomerOrder (
    OrderID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    CustomerID INT(11) UNSIGNED NOT NULL,
    CartID INT(11) UNSIGNED NOT NULL,
    BranchID INT(11) UNSIGNED NOT NULL,
    OrderTime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    SubTotal FLOAT(12),
    Status VARCHAR(20) DEFAULT 'UNSTARTED',
    FOREIGN KEY (CustomerID) REFERENCES Customer (CustomerID),
    FOREIGN KEY (CartID) REFERENCES Cart (CartID),
    FOREIGN KEY (BranchID) REFERENCES Branch (BranchID)
) ENGINE = InnoDB;

/*
 CREATE USER IF NOT EXISTSs and setup privileges.
 */

-- Create the customer user.
CREATE USER IF NOT EXISTS Customer @localhost IDENTIFIED BY "Password123";
GRANT SELECT, UPDATE, INSERT ON Customer TO Customer @localhost;
GRANT SELECT, UPDATE, INSERT ON Person TO Customer @localhost;
GRANT SELECT, INSERT ON Cart TO Customer @localhost;
GRANT SELECT, INSERT ON CustomerOrder TO Customer @localhost;
GRANT SELECT ON Cake TO Customer @localhost;

-- Create the baker user.
CREATE USER IF NOT EXISTS Baker @localhost IDENTIFIED BY "Password123";
GRANT SELECT ON Cart TO Baker @localhost;
GRANT SELECT, UPDATE ON CustomerOrder TO Baker @localhost;
GRANT SELECT, UPDATE ON IngredientCollection TO Baker @localhost;

-- Create the cashier user.
CREATE USER IF NOT EXISTS Cashier @localhost IDENTIFIED BY "Password123";
GRANT SELECT ON CustomerOrder TO Cashier @localhost;

-- Create the manager user.
CREATE USER IF NOT EXISTS Manager @localhost IDENTIFIED BY "Password123";
GRANT SELECT, UPDATE, INSERT ON Employee TO Manager @localhost;
GRANT SELECT, UPDATE, INSERT ON Person TO Manager @localhost;
GRANT SELECT ON Role TO Manager @localhost;
GRANT SELECT ON Branch TO Manager @localhost;


/* Post prototype refinement changes */
ALTER TABLE Cake ADD COLUMN IsVegan BOOL;
UPDATE Cake SET IsVegan = FALSE;
UPDATE Cake SET IsVegan = TRUE WHERE CakeID = 4;
ALTER TABLE CustomerOrder ADD COLUMN LastCardDigits CHAR(4);
ALTER TABLE Person ADD COLUMN Title VARCHAR(10);


/* Software development changes */

-- Make sure employees can check their own details.
GRANT SELECT ON Employee TO Baker @localhost;
GRANT SELECT ON Person TO Baker @localhost;
GRANT SELECT ON Role TO Baker @localhost;
GRANT SELECT ON Branch TO Baker @localhost;
GRANT SELECT ON Employee TO Cashier @localhost;
GRANT SELECT ON Person TO Cashier @localhost;
GRANT SELECT ON Role TO Cashier @localhost;
GRANT SELECT ON Branch TO Cashier @localhost;

-- Make sure cart id autoincrements
-- Temporarily remove foreign key relationship to modify then add again.
ALTER TABLE CustomerOrder
    DROP FOREIGN KEY customerorder_ibfk_2;
ALTER TABLE Cart
    MODIFY COLUMN CartID INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE CustomerOrder
    ADD CONSTRAINT FOREIGN KEY (CartID) REFERENCES Cart (CartID);

-- Let customer temporarily check the next auto_increment for cart.
GRANT DELETE ON Cart TO Customer@localhost;

--  Let baker see names of cakes.
GRANT SELECT ON Cake to Baker @localhost;

-- Let manager delete employees and people
GRANT DELETE ON Employee to Manager @localhost;
GRANT DELETE ON Person to Manager @localhost;

-- Let manager check stock levels for his/her branch
GRANT SELECT ON IngredientCollection TO Manager @localhost;
-- Let manager get ingredient names too.
GRANT SELECT ON Ingredient TO Manager @localhost;
-- Let manager subtract expired ingredient from his/her branch inventory
GRANT UPDATE ON IngredientCollection TO Manager @localhost;


-- Let customer check the name of the branch to collect order from
GRANT SELECT ON Branch TO Customer @localhost;

-- Let customer change their order status to cancelled
GRANT UPDATE ON CustomerOrder TO Customer @localhost;

-- Let cashier set order status to collected on collection.
GRANT UPDATE ON CustomerOrder TO Cashier @localhost;
<?php
// Form processing template
// You can find-replace CAPS things with actual values.
/*
 REPLACE_Form Name      space separated form name eg Customer Profile Update
 REPLACE_FormName       PascalCase form name eg CustomerProfileUpdate
 REPLACE_INPUTARRAY     "$_POST" or "$_GET"
 REPLACE_rel_form_url   source relative form path eg customer/signin.php
 REPLACE_rel_land_url   source relative landing page path eg customer/customer_profile.php


 Replace these with nothing if database link is not required:
 , $databaseLink
 $databaseLink = connectToNeatTreats("Customer", "Password123");

 */

/**
 * Processes a submitted REPLACE_Form Name form.
 */

require_once("../config.php");
// Uncomment or remove as required
// require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");

function onSubmitREPLACE_FormName($inputArray)
{
  // Declare form local constants.
  /** Url the form came from to send back any errors. */
  $formPageUrl = HREF_ROOT . "REPLACE_rel_form_url";
  /** Url the form intends to go if successful. */
  $landingPageUrl = HREF_ROOT . "REPLACE_rel_land_url";

  /** Whether form has terminated with success. */
  $wasFormProcessSuccessful = false;

  $databaseLink = connectToNeatTreats("Customer", "Password123");
  $formErrors = array();

  // Sanitize and prepare inputs for validation.
  sanitizeFormInputArray($inputArray);
  // Extract each field as required.
  // $input1 = $inputArray["input1"]
  // $input2 = $inputArray["input2"]

  // Validate input1 field.
  // ...
  
  // Validate input2 field.
  // ...  

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
  onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return;
  } else {
    // No errors, so do ... (add to database, set cookie, etc)

    // If the outcome was successful, set the form status.
    $wasFormProcessSuccessful = true;
  }

  if ($wasFormProcessSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - REPLACE_Form Name</title>
</head>

<body>
  Please wait while you are redirected...
<?php onSubmitREPLACE_FormName(REPLACE_INPUTARRAY); ?>
</body>

</html>

<!DOCTYPE HTML>
<html>

<body>
  <div id="footer" class="footer mdc-theme--background mdc-elevation--z1">
    <div class="footer-inner">
      <div class="footer-links">
        <ul class="navbar-root footer-page-links">
          <li class="navbar-item">
            <a href=<?php echo HREF_ROOT . "info/about.php"; ?> class="navbar-item mdc-typography--body1">
              <span>About</span>
            </a></li>
          <li class="navbar-item">
            <a href=<?php echo HREF_ROOT . "info/contact.php"; ?> class="navbar-item mdc-typography--body1">
              <span>Contact Us</span>
            </a></li>
          <li class="navbar-item">
            <a href=<?php echo HREF_ROOT . "info/faq.php"; ?> class="navbar-item mdc-typography--body1">
              <span>FAQ</span>
            </a></li>
          <li class="navbar-item">
            <a href=<?php echo HREF_ROOT . "info/privacy.php"; ?> class="navbar-item mdc-typography--body1">
              <span>Privacy Policy</span>
            </a></li>
          <li class="navbar-item">
            <a href=<?php echo HREF_ROOT . "info/terms.php"; ?> class="navbar-item mdc-typography--body1">
              <span>Terms and Conditions</span>
            </a></li>


          <a href="#" id="footer-page-to-top" class="navbar-item mdc-typography--body1">
            <i class="material-icons">arrow_upward</i>
            <span>Back to top</span>
          </a>
        </ul>


      </div>


      <div id="copyright-block" class="copyright-block">
        <i class="material-icons">copyright</i>
        <span class="mdc-typography--overline">Copyright Neat Treats 2019</span>
      </div>
    </div>
  </div>

</body>

</html>

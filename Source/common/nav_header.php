<!DOCTYPE HTML>
<html>
<?php
// require_once("../config.php");
require_once(SITE_ROOT . "common/cart_helper.php");
?>

<body>
  <div id="header" class="header mdc-elevation--z1 mdc-theme--surface">
    <div style="height: 20px;">
      <!--Disappears when scrolling.-->
    </div>
    <div class="inner-content-central header-inner mdc-elevation--z1">
      <!--Persistent-->
      <ul class="navbar-root">
        <li class="navbar-item">
          <a href=<?php echo HREF_ROOT . "shop/cakes.php"; ?> class="navbar-item mdl-typography--body-1">
            <i class="material-icons">cake</i>
            <span>CAKES</span>
          </a></li>
        <li class="navbar-item">
          <a href=<?php echo HREF_ROOT . "info/about.php"; ?> class="navbar-item mdc-typography--body1">
            <i class="material-icons">info</i>
            <span>ABOUT</span>
          </a></li>
        <li class="navbar-item">
          <a href=<?php echo HREF_ROOT; ?> class="navbar-item mdc-typography--body1">
            <img id="site-logo" alt="Neat Treats logo" src=<?php echo HREF_ROOT . "assets/images/logo.png" ?> />
          </a></li>
        <li class="navbar-item">
          <a href=<?php echo HREF_ROOT . "info/contact.php"; ?> class="navbar-item mdc-typography--body1">
            <i class="material-icons">perm_contact_calendar</i>
            <span>CONTACT</span>
          </a></li>
        <li class="navbar-item">
          <a href="<?php
                    // Can't start session after sending HTML!
                    // if (session_status() != PHP_SESSION_ACTIVE) {
                    //   session_start();
                    // }
                    $is_logged_in = isset($_SESSION["customerID"]);
                    $hrefLink = $is_logged_in ? "customer/customer_profile.php" : "customer/signin.php";
                    echo HREF_ROOT . $hrefLink;
                    ?>" class="navbar-item mdc-typography--body1">
            <i class="material-icons">perm_identity</i>
            <span><?php echo $is_logged_in ? "MY PROFILE" : "SIGNIN" ?></span>
          </a></li>
      </ul>
      <a href=<?php echo HREF_ROOT . "shop/cart.php"; ?> class=" cart-link navbar-item mdl-typography--body-1 mdc-theme--surface mdc-card">
        <i class="material-icons" style="float: left">shopping_cart</i>
        <span id="header-cart-link__text" style="float: right">
          <?php
          $newItemCount = getCartItemCount();
          echo $newItemCount == 0 ? "Empty" : "$newItemCount Cake" . ($newItemCount == 1 ? "" : "s");

          ?></span>
      </a>
    </div>
  </div>

</body>

</html>

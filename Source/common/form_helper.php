<?php

// require_once("../config.php");
require_once(SITE_ROOT . "common/db_connect.php");

/**
 * This module contains functions to help with handling form inputs.
 * 
 * @category Form Handling
 * 
 */

// Constants

/**
 * Form error types with their corresponding error messages.
 */
class EFormErrors
{
  /** A required field was left empty. */
  const EMPTY_FIELD = "Must not be left empty";

  /** A numeric field contained a non-numeric value. */
  const NOT_NUMBER = "Must be a number";

  /** The input wasn't one of the allowed lookup values. */
  const NOT_IN_LOOKUP = "Invalid option selected";

  /** The input didn't match the required pattern. */
  const PATTERN_FAIL = "Must match the required pattern";

  /** The input didn't match the $pattern. */
  public static function PATTERN_FAIL($pattern)
  {
    return "Must be like: $pattern";
  }

  /** Input that ought to be unique had conflicts in the database. */
  const ALREADY_EXISTS = "Already in use; try something else";

  /** Input that ought to be in the database wasn't found. */
  const DOESNT_EXIST = "Not in our records";

  /** Two inputs that ought to be identical were distinct. (use on second input) */
  const DOESNT_MATCH = "Inputs must match";

  /** Input didn't match a field in a specific row in the database. */
  const DOESNT_MATCH_ROW = "Doesn't match our records";

  /** Two inputs that ought to be different were identical. (use on second input) */
  const REPEATED_INPUT = "Must be different";

  /** Input was unchanged from the previous value in the database. */
  const REPEATED_ROW = "Must be different to previous value";

  /** The input length exceeded expected range. */
  const LENGTH_MISMATCH = "Invalid length";

  /** The input length exceeded the range [$min, $max] inclusive. */
  static function LENGTH_EXCEED_RANGE(int $min, int $max)
  {
    return "Length must be between $min and $max";
  }

  /** The input was not exactly as long as $requiredLength. */
  static function LENGTH_MISMATCH(int $requiredLength)
  {
    return "Must have a length of $requiredLength";
  }
  
  /** The input was shorter than expected. */
  const TOO_SHORT = "Must be longer";

  /** The input was shorter than $min. */
  static function TOO_SHORT(int $min)
  {
    return "Must be longer than $min";
  }

  /** The input was longer than expected. */
  const TOO_LONG = "Must be shorter";

  /** The input was longer than $max. */
  static function TOO_LONG(int $max)
  {
    return "Must be shorter than $max";
  }

  /** The required permissions for submission weren't met. */
  const NOT_PERMITTED = "Not permitted to submit form";
}

/**
 * Expected patterns for supported fields.
 */
class EInputPatterns
{
  /** Pattern for first name */
  const FIRST_NAME = "/^[A-Z,a-z,0-9]*$/";
  /** Pattern for second name */
  const SECOND_NAME = "/^[A-Z,a-z,0-9]*$/";
  /** Pattern for middle name. */
  const EMAIL = "/^[a-zA-Z0-9.!#$%&’*+=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/";
  /** Pattern for password */
  const PASSWORD = "/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*£]).{8,}/";
  /** Pattern for identifying words in pascal case strings. */
  const PASCAL_CASE = array('/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/');
  
  /** 
   * Pattern for a date that can be entered into MySQL.
   * 
   * Eg: `2018-03-25`, `2020/01/23`, `1992-30/2`
   * */
  const DATE = "/^[0-9]{4}[-\/][0-9]{1,2}[-\/][0-9]{1,2}$/";

  const PASS_CODE = "/^[A-Z]*$/";
}

/**
 * Lookup options for supported fields.
 */
class ELookupOptions
{
  /** Lookup options for title. */
  const TITLE = array("mr", "mrs", "miss", "ms", "dr");

  /** Lookup options for order status. */
  const ORDER_STATUS = array(EOrderStatus::UNSTARTED, EOrderStatus::STARTED, EOrderStatus::FINISHED, EOrderStatus::COLLECTED);

  /** Lookup options for customer profile tab indexes. */
  const CUSTOMER_PROFILE_TABS = array("orders", "account", "settings");

  /** Order statuses for which the customer can cancel their order. */
  const CANCELLABLE_STATUS = array(EOrderStatus::UNSTARTED, EOrderStatus::STARTED);

  /** Order statuses for which the baker can view and edit the order card. */
  const BAKER_SHOWN_STATUS = array(EOrderStatus::UNSTARTED, EOrderStatus::STARTED);
}



// Sanitization and validation

/**
 * Prepare any data coming directly from a form before any validation.
 * 
 * Useful for avoiding potentially invalid input and hacking. Input still
 * needs to be validated afterward.
 *
 * @param string $input Input data to prepare. 
 * @return string Prepared data.
 */
function sanitizeFormInput(string $input): string
{
  $input = trim($input);
  $input = strip_tags($input);
  $input = stripslashes($input);
  $input = htmlspecialchars($input);
  global $neatTreatDatabaseRoot;
  $input = $neatTreatDatabaseRoot->real_escape_string($input);

  return $input;
}

/**
 * Sanitize all form fields in an array.
 * 
 * Array will be modified in place so no value is returned.
 *
 * @param array $input Array to sanitize (`$_POST` or `$_GET`).
 * @return void
 */
function sanitizeFormInputArray(array &$input)
{
  foreach ($input as $key => $value) {
    $input[$key] = sanitizeFormInput($value);
  }
}

/**
 * Return whether a string has any data present.
 */
function isPresent(string $input): bool
{
  return $input !== "";
}

// All the below functions will return true if an empty string is passed.
// This way any required fields must be checked first using isPresent.

/**
 * Return whether a string has an allowable length, including
 * both endpoints.
 */
function isLengthBetween(string $input, $min, $max): bool
{
  if ($input === "") {return true;}
  
  $length = strlen($input);
  return $min <= $length && $length <= $max;
}

/**
 * Return whether a string has an allowable exact length.
 */
function isLengthExactly(string $input, $length): bool
{
  if ($input === "") {return true;}

  return strlen($input) == $length;
}

/**
 * Return whether a number is within a given range, including
 * both endpoints.
 */
function isInRange(string $input, $min, $max): bool
{
  if ($input === "") {return true;}
  
  $inputNum = floatval($input);
  return $min <= $inputNum && $inputNum <= $max;
}

/**
 * Return whether the input matches a regular expression.
 */
function matchesPattern(string $input, $pattern): bool
{
  if ($input === "") {return true;}
  
  return preg_match($pattern, $input);
}

/**
 * Return whether two strings contain the same data.
 */
function areInputsIdentical(string $input1, string $input2): bool
{
  if ($input1 === "") {return true;}
  
  return $input1 === $input2;
}

/**
 * Return whether a value is a valid options from a set of values.
 */
function isValidOption(string $input, array $options): bool
{
  if ($input === "") {return true;}

  foreach ($options as $option) {
    if ($option === $input) {
      return true;
    }
  }
  return false;
}

/**
 * Return whether a value is in the database.
 */
function isInDatabase(string $input, $connection, $table, $column, $useBinary = false): bool
{
  if ($input === "") {return true;}

  $binaryKeyword = 
  $query = ("SELECT COUNT($column) FROM $table " .
    "WHERE $column = ". ($useBinary ? "BINARY " : "") . "'$input';");

  $result = $connection->query($query);

  if ($connection->error) {
    return false;
  }

  return $result->fetch_assoc()["COUNT($column)"] > 0;
}

/**
 * Return whether a row contains the values in the database.
 * 
 * All inputs should be sanitized already.
 *
 * @param array $inputs Associative array of column names to input
 * values.
 * @param mysqli $connection Database connection to operate on.
 * @param string $table Name of table to check.
 * @param bool $useBinary Whether to check binary data for case
 * sensitivity. Can be slower to query. Default: false.
 * @return boolean Whether at least one row containing all specified
 * values was found.
 */
function isRowInDatabase(array $inputs, $connection, $table, $useBinary = false): bool
{
  // Return true if all inputs were empty.
  $allEmpty = true;
  foreach ($inputs as $_ => $value) {
    if ($inputs !== "") {
      $allEmpty = false;
      break;
    }
  }
  if ($allEmpty) {return true;}
  
  // All criteria required for row to be considered in database.
  $conditions = "";

  $isFirstCriterion = true;
  foreach ($inputs as $column => $inputValue) {
    $conditions .= (
      ($isFirstCriterion ? "" : " AND ") .
      ("$column = ") .
      ($useBinary ? "BINARY " : "") .
      ("'$inputValue'")
    );
    
    if ($isFirstCriterion) {
      $isFirstCriterion = false;
    }
  }

  $query = (
    "SELECT COUNT(*) FROM $table " .
    "WHERE $conditions;"
  );

  $result = $connection->query($query);

  if (!empty($connection->error)) {
    // An error occurred during the query.
    return false;
  }

  return $result->fetch_assoc()["COUNT(*)"] > 0;
}

/**
 * Return whether a string contains only digits.
 */
function isTypeInt(string $input): bool
{
  return matchesPattern($input, "/^[0-9]*$/");
}

/**
 * Returns whether a staff member is logged in.
 *
 * This does not check if the employee is in the database.
 *
 * @param array $sessionArray Current value of $_SESSION.
 * @param string $requiredRole Optionally specified role to check.
 * @return boolean Whether the staff member is logged in.
 */
function isEmployeeLoggedIn($sessionArray, $requiredRole = NULL): bool 
{
  $employeeID = $sessionArray["employeeID"] ?? NULL;
  $role = $sessionArray["role"] ?? NULL;
  return (
    $employeeID !== NULL
    && ($requiredRole === NULL || $role === $requiredRole));
}

/**
 * Returns whether a customer is logged in.
 *
 * This does not check if the customer is in the database.
 * 
 * @param array $sessionArray Current value of $_SESSION.
 * @return boolean Whether the customer is logged in.
 */
function isCustomerLoggedIn($sessionArray): bool
{
  $customerID = $sessionArray["customerID"] ?? NULL;
  return $customerID !== NULL;
}


// Make form presentable to client.

/**
 * Redirect to customer registration form page.
 */
function redirect(string $pageUrl)
{
  echo ("<script type='text/javascript'>" .
    "window.location.replace('$pageUrl');" .
    "</script>");
}

/**
 * Terminate the form processing sending back the errors to the client.
 */
function onFormValidationFail($formErrors, $inputArray, $formPageUrl,
                              $databaseLink=null)
{
  // Set cookies to temporarily hold the form errors.
  $cookieDuration = time() + 60 * 60 * 24 * 3;
  setcookie("errors", json_encode($formErrors), $cookieDuration, "/");

  onFormFail($inputArray, $formPageUrl, $databaseLink);
}

/**
 * Terminate the form processing on failure to let the client try again.
 * 
 * @see `onFormValidationFail` For sending back form with error messages.
 */
function onFormFail($inputArray, $formPageUrl, $databaseLink=null)
{
  // Set cookies to temporarily hold the inputs to show on the form again.
  $cookieDuration = time() + 60 * 60 * 24 * 3;
  setcookie("lastInput", json_encode($inputArray), $cookieDuration, "/");

  // Close the database connection as it's no longer needed.
  if ($databaseLink !== null) {
    $databaseLink->close();
  }
  
  // Load the original form page back to the client.
  redirect($formPageUrl);
}

/**
 * Terminate the form processing for a successful response.
 */
function onFormSuccess($landingPageUrl, $databaseLink = null)
{
  if ($databaseLink !== null) {
    $databaseLink->close();
  }
  redirect($landingPageUrl);
}

/**
 * Load form errors into an associative array and clear the cookie.
 *
 * @return array
 */
function loadFormErrors(): array
{
  return loadArrayFromCookie("errors");
}

/**
 * Load last form input data into an associative array and clear
 * the cookie.
 *
 * @return array
 */
function loadLastFormInput(): array
{
  return loadArrayFromCookie("lastInput");
}

/**
 * Load a JSON into an associative array and clear the cookie.
 *
 * @param string $cookieName Name of cookie to load from.
 * @return array Returned associative array. If $cookieName is not found
 * an empty array will be returned.
 */
function loadArrayFromCookie(string $cookieName): array
{
  if (!isset($_COOKIE[$cookieName])) {
    return array();
  }

  $ret = json_decode($_COOKIE[$cookieName], true);
  clearArrayFromCookie($cookieName);
  return $ret;
}

/**
 * Clear the JSON data stored in a cookie.
 * 
 * @param string $cookieName Name of cookie to clear.
 */
function clearArrayFromCookie(string $cookieName)
{
  // Clear cookie from this page specifically.
  setcookie($cookieName, "", time() - 50);
  // Clear cookie from the whole website directory.
  setcookie($cookieName, "", time() - 50, "/");
}

/**
 * Get the error class for a given field.
 */
function getErrorClass($formErrors, string $fieldName)
{
  return isset($formErrors[$fieldName]) ? "is-invalid is-dirty" : "";
}

/**
 * Get the error message for a given field.
 */
function getErrorMessage($formErrors, string $fieldName, $default = "")
{
  return isset($formErrors[$fieldName]) ? $formErrors[$fieldName] : $default;
}

/**
 * Get the last entered input value for a given field.
 */
function getLastInput($lastInputs, string $fieldName, $default = ""): string
{
  return $lastInputs[$fieldName] ?? $default;
}

/**
 * Return selected status of a given option field in a select form.
 */
function getLastSelectedState($lastInputs, string $fieldName, string $value, $default = ""): string
{
  if (isset($lastInputs[$fieldName]) && $lastInputs[$fieldName] == $value) {
    return "selected";
  }
  return "";
}

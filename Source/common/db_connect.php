<?php

// Connect to the database.

/**
 * Connect to the Neat Treats database.
 *
 * @param string $username Username for authentication. @see
 * STAFF_ROLE_* constants in config.php.
 * @param string $password Password for user.
 * @return mysqli Database connection for querying.
 */
function connectToNeatTreats(string $username, string $password) {
    $database = new mysqli("localhost", $username, $password, "NeatTreats");

    if (!mysqli_connect_errno()) {
        // No errors in connection.
        return $database;
    }
    else {
        return null;
    }
}

/**
 * Root level access to Neat Treats database. Should not be used
 * to query the database.
 */
$neatTreatDatabaseRoot = connectToNeatTreats("root", "") or die();

?>

<?php

// require_once("../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");

/**
 * Return the value of the cart cookie.
 */
function getCart() {
  if (isset($_COOKIE["cart"])) {
    return json_decode($_COOKIE["cart"], true);
    }
  return null;
}

/**
 * Set the value of the cart cookie.
 */
function setCart(array $cart) {
  if ($cart != null) {
    $duration = time() + 60 * 60 * 24 * 30;
    setcookie("cart", json_encode($cart), $duration, "/");
  }
}

/**
 * Empty the cart array in the customer’s cookies.
 */
function clearCart() {
  setcookie("cart", "", time() - 3600, "/");
}

/**
 * Get cart id associated with an order.
 *
 * @param [type] $orderID Order to check.
 * @param mysqli $databaseLink Connection to database. Must be able to
 * SELECT from CustomerOrder table.
 * @return integer|NULL Output cart id or NULL if not found.
 */
function getCartID($orderID, mysqli $databaseLink)
{
  // Get the cart id associated with the order.
  $query = "SELECT CartID FROM CustomerOrder WHERE OrderID = $orderID";
  $result = $databaseLink->query($query);
  if (!empty($databaseLink->error) || $result->num_rows == 0) {
    // Failed to make query or no orders with that order id found.
    return NULL;
  }
  $cartID = $result->fetch_assoc()["CartID"];
  return $cartID;
}

/**
 * Get data to show in an invoice for a given order.
 * 
 * @param [type] $orderID Order to check. Must be a valid order id.
 * @param array $outInvoiceData Output associative array for invoice
 * output. Keys are: SubTotal, BranchName, LastCardDigits, OrderTime,
 * CollectionTime.
 * @param mysqli $databaseLink Connection to database. Must be able to
 * SELECT from CustomerOrder and Branch table.
 * @return boolean Whether query succeeded. False if Order ID not in
 * database.
 */
function getInvoiceDetails(
  $orderID,
  &$outInvoiceData,
  mysqli $databaseLink
):bool
{
  $query = (
    "SELECT SubTotal, Branch.Name as 'BranchName', LastCardDigits, " .
      "OrderTime FROM CustomerOrder " .
    "LEFT JOIN Branch ON Branch.BranchID = CustomerOrder.BranchID " .
    "WHERE OrderID = $orderID " .
    "LIMIT 1;"
  );

  $result = $databaseLink->query($query);
  if (!empty($databaseLink->error) || $result->num_rows == 0) {
    $outInvoiceData = array();
    return false;
  }

  // Success - return the whole row.
  $row = $result->fetch_assoc();

  // Format order timestamp as ISO8601 format, not just string.
  $row["OrderTime"] = strtotime($row["OrderTime"]);

  // Set estimated time to be 3 days ahead.
  $collectionTime = $row["OrderTime"] + 60 * 60 * 24 * 3;
  $row["CollectionTime"] = $collectionTime;

  $outInvoiceData = $row;
  return true;
}

/**
 * Returns the customerid of the owner of an order.
 *
 * @param [type] $orderID Order to check.
 * @param [type] $databaseLink Connect to database. Must be able to
 * SELECT from CustomerOrder table.
 * @return int|NULL CustomerID who placed order or NULL if failed
 * query.
 */
function getOrderOwner($orderID, $databaseLink)
{
  $query = "SELECT CustomerID FROM CustomerOrder WHERE OrderID = $orderID;";
  $result = $databaseLink->query($query);
  if (!empty($databaseLink->error) || $result->num_rows == 0) {
    return NULL;
  }
  // Cast to integer before returning.
  return (int) $result->fetch_assoc()["CustomerID"];
}

/**
 * Get cart of items for a specific order.
 *
 * @param [type] $orderID Order to check.
 * @param array $outCartData Output associative array like
 * {cakeID => quantity}. Can be empty.
 * @param mysqli $databaseLink Connection to database. Must be able to
 * SELECT from CustomerOrder and Cart table.
 * @return boolean Whether query was successful.
 */
function getConfirmedCart(
  $orderID,
  &$outCartData,
  $databaseLink
): bool {
  // Initialise output.
  $outCartData = array();

  $cartID = getCartID($orderID, $databaseLink);
  if ($cartID === null) {
    return false;
  }

  $query = "SELECT CakeID, Quantity FROM Cart WHERE CartID = $cartID";
  $result = $databaseLink->query($query);
  if (!empty($databaseLink->error)) {
    // Failed to make query.
    return false;
  }
  
  // Load query rows into output array.
  while ($row = $result->fetch_assoc()) {
    $outCartData[$row["CakeID"]] = $row["Quantity"];
  }

  return true;
}

/**
 * Return total number of cakes in a cart.
 */
function getCartItemCount($cakeID=null) {
  $cart = getCart();
  if ($cart == null) {
    return 0;
  }

  $sum = 0;
  foreach ($cart as $cakeIDToCheck => $quantity) {
    if ($cakeID == null || $cakeID == $cakeIDToCheck) {          
      $sum += $quantity;
    }
  }
  return $sum;
}

/**
 * Return the total price of customer shopping cart.
 */
function getCartSubtotal($databaseLink) {
  $cart = getCart();
  if ($cart == null) {
    return 0.0;
  }

  // Query the database for the prices.
  $isFirst = true;
  $conditions = "";
  foreach ($cart as $cakeIDToCheck => $quantity) {
    $conditions .= (
      ($isFirst ? "" : " OR ") .
      ("CakeID = $cakeIDToCheck"));

    if ($isFirst) $isFirst = false;
  }

  $query = "SELECT CakeID, Price FROM Cake WHERE $conditions;";
  $result = $databaseLink->query($query);
  if (!empty($databaseLink->error)) {
    return 0.0;
  }

  // Sum up prices considering the quantity of each cake.
  $sum = 0.0;
  while ($row = $result->fetch_assoc()) {
    $quantity = $cart[$row["CakeID"]];
    $sum += (float)$row["Price"] * $quantity;
  }

  return number_format($sum, 2);
}

function getCakeCard($cakeID, $name, $price, $isVegan) {
  $maxNameLen = 14;
  if (strlen($name) > $maxNameLen) {
    // Limit name to 14 characters so it fits on one line
    $name = substr($name, 0, $maxNameLen - 3) . "...";
  }
  $cartCount = getCartItemCount($cakeID);
  $isInCart = $cartCount >= 1;
  $price = number_format($price, 2);
  $veganBadge = $isVegan ? '<span class="mdl-badge vegan-badge" data-badge="V"> </span>' : "";
  return '<li class="mdl-list__item">' .
      '<div class="mdc-card cake-card">' .
        '<a href="' . HREF_ROOT . "shop/cake_profile.php?cakeID=$cakeID" . '"><div class="mdc-card__primary-action cake-card__primary-action" tabindex="0">' .
          '<div class="mdc-card__media mdc-card__media--square cake-card__media" style="background-image: url(&quot;'. HREF_ROOT . "assets/images/cake_$cakeID.png" .'&quot;);"></div>' .
        '</div></a><div class="cake-card__action mdc-card__actions">' .
    '<div class="mdc-card__action-buttons">' .
          '<h2 class="cake-card__title mdc-typography--headline5">' . $name . $veganBadge . '<br>' .
          '<p class="cake-card__price">£' . $price . '</p>' .
        '</h2></div>' .
        '<div class="mdc-card__action-icons">' .
          '<button id="cart-button--' . $cakeID . '" class="cake-card__tocart mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect material-icons" onclick="onCakeCardCartActionClicked(' . $cakeID . ')" ' .
            'title="'. ($isInCart? "Go to cart": "Add to cart") . '">'. ($isInCart? "shopping_cart": "add_shopping_cart") . '</button>' .
    '</div></div></div></li>';
}

/**
 * Sort an array of arrays in place based on the criterion.
 *
 * The input array is an enumerated array of associative array.
 * The associative arrays are compared using the key criterion.
 * 
 * Uses bubble sort.
 * 
 * @param array $array Array of arrays to sort to sort in place.
 * @param [type] $criterion Key to compare items by.
 * @param boolean $descend Whether to sort in descending order.
 * Default is false (ascending).
 */
function sort2dArray(array &$toSort, $criterion, $descend = false)
{
  // Whether any swaps have been made this iteration.
  $madeSwaps = false;

  // Index of last item in list.
  $lastIndex = count($toSort) - 1;

  do {
    $madeSwaps = false;
    
    for ($index=0; $index < $lastIndex; $index++) {
      // Go through the whole array and compare pairs of items.
      $firstVal = $toSort[$index][$criterion];
      $secondVal = $toSort[$index + 1][$criterion];

      if ((!$descend && $firstVal > $secondVal)
        || ($descend && $firstVal < $secondVal)
      ) {
        // Swap the items.
        $temp = $toSort[$index];
        $toSort[$index] = $toSort[$index + 1];
        $toSort[$index + 1] = $temp;
        // This iteration counts as unsorted if we swapped anything.
        $madeSwaps = true;
      }
    }
  } while ($madeSwaps);
}

/**
 * Sort an array of arrays based on a criterion.
 * 
 * The input array is an enumerated array of associative arrays.
 * Associative arrays are compared using the key criterion.
 * 
 * Uses merge sort, worst case time complexity O(nlog(n)).
 *
 * @param array $array Array of arrays to sort to sort in place.
 * @param mixed $criterion Key to compare items by.
 * @param boolean $descend Whether to sort in descending order.
 * Default is false (ascending).
 * @return array Sorted array.
 */
function mergeSort2dArray(array $toSort, $criterion, $descend = false): array
{
  if (count($toSort) == 0) return array();

  /** Merges two pre-sorted arrays and returns a new array. */
  $_merge = function ($arrayA, $arrayB) use ($criterion, $descend): array {
    $merged = array();
    while (!empty($arrayA) && !empty($arrayB)) {
      // Compare from which array to add items if both have items.
      $a = $arrayA[0][$criterion];
      $b = $arrayB[0][$criterion];
      
      // Only pick arrayA if value a is less than value b.
      $chooseA = $descend ? ($a > $b) : ($a < $b);

      // Take the first array from the front.
      $toAdd = $chooseA ? array_shift($arrayA) : array_shift($arrayB);
      $merged[] = $toAdd;
    }

    // Add any remaining items.
    foreach ($arrayA as $_) $merged[] = $_;
    foreach ($arrayB as $_) $merged[] = $_;

    return $merged;
  };

  /** Yields i-th elements of both arrays until the longest one is exhausted. */
  $_zipLongest = function ($arrayA, $arrayB, $fillValue = NULL) {
    $lenA = count($arrayA);
    $lenB = count($arrayB);

    // No yielding if both are empty.
    if ($lenA == 0 && $lenB == 0) return;

    for ($i=0; $i < max($lenA, $lenB); $i++) { 
      // Yield successive elements until reaching end of longest array.
      // Use fill value if beyond the length of one of the arrays.
      yield [
        $lenA > $i ? $arrayA[$i] : $fillValue,
        $lenB > $i ? $arrayB[$i] : $fillValue
      ];
    }
  };

  // Put each item in its own array.
  $arrays = array();
  foreach ($toSort as $_) $arrays[] = array($_);

  while (count($arrays) > 1) {
    // Halve the array and merge their elements together.
    $mid = count($arrays) / 2;
    $firstHalf = array_slice($arrays, 0, $mid);
    $secondHalf = array_slice($arrays, $mid);

    // Safe to rewrite $arrays now that copies are made.
    $arrays = array();
    foreach($_zipLongest($firstHalf, $secondHalf)  as list($a, $b)) {
      $arrays[] = $a && $b ? $_merge($a, $b) : $a ?: $b;
    }
  }

  return $arrays[0];
}

/**
 * Precomputes the tables required for Boyer-Moore search to work.
 * 
 * These tables are required as input to the Boyer-Moore search
 * function, and only hold if the same needle is used.
 * 
 * Only small Latin characters and spaces are allowed. Needle will be
 * sanitized to remove any erroneous characters.
 *
 * @param string $inOutNeedle Pattern to search for. Will be sanitized
 * by removing unwanted characters and output by reference.
 * @param array $outBadCharTable Lookup table according to bad
 * character rule. Output by reference.
 * @param array $outGoodSuffixTable Lookup table according to good
 * suffix rule. Output by reference.
 */
function boyerMoorePreprocess(&$inOutNeedle, &$outBadCharTable, &$outGoodSuffixTable)
{
  // String of characters other that are allowed in the needle.
  // Set to allow spaces in the query string.  
  $allowedChars = " ";
  // Set to allowed small a-z letters.
  for ($_=97; $_ < 123; $_++) $allowedChars .= chr($_);

  // Sanitize the needle by removing non-latin characters and normalise to lower case.
  $inOutNeedle = preg_replace("/[^$allowedChars]/", "", strtolower($inOutNeedle));
  $needle = $inOutNeedle;

  $_enumerate = function ($array) { while (list($i, $value) = each($array)) yield [$i, $value]; };

  $outBadCharTable = (function () use ($needle, $allowedChars, $_enumerate): array {

    // A bad letter that isn't in the needle means the whole needle can be jumped.
    $wholeNeedleJump = -1;

    // Prefill the array into a 2d array with each allowed character to search for.
    $table = array_fill_keys(str_split($allowedChars), [$wholeNeedleJump]);
    
    if (strlen($needle) == 0) return $table;

    // State of each row according to index in needle. Default is to skip whole needle.    
    $alpha = array_fill_keys(str_split($allowedChars), $wholeNeedleJump);
    foreach ($_enumerate(str_split($needle)) as list($i, $char)) {
      // Each iteration, update the number of spaces from index i to the
      // previous occurrence of letter in needle.
      $alpha[$char] = $i;

      // Update table with new row state for this index in the needle.
      foreach ($_enumerate($alpha) as list($badIndex, $numJumps)) {
        $table[$badIndex][] = $numJumps;
      }
    }
    return $table;
  })();

  $outGoodSuffixTable = (function () use ($needle, $_enumerate): array {
    
    /** Return whether suffix appears at a given offset. */
    $matchesSuffix = function ($offset, $suffixFrom) use ($needle): bool {
      for ($suffixCheck = strlen($needle) - 1; $suffixCheck >= $suffixFrom; $suffixCheck--)
        if ($needle[$suffixCheck] != $needle[$suffixCheck - $offset]) return false;
      return true;
    };

    /** Return whether the preceding character of the real and hypothetical suffixes match. */
    $matchesPreceding = function ($offset, $suffixFrom) use ($needle): bool {      
      return $needle[$suffixFrom - 1] == $needle[$suffixFrom - 1 - $offset];
    };

    /** Check backward through whole needle to find index of a matching suffix. */
    $findSuffix = function ($suffixFrom) use ($needle, $matchesSuffix, $matchesPreceding): int {
      $lenNeedle = strlen($needle);
      $lenSuffix = $lenNeedle - $suffixFrom;

      // At max offset, check the very start of the needle.
      $maxOffset = $lenNeedle - 1 - ($lenSuffix - 1);
      for ($offset = 1; $offset <= $maxOffset; $offset++) {
        if (
          // Preceding characters must be different (or reached start of needle)
          ($offset == $maxOffset || !$matchesPreceding($offset, $suffixFrom))
          // Suffixes must be the same.
          && $matchesSuffix($offset, $suffixFrom)
        ) { 
          // Match found, return offset from needle end as proposed shift.
          return $offset;
        }
      }

      // Reached start of string and no matches found.
      return 0;
    };

    $table = array();
    foreach ($_enumerate(str_split($needle)) as list($i, $char)) {
      // Check backward through the needle to find the next last occurrence of
      // the suffix, and doesn't have the same preceding character.
      $table[$i] = $findSuffix($i);
    }
    return $table;
  })();
}

/**
 * Returns start index of first full instance of needle in haystack.
 * 
 * Tables must be calculated from the `boyerMoorePreprocess` function
 * and reused across subsequent runs with different haystacks, so long
 * as the needle (search pattern) is kept constant.
 *
 * @param string $needle Pattern to search for within the haystack.
 * @param string $haystack Text to search through.
 * @param array $badCharTable Precomputed lookup table for the bad
 * character rule for this needle.
 * @param array $goodSuffixTable Precomputed lookup table for the good
 * suffix table for this needle.
 * @return integer Index of the first occurrence of the needle in the
 * haystack, or -1 if it was not found.
 */
function boyerMooreSearch(
  string $needle,
  string $haystack,
  array $badCharTable,
  array $goodSuffixTable
  ): int
{
  // Check for invalid input lengths.
  $lenHaystack = strlen($haystack);
  $lenNeedle = strlen($needle);
  if ($lenNeedle > $lenHaystack || $lenNeedle == 0 || $lenHaystack == 0){
    return -1;
  }

  $k = $lenNeedle - 1;
  while ($k < $lenHaystack) {
    $i = $lenNeedle - 1;
    $h = $k;
    while ($i > -1 && $needle[$i] == $haystack[$h]) {
      // Work backwards from k to match letters to the pattern.
      $i--;
      $h--;
    }

    // Match found. Return the index and don't worry about
    // finding multiple matches.
    if ($i == -1) return $k - $lenNeedle + 1;

    // Match not found. Check how many letters we can safely
    // skip before trying to match again.

    // Try using bad character table.
    $badChar = $haystack[$h];
    $badCharShift = $i - (($badCharTable[$badChar] ?? -1)[$i] ?? -1);

    // i is the point of failure, anything already already checked is 'good'
    $goodSuffixShift = $goodSuffixTable[$i + 1] ?? 0;

    $bestShift = max($badCharShift, $goodSuffixShift);
    $k += $bestShift;
  }

  // No matches at all. Return -1.
  return -1;
}

/**
 * Search a 2d array for values matching the filter.
 *
 * The input array is an enumerated array of associative array.
 * The associative arrays are compared using the key criterion.
 * 
 * Uses linear search for each row and Boyer-Moore search for each
 * column.
 * 
 * @param array $toSearch Data to search through.
 * @param mixed $criterion Key to filter items by.
 * @param string $filter String to be contained within items to pass
 * filter.
 * @return array Items that passed the filter.
 */
function filter2dArray(
  array $toSearch,
  $criterion,
  string $filter
): array
{
  // Preprocess search term, making lookup tables for faster filtering.
  boyerMoorePreprocess($filter, $badCharTable, $goodSuffixTable);

  // Output array of filtered values.
  $filtered = array();
  // Number of filtered rows so far.
  $rowIndex = 0;
  foreach ($toSearch as $arrayToCheck) {
    $valueToCheck = strtolower($arrayToCheck[$criterion]);
    $foundAtIndex = boyerMooreSearch($filter, $valueToCheck, $badCharTable, $goodSuffixTable);
    if ($foundAtIndex != -1) {
      $filtered[$rowIndex] = $arrayToCheck;
      $rowIndex++;
    }
  }
  return $filtered;
}

/**
 * Output HTML for cake cards according to search and sort options.
 */
function populateCakeGrid($searchTerm = "", $sortMethod = "")
{
  $databaseLink = connectToNeatTreats("Customer", "Password123");
  if ($databaseLink === NULL) {
    return;
  }

  // Parse the sort method string to determine search parameters.
  switch ($sortMethod) {
    default:
      $sortCriteria = "";
      break;
    case "price_asc":
      $sortCriteria = "Price";
      $sortDescending = false;
      break;
    case "price_desc":
      $sortCriteria = "Price";
      $sortDescending = true;
      break;
    case "alpha_asc":
      $sortCriteria = "Name";
      $sortDescending = false;
      break;
    case "alpha_desc":
      $sortCriteria = "Name";
      $sortDescending = true;
      break;
  };

  $result = $databaseLink->query(
    "SELECT CakeID, Name, Price, IsVegan FROM Cake;"
  );

  if (empty($result->error)) {
    // Put the resulting database rows into an array. Since each row
    // is an array, we could say that $cakes is a 2d array.
    $rowIndex = 0;
    while ($cakeRow = $result->fetch_assoc()) {
      $cakes[$rowIndex] = $cakeRow;
      $rowIndex++;
    }

    if ($searchTerm != "") {
      // Filter to keep only rows whose Name contains the search term.
      $cakes = filter2dArray($cakes, "Name", $searchTerm);
    }

    if ($sortCriteria != "") {
      // Sort the rows if a sorting method was specified.
      $cakes = mergeSort2dArray($cakes, $sortCriteria, $sortDescending);
    }
  }

  foreach ($cakes as $cakeRow) {
    // Output each database row of cake data, now in sorted order.
    echo getCakeCard($cakeRow["CakeID"], $cakeRow["Name"], $cakeRow["Price"], $cakeRow["IsVegan"]);
  }
}

/**
 * Return formatted HTML for an editable cake row for cart table.
 */
function getCakeRow($cakeID, $cakeName, $unitPrice, $quantity)
{
  $quantityOptions = "";
  for ($i = 1; $i < 4; $i++) {
    $quantityOptions .= "<option" . ($i == $quantity ? " selected" : "") . ">$i</option>";
  }
  return '<tr id="cart-table-row__' . $cakeID . '">' .
    '<td><button onclick="onRemoveItem(' . $cakeID . ')" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect material-icons" title="Remove from cart">' .
    'close</button></td>' .
    '<td class="mdl-data-table__cell--non-numeric mdl-typography--body-1">' . $cakeName . '</td>' .
    '<td id="cart-unit-price__' . $cakeID . '" class="mdl-typography--body-1"> £' . number_format($unitPrice, 2) . '</td>' .
    '<td><select id="cart-quantity-select__'. $cakeID . '" class="mdl-textfield__input" style="width: 3em;" onchange="onQuantityChanged(' . $cakeID . ', this.value);">' .
    $quantityOptions . '</select></td>' .
    '<td id="cart-price__' . $cakeID . '" class="mdl-typography--body-1"> £' . number_format($unitPrice * $quantity, 2) . '</td>' .
    '</tr>';
};

/**
 * Return formatted HTML for an uneditable cake row for cart table.
 */
function getCakeRowUneditable($cakeID, $cakeName, $unitPrice, $quantity)
{
  $quantity = round($quantity);
  $unitPrice = number_format($unitPrice, 2);
  return '<tr id="cart-table-row__' . $cakeID . '">' .
    '<td></td>' .
    '<td class="mdl-data-table__cell--non-numeric mdl-typography--body-1">' . $cakeName . '</td>' .
    '<td id="cart-unit-price__' . $cakeID . '" class="mdl-typography--body-1"> £' . $unitPrice . '</td>' .
    "<td>$quantity</td>" .
    '<td id="cart-price__' . $cakeID . '" class="mdl-typography--body-1"> £' . number_format($unitPrice * $quantity, 2) . '</td>' .
    '</tr>';
};

/**
 * Output rows of cake details for cart table.
 */
function populateCakeItems($isEditable=true)
{
  $cart = getCart();
  if ($cart != null) {
    $databaseLink = connectToNeatTreats("Customer", "Password123");
    if ($databaseLink != null) {

      foreach ($cart as $cakeID => $quantity) {
        $result = $databaseLink->query("SELECT Name, Price FROM Cake WHERE CakeID = $cakeID");
        if (empty($databaseLink->error)) {
          $rowData = $result->fetch_assoc();          
          if ($isEditable) {
            echo getCakeRow($cakeID, $rowData["Name"], $rowData["Price"], $quantity);
          } else {
            echo getCakeRowUneditable($cakeID, $rowData["Name"], $rowData["Price"], $quantity);
          }
        }
      }
    }
  }
};

/**
 * Output uneditable rows of cake details for invoice items table.
 */
function populateInvoiceItems($orderID, $returnAsStr = false)
{
  $databaseLink = connectToNeatTreats("Customer", "Password123");
  if ($databaseLink === NULL) {
    return;
  }

  $cartItems = array();
  if (!getConfirmedCart($orderID, $cartItems, $databaseLink)) {
    // Failed to get items used for checkout.
    $databaseLink->close();
    return;
  }

  
  $returnHTML = "";  
  foreach ($cartItems as $cakeID => $quantity) {
    $result = $databaseLink->query("SELECT Name, Price FROM Cake WHERE CakeID = $cakeID");
    if (empty($databaseLink->error)) {
      $rowData = $result->fetch_assoc();
      $rowStr = getCakeRowUneditable($cakeID, $rowData["Name"], $rowData["Price"], $quantity);
      $returnHTML .= $rowStr;
    }
  }
  if ($returnAsStr) {
    return $returnHTML;
  }
  echo $returnHTML;
};

/**
 * Output HTML for a table of items in the cart.
 */
function makeCartTable($isEditable)
{
  echo (
    '<table id="cart-table" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">' .
      '<thead>' .
        '<tr>' .
          '<th></th>' .
          '<th class="mdl-data-table__cell--non-numeric">Cake Name</th>' .
          '<th>Unit Price</th>' .
          '<th>Quantity</th>' .
          '<th>Price</th>' .
        '</tr>' .
      '</thead>' .
      '<tbody>');
  populateCakeItems($isEditable);
  echo (
      '</tbody>' .
    '</table>');
}

/**
 * Output invoice table directly into output stream.
 *
 * @param [type] $orderID Order to check.
 */
function makeInvoiceTable($orderID, $returnAsStr = false) {
  $returnHTML = "";
  $returnHTML .= (
    '<table id="cart-table" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">' .
      '<thead>' .
        '<tr>' .
          '<th></th>' .
          '<th class="mdl-data-table__cell--non-numeric">Cake Name</th>' .
          '<th>Unit Price</th>' .
          '<th>Quantity</th>' .
          '<th>Price</th>' .
        '</tr>' .
      '</thead>' .
      '<tbody>');
  $returnHTML .= populateInvoiceItems($orderID, true);
  $returnHTML .= (
      '</tbody>' .
    '</table>');

  if ($returnAsStr) {
    return $returnHTML;
  }
  echo $returnHTML;
}

/**
 * Return formatted HTML contents for customer invoice email.
 */
function getInvoiceEmailContents($orderID, $invoiceData)
{
  $styleSheetUrl = HREF_ROOT . "assets/styles/neat_treats_main_style.css";
  $invoiceUrl = HREF_ROOT . "shop/invoice.php?order_id=$orderID";
  $brief = "Order #$orderID at " . date("H:i a", $invoiceData["OrderTime"]) . " on " . date("j F Y", $invoiceData["OrderTime"]);
  $subTotal = "£" . number_format($invoiceData["SubTotal"], 2);
  $lastCardDigits = $invoiceData["LastCardDigits"];
  $faqUrl = HREF_ROOT . "info/faq.php";
  $branchName = $invoiceData["BranchName"];
  $collectionTime = date("j F Y", $invoiceData["CollectionTime"]);

  return (
    '<html>' .
    '<head>' .
      '<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />' .
      '<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>' .
      '<link href="' . $styleSheetUrl . '" rel="stylesheet" type="text/css" />' .
    '</head>' .
    '<body>' .
      '<a href="' . $invoiceUrl . '">View this invoice in your browser</a>' .
      '<div class="content-headline-container">' .
        '<h1 class="mdl-typography--headline">Invoice</h1>' .
        '<p class="mdl-typography--subhead">Thanks for your order</p>' .
      '</div>' .
      '<div style="margin-left: 20px;">' .
        '<h2 class="mdl-typography--body-1" style="margin-top: 3em; margin-bottom: 0.5em">' .
          $brief .
        '</h2>' .
        '<h2 class="mdl-typography--subhead" style="margin-top: 3em; margin-bottom: 0.5em">' .
          'Purchased Items' .
        '</h2>' .
        '<div style="margin: 2em 0 2em 20px;">' .
          makeInvoiceTable($orderID, true) .
        '</div>' .
        '<p class="mdl-typography--body-1" style="margin-left: 20px">' .
          'Total Price:' .
          '<span class="mdl-typography--font-bold">' .
            $subTotal .
          '</span>' .
        '</p>' .
        '<h2 class="mdl-typography--subhead">Payment</h2>' .
        '<div style="margin-left: 20px;">' .
          '<p class="mdl-typography--body-1">' .
            'Payment Method:' .
            '<span class="mdl-typography--font-bold">Card</span>' .
            '<br>' .
            'Card Number:' .
            '<span class="mdl-typography--font-bold">' .
              'xxxxxxxxxxxx' . $lastCardDigits .
            '</span>' .
          '</p>' .
        '</div>' .
        '<h2 class="mdl-typography--subhead">Collection</h2>' .
        '<div style="margin-left: 20px;">' .
          '<p class="mdl-typography--body-1">' .
            'Please come the branch specified on the collection day and show your' .
            'order number to the cashier. For further details please read our ' .
            '<a href="' . $faqUrl . '">FAQ</a> page.' .
            '<br>' .
            'Branch to collect from:' .
            '<span class="mdl-typography--font-bold">' .
              $branchName .
            '</span>' .
            '<br>' .
            'Estimated collection date:' .
            '<span class="mdl-typography--font-bold">' .
              $collectionTime .
            '</span>' .
          '</p>' .
        '</div>' .
      '</div>' .
    '</body>' .
    '</html>'
  );
}

/**
 * Send an email of an invoice to a customer.
 */
function sendInvoiceEmail($customerEmail, $orderID, $invoiceData): bool
{
  $toEmail = $customerEmail;
  $subject = "Invoice of your order from Neat Treats";
  $body = getInvoiceEmailContents($orderID, $invoiceData);
  $headers = ("From: neattreats.sender@gmail.com\r\n" .
    "Content-Type: text/html; charset=ISO-8859-1\r\n");

  return mail($toEmail, $subject, $body, $headers);
}

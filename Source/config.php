<?php
/*
This file defines several constants used throughout the
website. This page (config.php) should be included in all
pages by using the following code:

<?php require_once("../config.php"); ?>

The path should contain the relative path to this file.

*/

// require_once("common/db_connect.php");

/**
 * Absolute filepath of the root directory of Neat Treats website.
 * 
 * Should be prepended when `include()`ing files relative the the root
 * directory such as `include(SITE_ROOT . "folder/index.php")`.
 * 
 */
define("SITE_ROOT", __DIR__ . DIRECTORY_SEPARATOR);

/**
 * URL start for the root directory of Neat Treats website.
 * 
 * Should be prepended to the link to any resource to be hosted
 * by the Neat Treats website when sending HTML code.
 * 
 * For example, in an `a` element, the value of href could be: 
 * `HREF_ROOT . "images/cake_1.png"`.
 * 
 * @var string
 */
define(
  "HREF_ROOT",
  (function (): string {
    // Find the relative path of the website root folder from the htdocs folder.
    //
    // The htdocs folder is the root folder to be hosted by XAMPP and
    // requesting any webpages must use a URL with its path relative to it.
    // __DIR__ is the absolute system filepath of this file (config.php) which is
    // located in the root directory of this website.

    $scheme = "http";
    $host = $_SERVER["HTTP_HOST"]; // default is localhost

    // System specific directory path separator.
    $sep = DIRECTORY_SEPARATOR;
    // Split at a folder called exactly htdocs eg.../htdocs/...
    $htdocsDelimiter = "${sep}htdocs$sep";
    $path = str_replace($sep, "/", explode($htdocsDelimiter, __DIR__)[1] ?? "");

    // Return the base URL for all website resources.
    return "$scheme://$host/$path/";
  }
  )() // Call this anonymous function immediately, getting the value of the constant.
);

// echo HREF_ROOT."\n";
// echo $_SERVER["DOCUMENT_ROOT"];

/**
 * Status codes when initialising a page.
 */
class EInitPageStatus
{
  /** Page loaded without problems. */
  const SUCCESS = 0;

  /** User isn't logged in. */
  const NOT_LOGGED_IN = 1;

  /** User doesn't have necessary privileges to access this page. */
  const NOT_PERMITTED = 2;

  /** Unknown failure to load. */
  const MAX = 3;
}

/**
 * Status codes for a customer order.
 */
class EOrderStatus
{
  /** Order hasn't been baked yet. */
  const UNSTARTED = "UNSTARTED";

  /** Bakers have begun baking at least one item of the order. */
  const STARTED = "STARTED";

  /** All items are baked and ready for collection. */
  const FINISHED = "FINISHED";

  /** All items have been collected by the customer. */
  const COLLECTED = "COLLECTED";

  /** Manually cancelled by customer. May have already been started. */
  const CANCELLED = "CANCELLED";
}


// Staff roles (with corresponding role names in database)

/** Staff role as baker. */
define("STAFF_ROLE_BAKER", "Baker");
/** Staff role as cashier. */
define("STAFF_ROLE_CASHIER", "Cashier");
/** Staff role as manager. */
define("STAFF_ROLE_MANAGER", "Manager");
/** Staff role as admin. */
define("STAFF_ROLE_ADMIN", "root");


// Form errors (with corresponding error messages)

/** A required field did not contain anything. */
define("FORM_ERROR_FIELD_EMPTY", "Please fill in this field");
/** A numeric field contained a non-numeric value. */
define("FORM_ERROR_NOT_NUMBER", "Only numbers allowed");
/** Input was not in the set of allowed lookup values. */
define("FORM_ERROR_INVALID_OPTION", "Invalid option selected");

// Start session for every page just before sending HTML headers.
if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}

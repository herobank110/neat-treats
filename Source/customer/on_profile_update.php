<?php

/**
 * Processes a submitted Customer Profile Update form.
 */

require_once("../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");

function onSubmitCustomerProfileUpdate($inputArray)
{
  // Declare form local constants.
  /** Url the form came from to send back any errors. */
  $formPageUrl = HREF_ROOT . "customer/customer_profile.php?tabindex=account";
  /** Url the form intends to go if successful. */
  $landingPageUrl = HREF_ROOT . "customer/customer_profile.php?tabindex=account";

  /** Whether form has terminated with success. */
  $wasFormProcessSuccessful = false;

  $databaseLink = connectToNeatTreats("Customer", "Password123");
  $formErrors = array();

  // Sanitize and prepare inputs for validation.
  sanitizeFormInputArray($inputArray);
  // Extract each field as required.
  $title = $inputArray["title"] ?? "";
  $firstName = $inputArray["first_name"];
  $secondName = $inputArray["second_name"];
  $email = $inputArray["email"];
  $oldPassword = $inputArray["old_password"];
  $newPassword = $inputArray["new_password"];
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  $customerID = $_SESSION["customerID"] ?? NULL;

  // Only logged in customers can checkout.
  if (!isset($customerID)) {
    // Redirect to go login first.
    onFormFail($inputArray, HREF_ROOT . "customer/signin.php", $databaseLink);
    // Don't process any further errors.
    return;
  }

  // Validate title field.
  if (!isValidOption($title, ELookupOptions::TITLE)) {
    $formErrors["title"] = EFormErrors::NOT_IN_LOOKUP;
  }
  
  // Validate first name field.
  if (!matchesPattern($firstName, EInputPatterns::FIRST_NAME)) {
    $formErrors["first_name"] = EFormErrors::PATTERN_FAIL;
  }

  // Validate second name field.
  if (!matchesPattern($secondName, EInputPatterns::SECOND_NAME)) {
    $formErrors["second_name"] = EFormErrors::PATTERN_FAIL;
  }

  // Validate email field.
  if (!matchesPattern($email, EInputPatterns::EMAIL)) {
    $formErrors["email"] = EFormErrors::PATTERN_FAIL;
  } else if (isRowInDatabase(array("NOT CustomerID" => $customerID, "Email" => $email), $databaseLink, "Customer")) {
    $formErrors["email"] = EFormErrors::ALREADY_EXISTS;
  }

  if (isPresent($oldPassword) || isPresent($newPassword)) {
    // Customer intends to change their password.

    // Validate old password field.
    if (!isPresent($oldPassword)) {
      $formErrors["old_password"] = EFormErrors::EMPTY_FIELD;
    } else if (!isRowInDatabase(array("CustomerID" => $customerID, "Password" => $oldPassword), $databaseLink, "Customer", true)) {
      $formErrors["old_password"] = EFormErrors::DOESNT_MATCH_ROW;
    }
  
    // Validate new password field.
    if (!isPresent($newPassword)) {
      $formErrors["new_password"] = EFormErrors::EMPTY_FIELD;
    } else if (!isLengthBetween($newPassword, 8, 64)) {
      $formErrors["new_password"] = EFormErrors::LENGTH_MISMATCH(8, 64);
    } else if (!matchesPattern($newPassword, EInputPatterns::PASSWORD)) {
      $formErrors["new_password"] = EFormErrors::PATTERN_FAIL;
    } else if (areInputsIdentical($oldPassword, $newPassword)) {
      $formErrors["new_password"] = EFormErrors::REPEATED_INPUT;
    }
  }

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return;
  } else {
    // No errors, so add to database
    // If fields are empty, don't change the database.

    $personFields = "";
    if (isPresent($title)) {
      $personFields .= ($personFields ? ", " : "") . "Title = '$title'";
    }
    if (isPresent($firstName)) {
      $personFields .= ($personFields ? ", " : "") . "FirstName = '$firstName'";
    } 
    if (isPresent($secondName)) {
      $personFields .= ($personFields ? ", " : "") . "SecondName = '$secondName'";
    }

    $customerFields = "";
    if (isPresent($email)) {
      $customerFields .= ($customerFields ? ", " : "") . "Email = '$email'";
    }
    if (isPresent($newPassword)) {
      $customerFields .= ($customerFields ? ", " : "") . "Password = '$newPassword'";
    }

    $updatePersonQuery = "";
    if (isPresent($personFields)) {
      $updatePersonQuery = (
        "UPDATE Person " .
        "SET $personFields " .
        "WHERE PersonID IN (SELECT PersonID FROM Customer WHERE CustomerID = $customerID) " .
        "LIMIT 1;");
    }

    $updateCustomerQuery = "";
    if (isPresent($customerFields)) {
      $updateCustomerQuery = (
        "UPDATE Customer " .
        "SET $customerFields " .
        "WHERE CustomerID = $customerID " .
        "LIMIT 1;");
    }

    $databaseLink->query($updatePersonQuery);
    if (empty($databaseLink->error)) {
      $databaseLink->query($updateCustomerQuery);
      if (empty($databaseLink->error)) {
        // If the outcome was successful, set the form status.
        $wasFormProcessSuccessful = true;
      }
    } else {
      echo $databaseLink->error;
      return;
    }

  }

  if ($wasFormProcessSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Customer Profile Update</title>
</head>

<body>
  Please wait while you are redirected...
<?php onSubmitCustomerProfileUpdate($_POST); ?>
</body>

</html>

<?php

/**
 * Log out any logged in customer user.
 */

require_once("../config.php");
require_once(SITE_ROOT . "common/form_helper.php");

function onSubmitCustomerLogout()
{
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  // Check that any employee is logged in.
  $employeeID = $_SESSION["customerID"] ?? null;
  if (isset($employeeID)) {
    // Clear the session variables.
    unset($_SESSION["customerID"]);
  }

  // Redirect to login page.
  redirect(HREF_ROOT . "customer/signin.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Customer Logout</title>
</head>

<body>
  Please wait while you are logged out...
  <?php onSubmitCustomerLogout(); ?>
</body>

</html>

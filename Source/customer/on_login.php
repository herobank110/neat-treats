<?php

/**
 * Processes a submitted Customer Login Form.
 */

require_once("../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");

function onSubmitCustomerLogin()
{
  $formPageUrl = HREF_ROOT . "customer/signin.php?tabindex=login";  
  $landingPageUrl = HREF_ROOT . "customer/customer_profile.php";
  $formErrors = array();
  $emptyFieldError = "Please fill in this field";
  $databaseLink = connectToNeatTreats("Customer", "Password123");
  $wasLoginSuccessful = false;

  sanitizeFormInputArray($_POST);
  $email = $_POST["email"];
  $password = $_POST["password"];

  if (!isPresent($email)) {
    $formErrors["email"] = $emptyFieldError;
  }
  if (!isPresent($password)) {
    $formErrors["password"] = $emptyFieldError;
  }

  if (isPresent($email) && isPresent($password)) {
    if (!isRowInDatabase(array("Email" => $email, "Password" => $password), $databaseLink, "Customer", true)) {
      $formErrors["email"] = "Invalid login combination";
    }
  }

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $_POST, $formPageUrl, $databaseLink);
    return;
  } else {
    // No errors, so log in as a customer.
    
    $query = (
      "SELECT CustomerID FROM Customer " .
      "WHERE Email = '$email' " .
      "LIMIT 1;"
    );
    
    $result = $databaseLink->query($query);
    if (empty($databaseLink->error)) {
      $customerID = $result->fetch_assoc()["CustomerID"];
      $wasLoginSuccessful = true;

      // Save the customer id so other pages can recognise the logged in customer.s
      if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
      }
      $_SESSION["customerID"] = $customerID;
    }
  }

  if ($wasLoginSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return;
  } else {
    onFormFail($_POST, $formPageUrl, $databaseLink);
    return;
  }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Customer Login</title>
</head>

<body>
  Please wait while you are redirected...
  <?php onSubmitCustomerLogin(); ?>
</body>

</html>

<?php

/**
 * Webpage for Customer Profile page.
 */

require_once("../config.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "customer/database_views.php");


// Initialise global variables so the script can access them.
$formErrors = array();
$lastInput = array();
$ordersData = array();

if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}
switch(CustomerViews::initCustomerProfilePage($formErrors, $lastInput, $ordersData, $_SESSION)){
  case EInitPageStatus::SUCCESS:
    break;
  case EInitPageStatus::NOT_LOGGED_IN:
    redirect(HREF_ROOT . "customer/signin.php");
  case EInitPageStatus::MAX:
    // Go home since some unknown error occurred.
    echo "UNCOMMENT THIS LINE!";
    // redirect(HREF_ROOT);
}

/**
 * Get mdl-tab active status for a tab.
 *
 * @param string $tabName Name of this tab.
 * @param boolean $isDefaultTab Show this tab if url unspecified.
 * @return string Class name to be put in `mdl-tabs__tab` and `mdl-tabs__panel`.
 */
function getTabActiveStatus(string $tabName, $isDefaultTab = false): string
{
  $defaultTab = $isDefaultTab ? $tabName : "";
  $suppliedTab = $_GET["tabindex"] ?? $defaultTab;
  if ($suppliedTab == $tabName) {
    return "is-active";
  } else if (!isValidOption($suppliedTab, ELookupOptions::CUSTOMER_PROFILE_TABS)) {
    return $isDefaultTab ? "is-active" : "";
  }
  return "";
}

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Customer Profile</title>

  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <script src=<?php echo HREF_ROOT . "assets/scripts/cookie_helper.js"; ?>></script>
  <script src=<?php echo HREF_ROOT . "assets/scripts/cancel_menu.js"; ?>></script>
  <script src=<?php echo HREF_ROOT . "customer/customer_profile.js"; ?>></script>
</head>

<body>

  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>

  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div>
          <h1 class="mdl-typography--headline">
            Welcome, <?php echo $lastInput["first_name"]; ?>
          </h1>
        </div>

        <div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
          <div class="mdl-tabs__tab-bar" style="justify-content: left;">
            <a href="#orders-panel" class="mdl-tabs__tab <?php echo getTabActiveStatus("orders", true); ?>">Orders</a>
            <a href="#account-panel" class="mdl-tabs__tab <?php echo getTabActiveStatus("account") ?>">Account</a>
            <a href="#settings-panel" class="mdl-tabs__tab <?php echo  getTabActiveStatus("settings"); ?>">Settings</a>
          </div>

          <div class="mdl-tabs__panel <?php echo getTabActiveStatus("orders", true); ?>" id="orders-panel">
            <ul class="mdl-list card-holder--customer-orders">
              <?php echo CustomerOutput::makeOrderCards($ordersData); ?>
            </ul>
          </div>
          <div class="mdl-tabs__panel <?php echo getTabActiveStatus("account"); ?>" id="account-panel">

            <p class="mdc-typography--headline5" style="margin-top: 40px; margin-left:20px;">
              Account Information
            </p>
            <div style="margin-left: 40px;">

              <form action="<?php echo HREF_ROOT . "customer/on_profile_update.php"; ?>" method="post">
                <fieldset>
                  <legend class="mdl-typography--subhead">Personal Information</legend>

                  <!-- Title -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "title"); ?>">
                    <label class="mdl-textfield__label" for="title">Title</label>
                    <select class="mdl-textfield__input" id="title" name="title" title="Please pick a title from the available options" style="appearance: button; ">
                      <option disabled selected></option>
                      <option value="mr" <?php echo getLastSelectedState($lastInput, "title", "mr"); ?>>Mr</option>
                      <option value="mrs" <?php echo getLastSelectedState($lastInput, "title", "mrs"); ?>>Mrs</option>
                      <option value="miss" <?php echo getLastSelectedState($lastInput, "title", "miss"); ?>>Miss</option>
                      <option value="ms" <?php echo getLastSelectedState($lastInput, "title", "ms"); ?>>Ms</option>
                      <option value="dr" <?php echo getLastSelectedState($lastInput, "title", "dr"); ?>>Dr</option>
                    </select>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "title", "Select a valid option"); ?></span>
                  </div>

                  <br><br>

                  <!-- First name -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "first_name"); ?>">
                    <input class="mdl-textfield__input" type="text" name="first_name" id="first_name" pattern="[A-Z,a-z,0-9]*" title="Please enter your name" value="<?php echo getLastInput($lastInput, "first_name"); ?>">
                    <label class="mdl-textfield__label" for="first_name">First Name</label>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "first_name", "Letters and numbers only"); ?></span>
                  </div>

                  <br><br>

                  <!-- Second name -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "second_name"); ?>">
                    <input class="mdl-textfield__input" type="text" name="second_name" id="second_name" pattern="[A-Z,a-z,0-9]*" title="Please enter your name" value="<?php echo getLastInput($lastInput, "second_name"); ?>">
                    <label class="mdl-textfield__label" for="second_name">Second Name</label>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "second_name", "Letters and numbers only"); ?></span>
                  </div>

                </fieldset>

                <br><br>

                <fieldset>
                  <legend class="mdl-typography--subhead">Account Information</legend>

                  <!-- Email -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "email"); ?>">
                    <input class="mdl-textfield__input" type="email" id="email" name="email" pattern="[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*" title="Enter an email you have access to" value="<?php echo getLastInput($lastInput, "email"); ?>">
                    <label class="mdl-textfield__label" for="email">Email Address</label>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "email", "Valid email address only"); ?></span>
                  </div>

                  <br><br>

                  <!-- Old Password -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "old_password"); ?>">
                    <input class="mdl-textfield__input" id="old_password" name="old_password" type="password" optional title="Enter your existing password" pattern="*" value="<?php echo getLastInput($lastInput, "old_password"); ?>">
                    <label class="mdl-textfield__label" for="old_password">Old Password</label>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "old_password"); ?></span>
                  </div>

                  <br><br>

                  <!-- New Password -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "new_password"); ?>">
                    <input class="mdl-textfield__input" id="new_password" name="new_password" type="password" optional title=" Enter a password with at least 8 small and capital letters, numbers and punctuation" pattern="*" value="<?php echo getLastInput($lastInput, "new_password"); ?>">
                    <label class="mdl-textfield__label" for="new_password">New Password</label>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "new_password", "Needs at least 8 small and capital letters, numbers and punctuation"); ?></span>

                  </div>

                </fieldset>

                <br><br>

                <!-- "submit" button for form -->
                <div>
                  <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary">
                    Submit
                  </button>
                </div>

              </form>

            </div>
          </div>
          <div class="mdl-tabs__panel <?php echo getTabActiveStatus("settings"); ?>" id="settings-panel">
            <div style="margin-top: 40px; display:flex; flex-direction:row; align-items:center;">
              <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent mdc-theme--secondary" onclick="showCancelMenu('cancel-menu__account');">
                Delete my account
              </button>
              <div style="width:20px;"></div>
              <div class="cancel-menu mdl-typography--body-1" id="cancel-menu__account">
                <span>Are you sure?</span>
                <a href="<?php echo HREF_ROOT . "customer/on_profile_delete.php?customer_id=$_SESSION[customerID]"; ?>">Yes</a>
                <a onclick="hideCancelMenu('cancel-menu__account');">No</a>
              </div>
            </div>
          </div>
        </div>


        <div style="height:80px;"></div>

        <a href="<?php echo HREF_ROOT . "customer/on_logout.php"; ?>" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--primary">
          Log out
        </a>

      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js"; ?>></script>
  <!--endregion -->
</body>

</html>
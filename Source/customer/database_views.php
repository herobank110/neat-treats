<?php

/**
 * Script for querying the database for customers.
 */

require_once(SITE_ROOT . "common/db_connect.php");


/**
 * Get current information for the Customer Profile account tab.
 *
 * @return array Associative array of field names with values.
 */
function getCustomerAccountTabInfo($customerID) : array
{
  $databaseLink = connectToNeatTreats("Customer", "Password123");
  if ($databaseLink != null) {
    $query = (
      "SELECT Title, FirstName, SecondName, Email FROM Customer " .
      "LEFT JOIN Person ON Person.PersonID = Customer.PersonID " .
      "WHERE CustomerID = $customerID " .
      "LIMIT 1;");
    $result = $databaseLink->query($query);
    if (empty($databaseLink->error)) {
      return $result->fetch_assoc();
    }
  }
  return array();
}

/**
 * Return whether customer is signed in or not.
 */
function getIsLoggedIn($sessionArray, $databaseLink): bool
{ 
  $customerID = $sessionArray["customerID"] ?? NULL;
  if ($customerID != NULL) {
    return isInDatabase($customerID, $databaseLink, "Customer", "CustomerID");
  }
  return false;
}

/**
 * Contains function to query the database for the customer.
 */
class CustomerViews
{

  /**
   * Get orders data from the database for 
   *
   * @param [type] $customerID Customer who made these orders.
   * @param array $outOrdersData Array of data as outputs. May be empty.
   * @param mysqli $databaseLink Connection to the database. Must be
   * able to SELECT from CustomerOrder table.
   * @return boolean Whether query was successful.
   */
  public static function getOrderCardsData(
    $customerID,
    &$outOrdersData,
    mysqli $databaseLink
  ): bool {

    $query = (
      "SELECT OrderID, SubTotal, OrderTime, Status FROM CustomerOrder " .
      "WHERE CustomerID = $customerID;");

    $result = $databaseLink->query($query);
    if (!empty($databaseLink->error)) {
      // An error in query, return unsuccessfully.
      $outOrdersData = array();
      return false;
    }

    // Enumerate the rows into the output array.
    $rowIndex = 0;
    while ($row = $result->fetch_assoc()) {
      $outOrdersData[$rowIndex] = $row;
      $rowIndex++;
    }
    // Return succesful status.
    return true;
  }

  /**
   * Load data for customer profile page.
   * 
   * @param array $outFormErrors Data of form errors.
   * @param array $outLastInput Data of last form inputs or current
   * database values if a field is empty.
   * @param array $outOrdersData Data of orders for making order cards.
   * @param array $sessionArray Value of $_SESSION value.
   * @return int Status of loading page.
   */
  public static function initCustomerProfilePage(
    &$outFormErrors,
    &$outLastInput,
    &$outOrdersData,
    $sessionArray
  ): int {
    $outFormErrors = array();
    $outLastInput = array();
    $outOrdersData = array();

    $databaseLink = connectToNeatTreats("Customer", "Password123");

    $isLoggedIn = getIsLoggedIn($sessionArray, $databaseLink);
    if (!$isLoggedIn) {
      // Must be logged in to access this page.
      $databaseLink->close();
      return EInitPageStatus::NOT_LOGGED_IN;
    }

    // Otherwise get the id (we are logged in).
    $customerID = $sessionArray["customerID"];

    // Get form data to put into the outputs.
    $outFormErrors = loadFormErrors();
    $outLastInput = loadLastFormInput();
    $customerInfo = getCustomerAccountTabInfo($sessionArray["customerID"]);

    // Replace any empty fields with existing values.
    foreach ($customerInfo as $key => $value) {
      // Convert to snake_case for consistency with form field names.
      $key = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $key));
      if (empty($outLastInput[$key])) {
        $outLastInput[$key] = $value;
      }
    }

    // Get order data from the database.
    // We need: OrderID, Price, DatePlaced foreach order.
    if(!CustomerViews::getOrderCardsData($customerID, $outOrdersData, $databaseLink)) {
      $databaseLink->close();
      return EInitPageStatus::MAX;
    }

    $databaseLink->close();
    return EInitPageStatus::SUCCESS;
  }

  /**
   * Delete a customer's records from the database
   *
   * @param [type] $customerID Employee to be deleted.
   * @param mysqli $databaseLink Connection to database. Must be able to
   * DELETE from Person and Customer tables. The customer can't do this,
   * so make sure to log in as the admin temporarily if everything else is
   * valid.
   * @return boolean Whether deletion was successful.
   */
  public static function deleteCustomerProfile(
    $customerID,
    mysqli $databaseLink
  ): bool
  {
    // Start by deleting all their orders - these depend on CustomerID.
    $ordersQuery = "DELETE FROM CustomerOrder WHERE CustomerID = $customerID;";
    $wasOrdersSuccessful = $databaseLink->query($ordersQuery);
    if (empty($databaseLink->error) && $wasOrdersSuccessful) {

      // CustomerID depends on PersonID so get the value before removing it!
      $getPersonIDQuery = "SELECT PersonID FROM Customer WHERE CustomerID = $customerID";
      $getPersonIDResult = $databaseLink->query($getPersonIDQuery);
      if (empty($databaseLink->error) && $getPersonIDResult->num_rows > 0) {

        $personID = $getPersonIDResult->fetch_assoc()["PersonID"];

        // Now that we have the PersonID and nothing depends on CustomerID,
        // we can safely delete the customer account row.
        $customerQuery = "DELETE FROM Customer WHERE CustomerID = $customerID;";
        $wasCustomerSuccessful = $databaseLink->query($customerQuery);
        if (empty($databaseLink->error) && $wasCustomerSuccessful) {


          // Finally delete the PersonID row that was waiting for CustomerID to go.
          $personQuery = "DELETE FROM Person WHERE PersonID = $personID;";
          $wasPersonSuccessful = $databaseLink->query($personQuery);
          if (empty($databaseLink->error) && $wasPersonSuccessful) {
            return true;
          }
        }
      }
    }
    return false;
  }

  /**
   * Cancel an order in the database.
   *
   * @param [type] $orderID Order to cancel.
   * @param mysqli $databaseLink Connection to database. Must be able to
   * UPDATE CustomerOrder table.
   * @return boolean Whether cancellation was successful. True if order
   * was not in database.
   */
  public static function cancelOrder(
    $orderID,
    mysqli $databaseLink
  ): bool
  {
    $cancelledStatus = EOrderStatus::CANCELLED;
    $query = (
      "UPDATE CustomerOrder " .
      "SET Status = '$cancelledStatus' " .
      "WHERE OrderID = $orderID " .
      "LIMIT 1;"
    );

    $result = $databaseLink->query($query);
    if (empty($databaseLink->error) && $result) {
      // Query success or order not in database.
      return true;
    }

    // Failed to execute query.
    return false;
  }
}

/**
 * Contains functions to display data obtained by CustomerViews.
 */
class CustomerOutput
{

  /**
   * Return HTML code for a single customer order card.
   *
   * @param array $orderData Associative array of data to create card
   * with. Should contains keys: OrderID, SubTotal, OrderTime.
   * @return string HTML Code
   */
  public static function makeOrderCard(array $orderData): string
  {
    $orderID = $orderData["OrderID"];
    $subTotal = "£" . number_format($orderData["SubTotal"], 2);
    // Show order status in Title case.
    $status = ucfirst(strtolower($orderData["Status"]));

    // Allow cancellation in a few stages of the baking process.
    $canCancel = isValidOption($orderData["Status"], ELookupOptions::CANCELLABLE_STATUS);

    // HTML id for the entire order card.
    $cardID = "order-card__$orderID";
    // HTML id for the cancel menu (if created).
    $cancelMenuID = "order-card-cancel-menu__$orderID";

    $invoiceUrl = HREF_ROOT . "shop/invoice.php?order_id=$orderID";
    $cancelUrl = HREF_ROOT . "shop/on_cancel_order.php?order_id=$orderID";

    // Contains full timestamp
    $orderTime = $orderData["OrderTime"];
    // format like: May 24, 2016 (only show date, not time)
    $datePlaced = date_format(date_create($orderTime), "F j, Y");

    return (
      '<li class="mdl-list__item">' .
        '<div class="order-card mdc-card mdc-card--outlined" id="' . $cardID . '">' .
          '<div class="mdl-card__title mdl-card--expand">' .
            '<h4 class="mdl-typography--headline">' .
              'Order #' . $orderID .
              '<p class="order-card-info__basic mdl-typography--body-1">' .
                'Total Price: ' . $subTotal . '<br>' .
                'Order Placed: ' . $datePlaced .
              '</p>' .
              '<p class="order-card-info__additional mdl-typography--body-1">' .
                'Status: ' . $status . '<br>' .
                '<a href="' . $invoiceUrl . '">View Invoice</a>' .
              '</p>' .
              '<a onclick="toggleOrderCardSize(' . $orderID . ');" class="order-card__toggle-size mdl-typography--body-1" id="order-card-size-toggle__' . $orderID . '">' .
                'More' .
              '</a>' .
            '</h4>' .
          '</div>' .
          ($canCancel ?
          '<div class="mdl-card__actions">' .
            '<button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent mdc-theme--secondary" onclick="showCancelMenu(\'' . $cancelMenuID . '\');">' .
              'Cancel order' .
            '</button>' .
            '<div class="mdl-layout-spacer"></div>' .
            '<div class="cancel-menu order-cancel-menu mdl-typography--body-1" id="' . $cancelMenuID . '">' .
              '<span>Are you sure? </span>' .
              '<a href="' . $cancelUrl . '"> Yes </a>' .
              '<a onclick="hideCancelMenu(\'' . $cancelMenuID . '\');"> No</a>' .
            '</div>' .
          '</div>' : "") .
        '</div>' .
      '</li>');
  }

  /**
   * Return HTML code for customer order cards.
   *
   * @param array $ordersData Data to make cards with.
   * @return string HTML output.
   */
  public static function makeOrderCards(array $ordersData): string
  {
    $returnHTML = "";
    foreach ($ordersData as $order) {
      $returnHTML .= CustomerOutput::makeOrderCard($order);
    }
    if (empty($returnHTML)) {
      // Return a special message if there's nothing to display.
      return '<p>No orders yet - go buy something!</p>';
    }
    return $returnHTML;
  }
}
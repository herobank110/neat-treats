/**
 * Javascript functions for Customer Profile page.
 */

/**
 * Toggle size of order card between expanded and collapsed.
 */
function toggleOrderCardSize(orderID) {
  var orderCard = document.getElementById("order-card__" + orderID);
  if (orderCard == null) {
    alert("order-card__" + orderID);
    return;
  }

  var sizeToggler = document.getElementById("order-card-size-toggle__" + orderID);

  if (orderCard.classList.contains("is-expanded")) {
    // If already expanded, collapse it.
    orderCard.classList.remove("is-expanded");
    if (sizeToggler) {
      sizeToggler.innerHTML = "More";
    }
  } else {
    // If already collapsed, expand it.
    orderCard.classList.add("is-expanded");
    if (sizeToggler) {
      sizeToggler.innerHTML = "Less";
    }
  }
}

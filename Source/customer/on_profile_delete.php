<?php

/**
 * Processes a submitted Delete Customer Account form.
 */

require_once("../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "customer/database_views.php");
require_once(SITE_ROOT . "staff/database_views.php");

function onSubmitDeleteCustomerAccount($inputArray, $sessionArray)
{
  /** Whether form has terminated with success. */
  $wasFormProcessSuccessful = false;

  // Only admin user can delete customer account.
  $databaseLink = connectToNeatTreats("root", "");
  $formErrors = array();  

  // Sanitize and prepare inputs for validation.
  sanitizeFormInputArray($inputArray);
  // Extract each field as required.
  $customerID = $inputArray["customer_id"] ?? "";

  // To allow the deletion, admin user must be logged in or the same
  // customer must be logged in.

  $isAdmin = isEmployeeLoggedIn($sessionArray, STAFF_ROLE_ADMIN);
  $isCustomer = isset($sessionArray["customerID"]);
  if (!$isAdmin && !$isCustomer) {
    onFormFail($inputArray, HREF_ROOT, $databaseLink);
  }

  // Declare form local constants.
  /** Url the form came from to send back any errors. */
  $formPageUrl = HREF_ROOT . ($isAdmin ? "staff/admin" : "customer/customer_profile.php");
  /** Url the form intends to go if successful. */
  $landingPageUrl = HREF_ROOT . ($isAdmin ? "staff/admin/delete_success.php" : "customer/profile_deleted_confirm.php");

  // Validate customerID field.
  if (!isPresent($customerID)) {
    $formErrors["customer_id"] = EFormErrors::EMPTY_FIELD;
  } elseif (!isInDatabase($customerID, $databaseLink, "Customer", "CustomerID")) {
    $formErrors["customer_id"] = EFormErrors::DOESNT_EXIST;
  }

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return false;
  } else {
    // No errors, so remove from database

    if (CustomerViews::deleteCustomerProfile($customerID, $databaseLink)) {
      // If the outcome was successful, set the form status.
      $wasFormProcessSuccessful = true;
    }
  }

  if ($wasFormProcessSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return true;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return false;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Delete Customer Account</title>
</head>

<body>
  Please wait while you are redirected...
  <?php
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  if (onSubmitDeleteCustomerAccount($_GET, $_SESSION)); {
    // Finally log out of the customer's account
    unset($_SESSION["customerID"]);
  }

  ?>
</body>

</html>
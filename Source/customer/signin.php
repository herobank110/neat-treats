<?php
// Update this path to match the relative path of this page.
require_once("../config.php");
require_once(SITE_ROOT . "common/form_helper.php");

$formErrors = loadFormErrors();
$lastInput = loadLastFormInput();

if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Login</title>
  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">

      <p class="mdc-typography--headline3" style="margin-top:0;">
        Sign in
      </p>

      <div style="min-height:500px;">

        <div class="mdc-card mdc-theme--secondary-bg mdc-theme--on-secondary" style="position:relative;width:350px;padding:10px 0;margin-bottom:50px;">
          <ul class="navbar-root mdc-card__media">
            <li class="navbar-item">
              <button class="mdc-button mdc-button--raised" onclick="setVisibleTab('login-tab')">
                <span class="mdc-typography--button">LOG IN</span>
              </button>
            </li>
            <li class="navbar-item">
              <button class="mdc-button mdc-button--raised" onclick="setVisibleTab('register-tab')">
                <span class="mdc-typography--button">REGISTER</span>
              </button>
            </li>
          </ul>
        </div>


        <div>
          <div id="login-tab">
            <p class="mdc-typography--headline5">
              Login
            </p>
            <div style="margin-left: 20px;">

              <form action="on_login.php" method="post">

                <!-- Email Address -->
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "email"); ?>">
                  <input class="mdl-textfield__input" type="text" id="email" name="email" title="Enter the email address you registered with" value="<?php echo getLastInput($lastInput, "email"); ?>">
                  <label class="mdl-textfield__label" for="email">Email Address</label>
                  <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "email", "Could not find in database"); ?></span>
                </div>

                <br><br>

                <!-- Password -->
                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "password"); ?>">
                  <input class="mdl-textfield__input" type="password" id="password" name="password" title="Enter your chosen password" value="<?php echo getLastInput($lastInput, "password"); ?>">
                  <label class="mdl-textfield__label" for="password">Password</label>
                  <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "password"); ?></span>
                </div>

                <br><br>

                <!-- "submit" button for form -->
                <div>
                  <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary">
                    Submit
                  </button>
                </div>

              </form>
            </div>
          </div>

          <div id="register-tab">
            <p class="mdc-typography--headline5">
              Register
            </p>
            <div style="margin-left: 20px;">

              <form action="<?php echo HREF_ROOT . "customer/on_register.php"; ?>" method="post">
                <fieldset>
                  <legend class="mdl-typography--subhead">Personal Information</legend>

                  <!-- Title -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "title"); ?>">
                    <label class="mdl-textfield__label" for="title">Title</label>
                    <select class="mdl-textfield__input" id="title" name="title" title="Please pick a title from the available options" style="appearance: button; ">
                      <option disabled selected></option>
                      <option value="mr" <?php echo getLastSelectedState($lastInput, "title", "mr"); ?>>Mr</option>
                      <option value="mrs" <?php echo getLastSelectedState($lastInput, "title", "mrs"); ?>>Mrs</option>
                      <option value="miss" <?php echo getLastSelectedState($lastInput, "title", "miss"); ?>>Miss</option>
                      <option value="ms" <?php echo getLastSelectedState($lastInput, "title", "ms"); ?>>Ms</option>
                      <option value="dr" <?php echo getLastSelectedState($lastInput, "title", "dr"); ?>>Dr</option>
                    </select>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "title", "Select a valid option"); ?></span>
                  </div>

                  <br><br>

                  <!-- First name -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "first_name"); ?>">
                    <input class="mdl-textfield__input" type="text" name="first_name" id="first_name" pattern="[A-Z,a-z,0-9]*" title="Please enter your name" value="<?php echo getLastInput($lastInput, "first_name"); ?>">
                    <label class="mdl-textfield__label" for="first_name">First Name</label>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "first_name", "Letters and numbers only"); ?></span>
                  </div>

                  <br><br>

                  <!-- Second name -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "second_name"); ?>">
                    <input class="mdl-textfield__input" type="text" name="second_name" id="second_name" pattern="[A-Z,a-z,0-9]*" title="Please enter your name" value="<?php echo getLastInput($lastInput, "second_name"); ?>">
                    <label class="mdl-textfield__label" for="second_name">Second Name</label>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "second_name", "Letters and numbers only"); ?></span>
                  </div>

                </fieldset>

                <br><br>

                <fieldset>
                  <legend class="mdl-typography--subhead">Account Information</legend>

                  <!-- Email -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "email"); ?>">
                    <input class="mdl-textfield__input" type="email" id="email" name="email" pattern="[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*" title="Enter an email you have access to" value="<?php echo getLastInput($lastInput, "email"); ?>">
                    <label class="mdl-textfield__label" for="email">Email Address</label>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "email", "Valid email address only"); ?></span>
                  </div>

                  <br><br>

                  <!-- Password -->
                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "password"); ?>">
                    <input class="mdl-textfield__input" type="password" name="password" id="password" pattern="(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*£]).{8,}" title="Passwords are case sensitive" value="<?php echo getLastInput($lastInput, "password"); ?>">
                    <label class="mdl-textfield__label" for="password">Password</label>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "password", "Must have 8 numbers, letters and punctuation characters"); ?></span>
                  </div>

                  <br><br>
                  

                  <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "password_confirm") ?>">
                    <input class="mdl-textfield__input" type="password" name="password_confirm" id="password_confirm" title="Please repeat your chosen password" value="<?php echo getLastInput($lastInput, "password_confirm"); ?>">
                    <label class="mdl-textfield__label" for="password_confirm">Confirm Password</label>
                    <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "password_confirm", "Passwords must match") ?></span>
                  </div>

                </fieldset>

                <br><br>

                <!-- "submit" button for form -->
                <div>
                  <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary">
                    Submit
                  </button>
                </div>

              </form>

            </div>
          </div>
        </div>



      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <script src="<?php echo HREF_ROOT . "assets/scripts/tab_sections.js"; ?>"></script>
  <script type='text/javascript'>
    addTabToGroup("login-tab");
    addTabToGroup("register-tab");
    <?php
      switch ($_GET["tabindex"] ?? "") {
        case "login":
          echo "setVisibleTab('login-tab')";
          break;
        case "register":
          echo "setVisibleTab('register-tab')";
          break;
        
        default:
          echo "setVisibleTab('login-tab')";
          break;
      };
    ?>
  </script>
  <!--endregion -->
</body>

</html>
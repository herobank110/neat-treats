<?php

/**
 * Processes a submitted Customer Registration Form.
 */

require_once("../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");

function onSubmitCustomerRegister()
{
  $titleOptions = array("mr", "mrs", "miss", "ms", "dr");
  $firstNamePattern = "/[A-Z,a-z,0-9]*/";
  $secondNamePattern = "/[A-Z,a-z,0-9]*/";
  $emailPattern = "/[a-zA-Z0-9.!#$%&’*+=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*/";
  $passwordPattern = "/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*£]).{8,}/";

  $formErrors = array();
  $emptyFieldError = "Please fill in this field";
  $databaseLink = connectToNeatTreats("Customer", "Password123");


  sanitizeFormInputArray($_POST);

  $title = $_POST["title"] ?? "";
  $firstName = $_POST["first_name"];
  $secondName = $_POST["second_name"];
  $email = $_POST["email"];
  $password = $_POST["password"];
  $passwordConfirm = $_POST["password_confirm"];

  if (!isValidOption($title, $titleOptions)) {
    $formErrors["title"] = "Invalid option selected";
  }

  if (!isPresent($firstName)) {
    $formErrors["first_name"] = $emptyFieldError;
  } else {
    if (!matchesPattern($firstName, $firstNamePattern)) {
      $formErrors["first_name"] = "Letters and numbers only";
    }
  }

  if (!isPresent($secondName)) {
    $formErrors["second_name"] = $emptyFieldError;
  } else {
    if (!matchesPattern($secondName, $secondNamePattern)) {
      $formErrors["second_name"] = "Letters and numbers only";
    }
  }

  if (!isPresent($email)) {
    $formErrors["email"] = $emptyFieldError;
  } else {
    if (!matchesPattern($email, $emailPattern)) {
      $formErrors["email"] = "Invalid email address";
    }
    if (isInDatabase($email, $databaseLink, "Customer", "Email")) {
      $formErrors["email"] = "Email already in use";
    }
  }

  if (!isPresent($password)) {
    $formErrors["password"] = $emptyFieldError;
  } else {
    if (!matchesPattern($password, $passwordPattern)) {
      $formErrors["password"] = "Needs at least 8 small and capital letters, numbers and punctuation";
    }
  }

  if (!isPresent($passwordConfirm)) {
    $formErrors["password_confirm"] = $emptyFieldError;
  } else {
    if (!areInputsIdentical($password, $passwordConfirm)) {
      $formErrors["password_confirm"] = "Passwords must match";
    }
  }

  if (count($formErrors) > 0) {
    // Invalid attempt to register customer.

    $databaseLink->close();
    setcookie("errors", json_encode($formErrors), time() + 60 * 60 * 24 * 3);
    setcookie("lastInput", json_encode($_POST), time() + 60 * 60 * 24 * 3);
    redirect(HREF_ROOT . "customer/signin.php?tabindex=register");
    return;
  } else {
    // Valid inputs to register customer.

    $wasInsertionSuccessful = false;
    // Add personal data to database.
    $query = ("INSERT INTO Person (Title, FirstName, SecondName) " .
              "VALUES ('$title', '$firstName', '$secondName');");
    $databaseLink->query($query);
    if (empty($databaseLink->error)) {
      // Add account information to database.
      $addedPersonID = $databaseLink->insert_id;
      $query = ("INSERT INTO Customer(PersonID, Email, Password) " .
                "VALUES ('$addedPersonID', '$email', '$password');");
      $databaseLink->query($query);
      if (empty($databaseLink->error)) {
        $addedCustomerID = $databaseLink->insert_id;
        $wasInsertionSuccessful = true;
      } else {
        // Failed to add customer information.
        // Delete the newly added personal information.
        $databaseLink->query("DELETE FROM Person WHERE PersonID = $addedPersonID");
      }
    }
    $databaseLink->close();

    if ($wasInsertionSuccessful) {
      // Automatically log in as customer.
      if (session_status() != PHP_SESSION_ACTIVE) {
        session_start();
      }
      $_SESSION["customerID"] = $addedCustomerID;
      redirect(HREF_ROOT . "customer/customer_profile.php");
      return;
    } else {
      // Failed to insert into database.
      redirect(HREF_ROOT . "customer/signin.php?tabindex=register");
      return;
    }
  }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Registration</title>
</head>

<body>
  Please wait while you are redirected...
  <?php onSubmitCustomerRegister(); ?>
</body>

</html>
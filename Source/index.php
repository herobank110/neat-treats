<?php

/**
 * Webpage for the Neat Treats home page.
 */

require_once("config.php");

?>

<!DOCTYPE HTML>
<html>

<head>
  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Neat Treats</title>
</head>

<body>
  <!--Navigation Header-->
  <?php include "common/nav_header.php" ?>

  <!-- #region Page Content-->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Neat Treats
          </h1>
          <p class="mdl-typography--subhead">
            Welcome to our new website!
          </p>
        </div>

        <div style="margin-left: 20px;">
          <div class="home-link-container">
            <a href="<?php echo HREF_ROOT . "/shop/cake_profile.php?cakeID=10"; ?>">
              <p class="mdl-typography--body-2">
                Our Most Popular Cake
              </p>
              <div class="home-link__image" style="background-image: url(&quot;<?php echo HREF_ROOT . "assets/images/home_most_popular.png"; ?>&quot;);"></div>
            </a>
          </div>

          <div class="home-link-container">
            <a href="<?php echo HREF_ROOT . "shop/cakes.php"; ?>">
              <p class="mdl-typography--body-2">
                Browse All Cakes
              </p>
              <div class="home-link__image" style="background-image: url(&quot;<?php echo HREF_ROOT . "assets/images/home_browse_collection.png"; ?>&quot;);"></div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--endregion -->

  <!--Navigation Footer-->
  <?php include "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src="assets/scripts/mdc_page_setup.js"></script>
  <!--endregion -->

</body>

</html>

<?php

/**
 * Processes a submitted Staff Login Form.
 */

require_once("../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "staff/database_views.php");

function onSubmitStaffLogin()
{
  $formErrors = array();
  $emptyFieldError = "Please fill in this field";
  $databaseLink = connectToNeatTreats("root", "");

  sanitizeFormInputArray($_POST);
  $employeeID = $_POST["employee_id"];
  $passCode = $_POST["pass_code"];

  if (!isPresent($employeeID)) {
    $formErrors["employee_id"] = $emptyFieldError;
  }
  if (!isPresent($passCode)) {
    $formErrors["pass_code"] = $emptyFieldError;
  }

  if (isPresent($employeeID) && isPresent($passCode)) {
    if (!isRowInDatabase(array("EmployeeID" => $employeeID, "PassCode" => $passCode), $databaseLink, "Employee", true)) {
      $formErrors["employee_id"] = "Invalid login combination";
    }
  }

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $_POST, HREF_ROOT . "staff/login.php", $databaseLink);
    return;
  } else {
    // No errors, so log in as a staff member.

    $wasLoginSuccessful = false;
    
    $query = (
      "SELECT Role.Name as 'Role' FROM Employee " .
      "LEFT JOIN Role ON Employee.RoleID = Role.RoleID " .
      "WHERE Employee.EmployeeID = $employeeID;"
    );

    $result = $databaseLink->query($query);
    if (empty($databaseLink->error)) {
      $role = $result->fetch_assoc()["Role"];
      $wasLoginSuccessful = true;
    }
  }

  if ($wasLoginSuccessful) {
    if (session_status() != PHP_SESSION_ACTIVE) {
      session_start();
    }
    $_SESSION["employeeID"] = $employeeID;
    $_SESSION["role"] = $role;

    // Redirect to the landing page for their role.
    $landingPage = getStaffLandingPageUrl($role);

    redirect($landingPage);
    
    $databaseLink->close();
    return;
  } else {
    onFormFail($_POST, HREF_ROOT . "staff/login.php", $databaseLink);
    return;
  }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Staff Login</title>
</head>

<body>
  Please wait while you are redirected...
  <?php onSubmitStaffLogin(); ?>
</body>

</html>

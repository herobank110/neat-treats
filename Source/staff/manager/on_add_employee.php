<?php

/**
 * Processes a submitted Manager Employee Profile Update form.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");

function onSubmitManagerAddEmployee($inputArray, $sessionArray)
{
  // Declare form local constants.
  /** Url the form came from to send back any errors. */
  $formPageUrl = HREF_ROOT . "staff/manager/add_employee.php";
  /** Url the form intends to go if successful. */
  $landingPageUrl = HREF_ROOT . "staff/manager/employees.php";

  /** Whether form has terminated with success. */
  $wasFormProcessSuccessful = false;

  $databaseLink = connectToNeatTreats(STAFF_ROLE_MANAGER, "Password123");
  $formErrors = array();

  // Sanitize and prepare inputs for validation.
  sanitizeFormInputArray($inputArray);
  // Extract each field as required.
  $title = $inputArray["title"] ?? "";
  $firstName = $inputArray["first_name"];
  $secondName = $inputArray["second_name"];
  $role = $inputArray["role"] ?? "";
  $branch = $inputArray["branch"] ?? "";
  $startDate = $inputArray["start_date"];

  // Generate a random unique 4 digit employee id.
  do {
    $newEmployeeID = "";
    for ($i=0; $i < 4; $i++) { 
      // Make sure it can't start with a zero
      // (that would not be a 4 digit number!)
      $newEmployeeID .= (string) random_int(1, 9);
      // Ensure it isn't taken already.
    }
  } while (isInDatabase($newEmployeeID, $databaseLink, "Employee", "EmployeeID"));

  // Generate a random passcode of 8 [A-Z] characters.
  $passCode = "";
  for ($i=0; $i < 8; $i++) { 
    $passCode .= chr(random_int(65, 65 + 25));
  }

  #region validation

  // Only logged in managers can update employees.
  if (!isEmployeeLoggedIn($sessionArray, STAFF_ROLE_MANAGER)) {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }

  // Validate title field.
  if (!isValidOption($title, ELookupOptions::TITLE)) {
    $formErrors["title"] = EFormErrors::NOT_IN_LOOKUP;
  }

  // Validate first name field.
  if (!isPresent($firstName)) {
    $formErrors["first_name"] = EFormErrors::EMPTY_FIELD;
  } else if (!matchesPattern($firstName, EInputPatterns::FIRST_NAME)) {
    $formErrors["first_name"] = EFormErrors::PATTERN_FAIL;
  }

  // Validate second name field.
  if (!isPresent($secondName)) {
    $formErrors["second_name"] = EFormErrors::EMPTY_FIELD;
  } else if (!matchesPattern($secondName, EInputPatterns::SECOND_NAME)) {
    $formErrors["second_name"] = EFormErrors::PATTERN_FAIL;
  }

  // Validate role field.
  if (!isPresent($role)) {
    $formErrors["role"] = EFormErrors::EMPTY_FIELD;
  } else if (!isInDatabase($role, $databaseLink, "Role", "RoleID")) {
    $formErrors["role"] = EFormErrors::NOT_IN_LOOKUP;
  }

  // Validate branch field.
  if (!isPresent($branch)) {
    $formErrors["branch"] = EFormErrors::EMPTY_FIELD;
  } else if (!isInDatabase($branch, $databaseLink, "Branch", "BranchID")) {
    $formErrors["branch"] = EFormErrors::NOT_IN_LOOKUP;
  }

  // Validate start date field.
  if (!matchesPattern($startDate, EInputPatterns::DATE)) {
    $formErrors["start_date"] = EFormErrors::PATTERN_FAIL("YYYY-DD-MM");
  }

  #endregion validation

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return;
  } else {
    // No errors, so add to database
    // If the outcome was successful, set the form status.

    $personQuery = (
      "INSERT INTO Person " .
      "(Title, FirstName, SecondName) " .
      "VALUES " .
      "('$title', '$firstName', '$secondName');");

    $result = $databaseLink->query($personQuery);
    if (empty($databaseLink->error) && $result) {
      $newPersonID = $databaseLink->insert_id;

      $employeeQuery = (
        "INSERT INTO Employee " .
        "(EmployeeID, PersonID, RoleID, BranchID, StartDate, PassCode) " .
        "VALUES " .
        "('$newEmployeeID', '$newPersonID', '$role', '$branch', '$startDate', '$passCode');");

      $result = $databaseLink->query($employeeQuery);
      if (empty($databaseLink->error) && $result) {
        $wasFormProcessSuccessful = true;
      } else {
        // Remove the added person if the employee didn't go through.
        $databaseLink->query("DELETE FROM Person WHERE PersonID = $newPersonID");
      }
    }
  }
  
  // INSERT INTO Employee 
  // (EmployeeID, PersonID, RoleID, BranchID, StartDate, PassCode) 
  // VALUES 
  // (2284, '18', '3', '1', '2020-01-10', 'LMWZIXCV'); 


  if ($wasFormProcessSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Customer Profile Update</title>
</head>

<body>
  Please wait while you are redirected...
  <?php
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  onSubmitManagerAddEmployee($_POST, $_SESSION);
  ?>
</body>

</html>

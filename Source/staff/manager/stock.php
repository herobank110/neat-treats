<?php

/**
 * Webpage for Manager Stock page.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "staff/database_views.php");

// Initialise global variables so the script can access them.
$stockTableData = array();

if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}
switch (ManagerViews::initStockPage($stockTableData, $_SESSION)) {
  case EInitPageStatus::SUCCESS:
    // Successful loading page.
    break;
  case EInitPageStatus::NOT_LOGGED_IN:
    // User is not logged in, go to login page.
    redirect(HREF_ROOT . "staff/login.php");
  case EInitPageStatus::NOT_PERMITTED:
    // Use isn't permitted to access this page, go to their landing page.
    redirect(getStaffLandingPageUrl($_SESSION["role"]));
  default:
    // Fall through error, go to homepage.
    redirect(HREF_ROOT);
}

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Stock</title>
  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <script src=<?php echo HREF_ROOT . "assets/scripts/cancel_menu.js"; ?>></script>
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Manage Stock
          </h1>
          <p class="mdl-typography--subhead">
            Input the Expired Quantity and click Recalculate
          </p>
        </div>

        <div style="margin-left: 20px;">
          <!-- Create a form to submit the expired quantity columns. -->
          <form id="stock-form" method="post" action="<?php echo HREF_ROOT . "staff/manager/on_update_stock.php"; ?>">
            <!-- Create the stock table -->
            <?php echo ManagerOutput::makeStockTable($stockTableData); ?>
          </form>

          <div style="height:40px;"></div>

          <!-- Submit new expired quantity column -->
          <div style="display:flex; flex-direction:row; align-items:flex-start;">
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button-colored mdl-button--raised mdl-button--primary" onclick="showCancelMenu('cancel-menu__recalculate');">
              Recalculate
            </button>
            <div style="width:20px;"></div>
            <!-- Confirmation menu -->
            <div class="cancel-menu mdl-typography--body-1" id="cancel-menu__recalculate">
              <span>Any expired quantities will be subtracted from the database.<br>Are you sure? </span>
              <a href="#" onclick="document.getElementById('stock-form').submit();">Yes</a>
              <a onclick="hideCancelMenu('cancel-menu__recalculate');">No</a>
            </div>
          </div>

          <div style="height:80px;"></div>

          <!-- Back button -->
          <a href="<?php echo HREF_ROOT . "staff/manager"; ?>" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent mdc-theme--secondary">
            Cancel
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>
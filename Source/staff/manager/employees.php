<?php

/**
 * Webpage for Manager Employees page.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "staff/database_views.php");

if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}

$tableData = array();
switch (ManagerViews::initEmployeesPage($tableData, $_SESSION)) {
  case EInitPageStatus::SUCCESS:
    // Successful loading page.
    break;
  case EInitPageStatus::NOT_LOGGED_IN:
    // User is not logged in, go to login page.
    redirect(HREF_ROOT . "staff/login.php");
  case EInitPageStatus::NOT_PERMITTED:
    // Use isn't permitted to access this page, go to their landing page.
    redirect(getStaffLandingPageUrl($_SESSION["role"]));
  default:
    // Fall through error, go to homepage.
    redirect(HREF_ROOT);
}

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Employees</title>
  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Manage Employees
          </h1>
          <p class="mdl-typography--subhead">
            View and update your employees
          </p>
        </div>

        <div style="margin-left: 20px; overflow:auto;">
          <?php echo ManagerOutput::makeEmployeeTable($tableData); ?>
        </div>

        <!-- A small divider from the table. -->
        <div style="height: 80px"></div>

        <!-- Add Employee -->
        <a href="<?php echo HREF_ROOT . "staff/manager/add_employee.php"; ?>" title="Add an employee" class="mdl-button mdl-js-button mdl-button--fab mdl-button--colored mdl-button--accent" style="float:right;">
          <i class="material-icons">add</i>
        </a>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>
<?php

/**
 * Processes a submitted Manager Employee Profile Update form.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");

function onSubmitManagerEmployeeProfileUpdate($inputArray, $sessionArray)
{
  /** Whether form has terminated with success. */
  $wasFormProcessSuccessful = false;

  $databaseLink = connectToNeatTreats(STAFF_ROLE_MANAGER, "Password123");
  $formErrors = array();

  // Sanitize and prepare inputs for validation.
  sanitizeFormInputArray($inputArray);
  // Extract each field as required.
  $title = $inputArray["title"] ?? "";
  $firstName = $inputArray["first_name"];
  $secondName = $inputArray["second_name"];
  // Profiling the employee to be updating, not the logged in manager.
  $profiledID = $inputArray["employee_id"];
  $role = $inputArray["role"] ?? "";
  $branch = $inputArray["branch"] ?? "";
  $startDate = $inputArray["start_date"];
  $passCode = $inputArray["pass_code"];

  // Declare form local constants.
  /** Url the form came from to send back any errors. */
  $formPageUrl = HREF_ROOT . "staff/manager/employee_profile.php?employee_id=$profiledID";
  /** Url the form intends to go if successful. */
  $landingPageUrl = HREF_ROOT . "staff/manager/employee_profile.php?employee_id=$profiledID";

  #region validation

  // Only logged in managers can update employees.
  if (!isEmployeeLoggedIn($sessionArray, STAFF_ROLE_MANAGER)) {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }

  // Validate title field.
  if (!isValidOption($title, ELookupOptions::TITLE)) {
    $formErrors["title"] = EFormErrors::NOT_IN_LOOKUP;
  }

  // Validate first name field.
  if (!matchesPattern($firstName, EInputPatterns::FIRST_NAME)) {
    $formErrors["first_name"] = EFormErrors::PATTERN_FAIL;
  }

  // Validate second name field.
  if (!matchesPattern($secondName, EInputPatterns::SECOND_NAME)) {
    $formErrors["second_name"] = EFormErrors::PATTERN_FAIL;
  }

  // Validate employee id (the one we want to change) field.
  if (!isInDatabase($profiledID, $databaseLink, "Employee", "EmployeeID")) {
    $formErrors["employee_id"] = EFormErrors::DOESNT_EXIST;
  }

  // Validate role field.
  if (!isInDatabase($role, $databaseLink, "Role", "RoleID")) {
    $formErrors["role"] = EFormErrors::NOT_IN_LOOKUP;
  }

  // Validate branch field.
  if (!isInDatabase($branch, $databaseLink, "Branch", "BranchID")) {
    $formErrors["branch"] = EFormErrors::NOT_IN_LOOKUP;
  }

  // Validate start date field.
  if (!matchesPattern($startDate, EInputPatterns::DATE)) {
    $formErrors["start_date"] = EFormErrors::PATTERN_FAIL("YYYY-DD-MM");
  }

  // Validate pass code field.
  if (!isLengthExactly($passCode, 8)) {
    $formErrors["pass_code"] = EFormErrors::LENGTH_MISMATCH(8);
  } else if (!matchesPattern($passCode, EInputPatterns::PASS_CODE)) {
    $formErrors["pass_code"] = EFormErrors::PATTERN_FAIL("A-Z");
  }  

  #endregion validation

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return;
  } else {
    // No errors, so add to database
    // If fields are empty, don't change the database.

    $personFields = implode(", ", array(
      isPresent($title) ? "Title = '$title'" : "",
      isPresent($firstName) ? "FirstName = '$firstName'" : "",
      isPresent($secondName) ? "SecondName = '$secondName'" : "",
    ));

    $employeeFields = implode(", ", array(
      isPresent($role) ? "RoleID = '$role'" : "",
      isPresent($branch) ? "BranchID = '$branch'" : "",
      isPresent($startDate) ? "StartDate = '$startDate'" : "",
      isPresent($passCode) ? "PassCode = '$passCode'" : ""
    ));

    $updatePersonQuery = "";
    if (isPresent($personFields)) {
      $updatePersonQuery = ("UPDATE Person " .
        "SET $personFields " .
        "WHERE PersonID IN (SELECT PersonID FROM Employee WHERE EmployeeID = $profiledID) " .
        "LIMIT 1;");
    }

    $updateEmployeeQuery = "";
    if (isPresent($employeeFields)) {
      $updateEmployeeQuery = ("UPDATE Employee " .
        "SET $employeeFields " .
        "WHERE EmployeeID = $profiledID " .
        "LIMIT 1;");
    }

    if (isPresent($updatePersonQuery)) {
      $databaseLink->query($updatePersonQuery);
      if (empty($databaseLink->error) && isPresent($updateEmployeeQuery)) {
        $databaseLink->query($updateEmployeeQuery);
        if (empty($databaseLink->error)) {
          // If the outcome was successful, set the form status.
          $wasFormProcessSuccessful = true;
        }
      }
    }
  }

  if ($wasFormProcessSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Customer Profile Update</title>
</head>

<body>
  Please wait while you are redirected...
  <?php
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  onSubmitManagerEmployeeProfileUpdate($_POST, $_SESSION);
  ?>
</body>

</html>

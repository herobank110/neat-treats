<?php

/**
 * Processes a submitted Delete Employee Record request.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "staff/database_views.php");

function onSubmitDeleteEmployee($inputArray, $sessionArray)
{

  /** Whether form has terminated with success. */
  $wasFormProcessSuccessful = false;

  $databaseLink = connectToNeatTreats(STAFF_ROLE_MANAGER, "Password123");
  $formErrors = array();

  // Sanitize and prepare inputs for validation.
  sanitizeFormInputArray($inputArray);
  // Extract each field as required.
  $profiledID = $inputArray["employee_id"] ?? "";
  
  // Declare form local constants.
  $notLoggedInUrl = HREF_ROOT . "staff/login.php";

  // Make sure we are logged in as a manager.
  if (!isEmployeeLoggedIn($sessionArray, STAFF_ROLE_MANAGER)
    && !isEmployeeLoggedIn($sessionArray, STAFF_ROLE_ADMIN)
  ) {
    onFormFail($inputArray, $notLoggedInUrl, $databaseLink);
  }
  
  $role = $sessionArray["role"];
  $isAdmin = $role == STAFF_ROLE_ADMIN;
  // Declare more form local constants.
  /** Url the form came from to send back any errors. */
  $formPageUrl = HREF_ROOT . ($isAdmin ? "staff/admin" : "staff/manager/employees.php?employee_id=$profiledID");
  /** Url the form intends to go if successful. */
  $landingPageUrl = HREF_ROOT . ($isAdmin ? "staff/admin/delete_success.php" : "staff/manager/employees.php");

  // Validate profiledID field.
  if (!isPresent($profiledID)) {
    $formErrors["employee_id"] = EFormErrors::EMPTY_FIELD;
  } elseif (!isInDatabase($profiledID, $databaseLink, "Employee", "EmployeeID")) {
    $formErrors["employee_id"] = EFormErrors::DOESNT_EXIST;
  }

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return false;
  } else {
    // No errors, so remove from database


    if (ManagerViews::deleteEmployeeProfile($profiledID, $databaseLink)) {
      // If the outcome was successful, set the form status.
      $wasFormProcessSuccessful = true;
    }
  }

  if ($wasFormProcessSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Delete Employee Account</title>
</head>

<body>
  Please wait while you are redirected...
  <?php
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  onSubmitDeleteEmployee($_GET, $_SESSION);
  ?>
</body>

</html>

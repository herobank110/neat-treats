<?php

/**
 * Webpage for Manager Employee Profile page.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "staff/database_views.php");

// Initialise global variables so the script can access them.
$formErrors = array();
$lastInput = array();
$employeePageData = array();

if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}

switch (ManagerViews::initEmployeeProfilePage(
  $formErrors,
  $lastInput,
  $employeePageData,
  $_GET,
  $_SESSION
)) {
  case EInitPageStatus::SUCCESS:
    // Successful loading page.
    break;
  case EInitPageStatus::NOT_LOGGED_IN:
    // User is not logged in, go to login page.
    redirect(HREF_ROOT . "staff/login.php");
  case EInitPageStatus::NOT_PERMITTED:
    // Use isn't permitted to access this page, go to their landing page.
    redirect(getStaffLandingPageUrl($_SESSION["role"]));
  default:
    // Fall through error, go to homepage.
    redirect(HREF_ROOT);
}

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Employee Profile</title>
  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  
  <script src=<?php echo HREF_ROOT . "assets/scripts/cancel_menu.js"; ?>></script>
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Manage Employement Record
          </h1>
          <p class="mdl-typography--subhead">
            <?php echo ManagerOutput::makeEmployeeProfileSubhead($lastInput); ?>
          </p>
        </div>

        <div style="margin-left: 20px;">
          <form action="<?php echo HREF_ROOT . "staff/manager/on_profile_update.php"; ?>" method="post">
            <fieldset>
              <legend class="mdl-typography--subhead">Personal Information</legend>

              <!-- Title -->
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "title"); ?>">
                <label class="mdl-textfield__label" for="title">Title</label>
                <select class="mdl-textfield__input" id="title" name="title" title="Please pick a title from the available options" style="appearance: button; ">
                  <option disabled selected></option>
                  <option value="mr" <?php echo getLastSelectedState($lastInput, "title", "mr"); ?>>Mr</option>
                  <option value="mrs" <?php echo getLastSelectedState($lastInput, "title", "mrs"); ?>>Mrs</option>
                  <option value="miss" <?php echo getLastSelectedState($lastInput, "title", "miss"); ?>>Miss</option>
                  <option value="ms" <?php echo getLastSelectedState($lastInput, "title", "ms"); ?>>Ms</option>
                  <option value="dr" <?php echo getLastSelectedState($lastInput, "title", "dr"); ?>>Dr</option>
                </select>
                <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "title", "Select a valid option"); ?></span>
              </div>

              <br><br>

              <!-- First name -->
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "first_name"); ?>">
                <input class="mdl-textfield__input" type="text" name="first_name" id="first_name" pattern="[A-Z,a-z,0-9]*" title="Please enter your name" value="<?php echo getLastInput($lastInput, "first_name"); ?>">
                <label class="mdl-textfield__label" for="first_name">First Name</label>
                <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "first_name", "Letters and numbers only"); ?></span>
              </div>

              <br><br>

              <!-- Second name -->
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "second_name"); ?>">
                <input class="mdl-textfield__input" type="text" name="second_name" id="second_name" pattern="[A-Z,a-z,0-9]*" title="Please enter your name" value="<?php echo getLastInput($lastInput, "second_name"); ?>">
                <label class="mdl-textfield__label" for="second_name">Second Name</label>
                <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "second_name", "Letters and numbers only"); ?></span>
              </div>

            </fieldset>

            <br><br>

            <fieldset>
              <legend class="mdl-typography--subhead">Employment</legend>
              <!-- Employee ID -->
              <input type="hidden" name="employee_id" value="<?php echo $lastInput["employee_id"]; ?>">

              <!-- Role -->
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "role"); ?>">
                <label class="mdl-textfield__label" for="role">Role</label>
                <select class="mdl-textfield__input" id="role" name="role" title="Select the employee's job role" style="appearance: button; ">
                  <option disabled selected></option>
                  <!-- These must be loaded dynamically from PHP -->
                  <?php echo ManagerOutput::makeRoleOptions($employeePageData, $lastInput); ?>
                </select>
                <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "role", "Invalid role selected"); ?></span>
              </div>

              <br><br>

              <!-- Branch -->
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "branch"); ?>">
                <label class="mdl-textfield__label" for="branch">Branch</label>
                <select class="mdl-textfield__input" id="branch" name="branch" title="Select the employee's working branch" style="appearance: button; ">
                  <option disabled selected></option>
                  <!-- These must be loaded dynamically from PHP -->
                  <?php echo ManagerOutput::makeBranchOptions($employeePageData, $lastInput); ?>
                </select>
                <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "role", "Invalid branch selected"); ?></span>
              </div>

              <br><br>

              <!-- Start Date -->
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "start_date"); ?>">
                <input class="mdl-textfield__input" id="start_date" name="start_date" type="date" optional title="Enter when the employment will start/started" pattern="*" value="<?php echo getLastInput($lastInput, "start_date"); ?>">
                <label class="mdl-textfield__label" for="pass_code">Start Date</label>
                <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "start_date"); ?></span>
              </div>

              <br><br>

              <!-- Pass Code -->
              <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "pass_code"); ?>">
                <input class="mdl-textfield__input" id="pass_code" name="pass_code" type="text" optional title="Enter a new passcode" pattern="*" value="<?php echo getLastInput($lastInput, "pass_code"); ?>">
                <label class="mdl-textfield__label" for="pass_code">Pass Code</label>
                <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "pass_code"); ?></span>
              </div>

            </fieldset>

            <br><br>

            <!-- "submit" button for form -->
            <div>
              <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary">
                Submit
              </button>
            </div>

          </form>

          <div style="height:80px;"></div>

          <!-- Delete Employee -->
          <div style="display:flex; flex-direction:row; align-items:center;">
            <button class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--accent mdc-theme--secondary" onclick="showCancelMenu('cancel-menu__delete');">
              Delete this record
            </button>
            <div style="width:20px;"></div>
            <!-- Confirmation menu -->
            <div class="cancel-menu mdl-typography--body-1" id="cancel-menu__delete">
              <span>Are you sure?</span>
              <a href="<?php echo HREF_ROOT . "staff/manager/on_profile_delete.php?employee_id=$lastInput[employee_id]"; ?>">Yes</a>
              <a onclick="hideCancelMenu('cancel-menu__delete');">No</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>
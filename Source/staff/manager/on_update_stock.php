<?php

/**
 * Processes a submitted Manager Update Stock form.
 * 
 * This will subtract the expired ingredient quantities
 * from the branch inventory.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "staff/database_views.php");

function onSubmitManagerUpdateStock($inputArray, $sessionArray)
{
  // Declare form local constants.
  /** Url the form came from to send back any errors. */
  $formPageUrl = HREF_ROOT . "staff/manager/stock.php";
  /** Url the form intends to go if successful. */
  $landingPageUrl = HREF_ROOT . "staff/manager/stock.php";

  /** Whether form has terminated with success. */
  $wasFormProcessSuccessful = false;

  $databaseLink = connectToNeatTreats(STAFF_ROLE_MANAGER, "Password123");
  $formErrors = array();
  
  // Sanitize and prepare inputs for validation.
  $expiredItems = array();
  foreach ($inputArray as $ingredientID => $quantity) {
    $ingredientID = sanitizeFormInput($ingredientID);
    $quantity = sanitizeFormInput($quantity);
    $expiredItems[$ingredientID] = $quantity;
  }

  // Only logged in managers can update expired stock.
  if (!isEmployeeLoggedIn($sessionArray, STAFF_ROLE_MANAGER)) {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }

  // Employee id for the manager.
  $employeeID = $sessionArray["employeeID"];
  // Branch id for the manager.
  $branchID = EmployeeViews::getWorkingBranch($employeeID, $databaseLink);

  // Validate incoming ingredient ids and quantities.
  // Each field represents an expired quantity it so its easier to bulk process them.
  foreach ($expiredItems as $ingredientID => $expiredQuantity) {
    if (!is_numeric($expiredQuantity)) {
      $formErrors["form_overall"] = EFormErrors::NOT_NUMBER;
    } else if (!isInDatabase($ingredientID, $databaseLink, "Ingredient", "IngredientID")) {
      $formErrors["form_overall"] = EFormErrors::DOESNT_EXIST;
    }
  }

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return;
  } else {
    // No errors, so subtract expired items from database.

    // First get all the current values.
    $currentStock = array();
    if (!ManagerViews::getCurrentStock($branchID, $currentStock, $databaseLink)) {
      onFormFail($inputArray, $formPageUrl, $databaseLink);
      return;
    }

    // Calculate new quantity of each item.
    $newItems = array();
    foreach ($expiredItems as $ingredientID => $expiredQuantity) {
      // Cast to float so we can use do numeric calculations.
      $expiredQuantity = (float) $expiredQuantity;

      if ($expiredQuantity == 0.0) {
        // Don't specify a query for quantities that are unchanged.
        continue;
      }

      $currentQuantity = $currentStock[$ingredientID];
      $newQuantity = $currentQuantity - $expiredQuantity;
      if ($newQuantity < 0.0) {
        // Ensure quantity can't be negative - clamp at zero.
        $newQuantity = 0.0;
      }

      $newItems[$ingredientID] = $newQuantity;
    }

    // Now query the database to set the new inventory information.

    // Get the inventory id the branch uses.
    $inventoryID = ManagerViews::getBranchInventoryID($branchID, $databaseLink);
    $wasQuantityUpdateSuccessful = true;
    foreach ($newItems as $ingredientID => $quantity) {
      // Set the quantity of each item to the new quantity. 
      $query = (
        "UPDATE IngredientCollection " .
        "SET Quantity = $quantity " .
        "WHERE IngredientCollectionID = $inventoryID " .
        "AND IngredientID = $ingredientID;");

      $result = $databaseLink->query($query);
      if (!empty($databaseLink->error) || $result) {
        // Error in query - stop processing and return.
        $wasQuantityUpdateSuccessful = false;
        break;
      }
    }

    if ($wasQuantityUpdateSuccessful) {
      // If the outcome was successful, set the form status.
      $wasFormProcessSuccessful = true;
    }
  }

  if ($wasFormProcessSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Manager Update Stock</title>
</head>

<body>
  Please wait while you are redirected...
  <?php
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  onSubmitManagerUpdateStock($_POST, $_SESSION);
  ?>
</body>

</html>
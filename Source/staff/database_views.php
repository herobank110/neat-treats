<?php

/**
 * Function library for database interactions by staff members.
 */

require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");

/**
 * Return an associative array of options for logged in employee.
 * 
 * Outputs include FirstName, BranchName, StartDate.
 * 
 * @param string $pageStaffRole Intended staff role for this page.
 * @param mysqli $databaseLink Connection for accessing database. Should
 * be accessed by same user as the landing page role.
 * @param array $sessionArray Current $_SESSION array.
 * @return array|null Array of employee details. NULL if couldn't
 * be accessed.
 */
function getLandingPageDetails($pageStaffRole, $databaseLink, $sessionArray)
{
  $employeeID = $sessionArray["employeeID"] ?? NULL;
  $userRole = $sessionArray["role"] ?? NULL;
  if (
    $employeeID === NULL || $userRole === NULL
    || $userRole !== $pageStaffRole
  ) {
    // User is not logged in as a valid baker user.
    return NULL;
  }

  // Check the database for employee details.
  $query = ("SELECT FirstName, Branch.Name as 'BranchName', StartDate FROM Employee " .
    "LEFT JOIN Person ON Person.PersonID = Employee.PersonID " .
    "LEFT JOIN Branch ON Branch.BranchID = Employee.BranchID " .
    "WHERE EmployeeID = $employeeID " .
    "LIMIT 1;");

  $result = $databaseLink->query($query);
  if (!empty($databaseLink->error)) {
    // Query failed.
    return NULL;
  }

  return $result->fetch_assoc();
}

// Baker functions

/**
 * Return a baker order card with specified information.
 * 
 * @param array $cartItems An associative array like {cake name => quantity}
 */
function getBakerOrderCard($orderID, array $cartItems, string $status, string $nextStatus): string
{
  $items = "";
  foreach ($cartItems as $cakeName => $quantity) {
    $items .= "${quantity}x $cakeName<br>";
  }

  $updateUrl = (HREF_ROOT . // Url root (required)
    "staff/baker/on_set_order_status.php" . // Relative page link.
    "?order_id=$orderID&new_status=$nextStatus"); // $_GET options.

  return ('<div class="baker-order-card mdc-card mdc-card--outlined">' .
    '<div class="mdl-card__title">' .
    '<p class="mdl-typography--headline">Order No. #' . $orderID . '</p>' .
    $items .
    '<em>Status: ' . $status . '</em>' .
    '</div>' .
    '<div class="mdl-card__actions">' .
    '<div class="mdl-layout-spacer"></div>' .
    '<a href="' . $updateUrl . '" ' .
    'class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--primary">' .
    'Mark as ' . $nextStatus .
    '</a>' .
    '</div>' .
    '</div>');
}

/** 
 * Returns the next logical order status in baking pipeline.
 * 
 * @see EOrderStatus
 * 
 * If already finished, returns FINISHED.
 * Invalid options will return UNSTARTED.
 */
function getNextBakingStatus($currentStatus)
{
  switch ($currentStatus) {
    case EOrderStatus::UNSTARTED:
      return EOrderStatus::STARTED;
    case EOrderStatus::STARTED:
      return EOrderStatus::FINISHED;
    case EOrderStatus::FINISHED:
      return EOrderStatus::FINISHED;

    default:
      return EOrderStatus::UNSTARTED;
  }
}

/**
 * Return the landing page url for a staff member.
 */
function getStaffLandingPageUrl($role): string
{
  switch ($role) {
    case STAFF_ROLE_BAKER:
      // Redirect to baker landing page.
      $landingPage = "staff/baker";
      break;
    case STAFF_ROLE_CASHIER:
      // Redirect to cashier landing page.
      $landingPage = "staff/cashier";
      break;
    case STAFF_ROLE_MANAGER:
      // Redirect to manager landing page.
      $landingPage = "staff/manager";
      break;
    case STAFF_ROLE_ADMIN:
      $landingPage = "staff/admin";
      break;
    default:
      // Invalid role, redirect to login page.
      $landingPage = "staff/login.php";
      break;
  }

  return HREF_ROOT . $landingPage;
}

/**
 * Returns all active orders in baker's working branch.
 * 
 * @param mysqli $databaseLink Connection to database.
 * @param mixed $branchID ID of branch to retrieve orders from.
 * @return array|null Associative array of orders like {orderID => details}
 * or NULL if there was an error loading data.
 */
function getOrdersAtBranch($branchID, $databaseLink)
{
  // Find the orders belonging to this branch.
  
  // Only show order in a status the baker can modify.
  $validStatuses = "('" . implode("', '", ELookupOptions::BAKER_SHOWN_STATUS) . "')";
  
  $orderQuery = ("SELECT OrderID, CartID, Status FROM CustomerOrder " .
    "WHERE BranchID = $branchID " .
    "AND Status IN $validStatuses " .
    "ORDER BY OrderTime;");

  $orderResult = $databaseLink->query($orderQuery);

  if (!empty($databaseLink->error)) {
    return NULL;
  };

  // Fill the orders array with details arrays.
  $foundOrders = array();
  while ($orderRow = $orderResult->fetch_assoc()) {
    $details = array("Status" => $orderRow["Status"], "Items" => array());
    // Add the ordered items to the details.
    $cartID = $orderRow["CartID"];
    $cartQuery = ("SELECT Cake.Name as 'CakeName', Quantity FROM Cart " .
      "LEFT JOIN Cake on Cake.CakeID = Cart.CakeID " .
      "WHERE CartID = $cartID;");

    $cartResult = $databaseLink->query($cartQuery);
    if (!empty($databaseLink->error)) {
      return NULL;
    }

    while ($cartRow = $cartResult->fetch_assoc()) {
      $details["Items"][$cartRow["CakeName"]] = $cartRow["Quantity"];
    }

    // Add the details to the orders to return.
    $foundOrders[$orderRow["OrderID"]] = $details;
  }

  return $foundOrders;
}

/**
 * Return the baker order cards in HTML format ready for display.
 * 
 * Return value must be `echo`ed in order to be displayed.
 * 
 * @param array $foundOrders Data to create orders with.
 */
function displayBakerOrders(array $foundOrders): string
{
  $returnHTML = "";
  foreach ($foundOrders as $orderID => $details) {
    $nextStatus = getNextBakingStatus($details["Status"]);

    $returnHTML .= getBakerOrderCard(
      $orderID,
      $details["Items"],
      $details["Status"],
      $nextStatus
    );
  }
  return $returnHTML;
}

/**
 * Get data for sending email to customer that order is finished.
 *
 * @param [type] $orderID Order to notify about.
 * @param [type] $outOrderFinishedEmailData Data to create email with.
 * Contains keys: OrderID, CustomerEmail.
 * @param mysqli $databaseLink Connection to database. Must be able to
 * SELECT from CustomerOrder, Cake, Cart and Customer tables.
 * @return boolean Was successful in query. False if order could not be
 * found.
 */
function getCollectOrderEmailData(
  $orderID,
  &$outCollectOrderEmailData,
  mysqli $databaseLink
): bool
{
  // Initialise output variables
  $outCollectOrderEmailData = array();
  
  // Get customer email address.
  $query = (
    "SELECT Email FROM Customer " . 
    "WHERE CustomerID IN " . 
      "(SELECT CustomerID FROM CustomerOrder " .
      "WHERE OrderID = $orderID) " .
    "LIMIT 1;"
    );
  $result = $databaseLink->query($query);
  echo $databaseLink->error;
  if (!empty($databaseLink->error) || $result->num_rows == 0) {
    return false;
  }
  
  $email = $result->fetch_assoc()["Email"];
  // Set outputs and return with success.
  $outCollectOrderEmailData["OrderID"] = $orderID;
  $outCollectOrderEmailData["CustomerEmail"] = $email;
  return true;
}

/**
 * Return HTML code for customer collect order email body.
 *
 * @param array $outCollectOrderEmailData Data for email.
 * @return string HTML code for email body.
 */
function getCollectOrderEmailBody($outCollectOrderEmailData): string
{
  $orderID = $outCollectOrderEmailData["OrderID"];
  $faqUrl = HREF_ROOT . "info/faq.php";

  return (
    '<html><body>' .
    '<h1>Your order can be collected!</h1>' .
    '<p>Show this order number to collect your order: ' .
      '<strong>Order #' . $orderID . '</strong></p>' .
    '<p><b>We hope you enjoy your order!</b></p>' .
    '<p>Please visit our <a href="' . $faqUrl . '">FAQ page</a> ' .
    'if you have any queries.</p>' .
    '</body></html>'
  );
}

/**
 * Send an email to the customer to notify them to collect it.
 *
 * @param array $outCollectOrderEmailData Data for email.
 * @return boolean Was email successfully sent?
 */
function sendCollectOrderEmail($outCollectOrderEmailData):bool
{
  $toEmail = $outCollectOrderEmailData["CustomerEmail"];
  $subject = "Your order from Neat Treats is ready for collection";
  $body = getCollectOrderEmailBody($outCollectOrderEmailData);
  $headers = ("From: neattreats.sender@gmail.com\r\n" .
    "Content-Type: text/html; charset=ISO-8859-1\r\n");

  return mail($toEmail, $subject, $body, $headers);
}

/**
 * Get the ingredients required to make a cake.
 *
 * @param [type] $cakeID Cake to check.
 * @param [type] $outRecipe Associative array of ingredients, like
 * {ingredientID => quantity}.
 * @param mysqli $databaseLink Connection to the database. Must be able
 * to SELECT from Cake and IngredientCollection tables.
 * @return bool Whether query was successful. False if cake doesn't
 * exist.
 */
function getRecipe($cakeID, &$outRecipe, mysqli $databaseLink): bool
{
  // Initialise output.
  $outRecipe = array();

  // Get the ingredients to make each cake.
  $query = ("SELECT IngredientID, Quantity FROM IngredientCollection " .
    "WHERE IngredientCollectionID IN " .
    "(SELECT IngredientCollectionID FROM Cake " .
    "WHERE CakeID = $cakeID);");

  $result = $databaseLink->query($query);
  if (!empty($databaseLink->error) || $result->num_rows == 0) {
    // Failed to get ingredients for this cake or cake doesn't exist.
    return false;
  }

  // Return the results in an associative array.
  while ($row = $result->fetch_assoc()) {
    $outRecipe[$row['IngredientID']] = $row['Quantity'];
  }
  return true;
}

/**
 * Subtract the ingredients from the database for a given order.
 * 
 * This is called after the baker sets the order status of an order
 * from Unstarted to Started. This function requires that the order 
 * status is Started when called.
 *
 * @param array $cartItems Associative array of items to consume, like
 * {cakeID => quantity}.
 * @param [type] $branchID Branch whose inventory to take ingredients
 * out of.
 * @param mysqli $databaseLink Connection to database. Must be able to
 * SELECT from Branch, Cake and IngredientCollection tables and UPDATE the
 * IngredientCollection table.
 * @return boolean Whether database was successfully updated.
 */
function consumeIngredients($cartItems, $branchID, mysqli $databaseLink): bool
{
  // Get the current values.
  $inventoryID = ManagerViews::getBranchInventoryID($branchID, $databaseLink);
  if ($inventoryID === NULL) {
    // Most likely there's insufficient access rights to get the branch table.
    return false;
  }

  // Ingredients currently in the branch. Quantities will be subtracted later.
  $currentStock = array();
  if (!ManagerViews::getCurrentStock($branchID, $currentStock, $databaseLink)) {
    return false;
  }

  foreach ($cartItems as $cakeID => $cakeQuantity) {
    // Get the ingredients needed to make each cake.
    if (!getRecipe($cakeID, $recipe, $databaseLink)) {
      // Couldn't get ingredients.
      return false;
    }

    foreach ($recipe as $ingredientID => $ingredientQuantity) {
      // Subtract from the current stock, ensuring we never 
      // go into negative numbers.

      // Multiple by how many cakes of the same type we're doing.
      $totalUsed = ($ingredientQuantity * $cakeQuantity);
      $newQuantity = $currentStock[$ingredientID] - $totalUsed;
      if ($newQuantity < 0) {
        // Never go into negative quantities.
        $newQuantity = 0;
      }

      // Propogate changes into the current stock array.
      $currentStock[$ingredientID] = $newQuantity;
    }
  }

  // Propogate changes into the database.
  $wasQuantityUpdateSuccessful = true;
  foreach ($currentStock as $ingredientID => $quantity) {
    // Set the quantity of each item to the new quantity. 
    $query = (
      "UPDATE IngredientCollection " .
      "SET Quantity = $quantity " .
      "WHERE IngredientCollectionID = $inventoryID " .
      "AND IngredientID = $ingredientID;");

    $result = $databaseLink->query($query);
    if (!empty($databaseLink->error) || $result) {
      // Error in query - stop processing and return.
      $wasQuantityUpdateSuccessful = false;
      break;
    }
  }

  return $wasQuantityUpdateSuccessful;
}

/**
 * Prepare data for the baker orders page.
 * 
 * @param array $foundOrders Data to pass to displayOrders() function.
 * 
 * @return int Status code of page loading. @see InitPageStatus
 */
function initBakerOrdersPage($sessionArray, array &$foundOrders): int
{

  $employeeID = $sessionArray["employeeID"] ?? NULL;
  $role = $sessionArray["role"] ?? NULL;

  if ($role != STAFF_ROLE_BAKER) {
    return EInitPageStatus::NOT_PERMITTED;
  }


  $databaseLink = connectToNeatTreats(STAFF_ROLE_BAKER, "Password123");

  // Find out which branch the baker works at.
  $branch = EmployeeViews::getWorkingBranch($employeeID, $databaseLink);
  $foundOrders = getOrdersAtBranch($branch, $databaseLink);

  // if ($foundOrders === NULL) {
  //   return EInitPageStatus::MAX;
  // }

  $ordersArray = $foundOrders;
  return EInitPageStatus::SUCCESS;
}

/**
 * Database views that all staff members can access.
 */
class EmployeeViews
{
  public static function getWorkingBranch($employeeID, $databaseLink): int
  {
    $query = "SELECT BranchID FROM Employee WHERE EmployeeID = $employeeID";
    $result = $databaseLink->query($query);
    if (empty($databaseLink->error) && $result->num_rows > 0) {
      return $result->fetch_assoc()["BranchID"];
    }
    return NULL;
  }
}

/**
 * Static function library for the manager user to interact with database.
 */
class ManagerViews
{
  /**
   * Get basic data for employees at a branch.
   *
   * @param array $outEmployeeTableData Array of basic data array.
   * Possibly empty.
   * @param [type] $branchID Id of branch to check.
   * @param mysqli $databaseLink Connection to database. Must be able to
   * SELECT from Employee table.
   * @return boolean Whether query was successful.
   */
  static public function getEmployeeTableData(array &$outEmployeeTableData, $branchID, $databaseLink): bool
  {
    $query = ("SELECT EmployeeID, FirstName, SecondName, " .
      "Role.Name as 'RoleName' FROM Employee " .
      "LEFT JOIN Person ON Person.PersonID = Employee.PersonID " .
      "LEFT JOIN Role ON Role.RoleID = Employee.RoleID " .
      "WHERE BranchID = $branchID;");

    $result = $databaseLink->query($query);
    if (!empty($databaseLink->error)) {
      // An error with the query. Return unsuccessful status.
      return false;
    }

    $rowIndex = 0;
    while ($row = $result->fetch_assoc()) {
      // Add each returned row to the result array.
      $outEmployeeTableData[$rowIndex] = $row;
      $rowIndex++;
    }
    return true;
  }

  /**
   * Undocumented function
   *
   * @param array $outEmployeeProfileData Array of employee profile
   * data.
   * @param [type] $employeeID Id of the employee to fetch.
   * @param [type] $databaseLink Connect to database. Must be able to
   * SELECT from Employee and Person.
   * @return boolean Whether query was successful. False if result empty.
   */
  public static function getEmployeeProfileData(&$outEmployeeProfileData, $employeeID, $databaseLink): bool
  {
    $query = ("SELECT Title, FirstName, SecondName, RoleID as 'Role', BranchID as 'Branch', " .
      "StartDate, PassCode FROM Employee " .
      "LEFT JOIN Person ON Person.PersonID = Employee.PersonID " .
      "WHERE EmployeeID = $employeeID " .
      "LIMIT 1;");

    $result = $databaseLink->query($query);

    if (!empty($databaseLink->error) || $result->num_rows < 1) {
      // Failure to execute query or empty result.
      $outEmployeeProfileData = array();
      return false;
    }

    $outEmployeeProfileData = $result->fetch_assoc();
    return true;
  }

  /**
   * Contains rules for supplier ordering stipulations.
   * 
   * Each ingredient has two components: 
   * The `minimumOrder` is the minimum allowed restock quantity, from
   * which valid restock levels will increase in `multiplesOf`.
   * The `multiplesOf` is the multiples of quantity for a restock order
   * to increase in.
   * 
   * SUPPLIER_RULES ::= array(
   *   (int) ingredientID => array(
   *     "multiplesOf" => float, "minimumOrder" => float
   *   )
   * )
   */

  const SUPPLIER_RULES = array(
    1 => array("multiplesOf" => 5.00, "minimumOrder" => 15.00),
    2 => array("multiplesOf" => 5.00, "minimumOrder" => 15.00),
    3 => array("multiplesOf" => 4.00, "minimumOrder" => 12.00),
    4 => array("multiplesOf" => 15.0, "minimumOrder" => 30),
    5 => array("multiplesOf" => 2.00, "minimumOrder" => 2.00),
    6 => array("multiplesOf" => 0.25, "minimumOrder" => 0.25),
    7 => array("multiplesOf" => 5.00, "minimumOrder" => 10.00),
    8 => array("multiplesOf" => 0.75, "minimumOrder" => 0.75),
    9 => array("multiplesOf" => 2.00, "minimumOrder" => 2.00),
    10 => array("multiplesOf" => 3.00, "minimumOrder" => 3.00),
    11 => array("multiplesOf" => 2.50, "minimumOrder" => 2.50),
    12 => array("multiplesOf" => 2.5, "minimumOrder" => 2.5),
    13 => array("multiplesOf" => 1.00, "minimumOrder" => 1.00),
    14 => array("multiplesOf" => 1.00, "minimumOrder" => 1.00),
    15 => array("multiplesOf" => 1.00, "minimumOrder" => 1.00),
    16 => array("multiplesOf" => 1.00, "minimumOrder" => 1.00),
    17 => array("multiplesOf" => 1.50, "minimumOrder" => 1.50),
    18 => array("multiplesOf" => 1.50, "minimumOrder" => 1.50),
    19 => array("multiplesOf" => 1.50, "minimumOrder" => 1.50),
    20 => array("multiplesOf" => 0.50, "minimumOrder" => 0.50),
    21 => array("multiplesOf" => 0.005, "minimumOrder" => 0.01),
    22 => array("multiplesOf" => 0.005, "minimumOrder" => 0.01),
    23 => array("multiplesOf" => 0.005, "minimumOrder" => 0.01));

  /**
   * Get restock amount considering the current amounts.
   * 
   * Doesn't actually use database to lookup values, supplier values
   * are kept constant.
   *
   * @param [type] $ingredientID Ingredient to check.
   * @param float $heldQuantity Currently held amount.
   * @param float $maxQuantity Maximum quantity storeroom can hold.
   * @param float $expiredQuantity Quantity that was expired. If the database
   * was already modified to subtract this value this may be reflected in
   * the $heldQuantity. In that case, leave the $expiredQuantity at zero.
   * @return float Amount to restock, obeying supplier's bulk order
   * rules.
   */
  public static function getRestockQuantity(
    $ingredientID,
    $heldQuantity,
    $maxQuantity,
    $expiredQuantity = 0
  ): float
  {
    // Check the supplier rules for this ingredient.

    $rules = self::SUPPLIER_RULES[$ingredientID] ?? NULL;
    if ($rules === NULL) {
      // Invalid ingredient to check.
      return 0.0;
    }

    // Minimum quantity that can be ordered.
    $minimumOrder = $rules["minimumOrder"];
    // Multiples to order in bulk of (starting at minimumOrder).
    $multiplesOf = $rules["multiplesOf"];

    // Quantity that can be kept for next week.
    $keep = $heldQuantity - $expiredQuantity;
    // Quantity that must be reordeded.
    $needed = $maxQuantity - $keep;

    // Quantity to reorder from supplier, obeying their bulk order rules.
    $toOrder = $minimumOrder;
    while ($toOrder < $needed) {
      // Add up in valid bulk order multiples until reached enough quantity.
      $toOrder += $multiplesOf;
    }

    return $toOrder;
  }

  /**
   * Get the IngredientCollectionID of a branch.
   * 
   * Ingredient collection is treated as the branch's inventory.
   *
   * @param [type] $branchID Branch to check.
   * @param mysqli $databaseLink Connect to database. Must be able to
   * SELECT from Branch table.
   * @return integer|NULL Output inventoryID or NULL if not found.
   */
  public static function getBranchInventoryID(
    $branchID,
    mysqli $databaseLink
  ): int {
    $query = ("SELECT IngredientCollectionID FROM Branch " .
      "WHERE BranchID = $branchID " .
      "LIMIT 1;");
    $result = $databaseLink->query($query);
    if (!empty($databaseLink->error) || $result->num_rows == 0) {
      // Failed to make the query.
      return NULL;
    }

    return $result->fetch_assoc()["IngredientCollectionID"];
  }

  /**
   * Get items stocked at the specified branch.
   *
   * @param [type] $branchID Branch to check.
   * @param array $currentStock Output associative array like
   * {ingredientID => quantity}.
   * @param mysqli $databaseLink Connection to database. Must be able to
   * SELECT from IngredientCollection and Branch table.
   * @return boolean Whether query was successful.
   */
  public static function getCurrentStock(
    $branchID,
    &$outCurrentStock,
    mysqli $databaseLink
  ): bool
  {
    // Initialise output array.
    $outCurrentStock = array();

    $inventoryID = self::getBranchInventoryID($branchID, $databaseLink);
    if ($inventoryID === NULL) {
      return false;
    }

    $query = (
      "SELECT IngredientID, Quantity FROM IngredientCollection " .
      "WHERE IngredientCollectionID = $inventoryID;"
    );

    $result = $databaseLink->query($query);
    if (!empty($databaseLink->error)) {
      return false;
    }

    while ($row = $result->fetch_assoc()) {
      $outCurrentStock[$row["IngredientID"]] = $row["Quantity"];
    }
    return true;
  }

  /**
   * Get stock table data from the database ready for displaying.
   *
   * @param [type] $branchID Branch whose storeroom to check.
   * @param array $outStockTableData Enumerated array of table rows data.
   * @param mysqli $databaseLink Connection to database. Must be able to 
   * SELECT from IngredientCollection table.
   * @return boolean Whether query succeeded.
   */
  public static function getStockTableData(
    $branchID,
    &$outStockTableData,
    mysqli $databaseLink
  ): bool
  {

    // Needed columns for output:
    // IngredientID, IngredientName, CurrentQuantity, MaxQuantity,
    // RestockQuantity

    // Get the current values from the database.

    // Find the ingredient collection id the branch is using.
    $inventoryID = self::getBranchInventoryID($branchID, $databaseLink);

    // Now get the actual inventory items.
    $itemsQuery = (
      "SELECT IngredientCollection.IngredientID as 'IngredientID', " .
        "Ingredient.Name as 'IngredientName', Quantity as 'CurrentQuantity', " .
        "MaxQuantity " .
        "FROM IngredientCollection " .
      "LEFT JOIN Ingredient " .
        "ON Ingredient.IngredientID = IngredientCollection.IngredientID " .
      "WHERE IngredientCollectionID = $inventoryID;");

    $result = $databaseLink->query($itemsQuery);
    if (!empty($databaseLink->error)) {
      // Failed to make the query.
      $outStockTableData = array();
      return false;
    }

    // Enumerate rows into the output array.
    // Column aliases have already been set in the query.
    $rowIndex = 0;
    while ($row = $result->fetch_assoc()) {
      // Calculate and set restock levels.
      $restockQuantity = ManagerViews::getRestockQuantity(
        $row["IngredientID"], $row["CurrentQuantity"], $row["MaxQuantity"]);

      // Reformulate the row to include RestockQuantity.
      $row["RestockQuantity"] = $restockQuantity;
      // Push to output array.
      $outStockTableData[$rowIndex] = $row;
      $rowIndex++;
    }

    // Return data with success status.
    return true;
  }

  /**
   * Get names and ids of all branches.
   *
   * @param array $outBranches Associative array like {branchid => name}.
   * @param [type] $databaseLink Connect to database. Must be able to
   * SELECT from Branch table.
   * @return boolean Whether query was successful. False if result empty.
   */
  public static function getAllBranches(&$outBranches, $databaseLink): bool
  {
    $result = $databaseLink->query("SELECT BranchID, Name FROM Branch;");
    if (!empty($databaseLink->error) || $result->num_rows == 0) {
      $outBranches = array();
      return false;
    }

    while ($row = $result->fetch_assoc()) {
      $outBranches[$row["BranchID"]] = $row["Name"];
    }
    return true;
  }

  /**
   * Get names and ids of all roles.
   *
   * @param array $outBranches Associative array like {roleid => name}.
   * @param [type] $databaseLink Connect to database. Must be able to
   * SELECT from Role table.
   * @return boolean Whether query was successful. False if result empty.
   */
  public static function getAllRoles(&$outRoles, $databaseLink): bool
  {
    $result = $databaseLink->query("SELECT RoleID, Name FROM Role;");
    if (!empty($databaseLink->error) || $result->num_rows == 0) {
      $outRoles = array();
      return false;
    }

    while ($row = $result->fetch_assoc()) {
      $outRoles[$row["RoleID"]] = $row["Name"];
    }
    return true;
  }

  /**
   * Load data for the manager employees page.
   *
   * @param array $outTableData Table data to pass to create the employee
   * table.
   * @param array $sessionArray Current value of `$_SESSION` array.
   * @return integer Status code of page loading.
   */
  public static function initEmployeesPage(array &$outTableData, $sessionArray): int
  {
    // Get the employee id of this manager.
    if (!isEmployeeLoggedIn($sessionArray)) {
      // Staff members must be logged in to view this page.
      return EInitPageStatus::NOT_LOGGED_IN;
    }

    $employeeID = $sessionArray["employeeID"];
    $role = $sessionArray["role"];

    if ($role != STAFF_ROLE_MANAGER) {
      // Only managers are permitted to access this page.
      return EInitPageStatus::NOT_PERMITTED;
    }

    $databaseLink = connectToNeatTreats(STAFF_ROLE_MANAGER, "Password123");

    $workingBranch = EmployeeViews::getWorkingBranch($employeeID, $databaseLink);
    if ($workingBranch !== null) {
      if (ManagerViews::getEmployeeTableData($outTableData, $workingBranch, $databaseLink)) {
        return EInitPageStatus::SUCCESS;
      }
    }
    return EInitPageStatus::MAX;
  }

  /**
   * Load data for the Manager Employee Profile page.
   *
   * @param array $outEmployeeData Data about the employee whose
   * profile was requested.
   * @param array $outFormErrors Errors in submitted form.
   * @param array $outLastInput Input field values from previous form
   * submission or current values from database.
   * @param array $outEmployeePageData Additional data from page loading
   * to pass to various functions in `ManagerOutput`.
   * @param array $getArray Current value of `$_GET` array.
   * @param array $sessionArray Current value of `$_SESSION` array.
   * @return integer Status code of page loading.
   */
  public static function initEmployeeProfilePage(
    &$outFormErrors,
    &$outLastInput,
    &$outEmployeePageData,
    $getArray,
    $sessionArray
  ): int {
    $outFormErrors = array();
    $outLastInput = array();
    $outEmployeePageData = array();

    // Get the employee id of this manager.
    if (!isEmployeeLoggedIn($sessionArray)) {
      // Staff members must be logged in to view this page.
      return EInitPageStatus::NOT_LOGGED_IN;
    }

    $employeeID = $sessionArray["employeeID"];
    $role = $sessionArray["role"];

    if ($role != STAFF_ROLE_MANAGER) {
      // Only managers are permitted to access this page.
      return EInitPageStatus::NOT_PERMITTED;
    }

    $profiledID = $getArray["employee_id"] ?? NULL;
    if ($profiledID === NULL) {
      // Employee to profile wasn't specified.
      return EInitPageStatus::MAX;
    }

    $databaseLink = connectToNeatTreats(STAFF_ROLE_MANAGER, "Password123");
    if (!isInDatabase($profiledID, $databaseLink, "Employee", "EmployeeID")) {
      // Employee does not exist in database.
      return EInitPageStatus::MAX;
    }

    // Get input field values from last form submission.
    $outFormErrors = loadFormErrors();
    $lastInput = loadLastFormInput();

    // Get all input field current values in database.
    if (!ManagerViews::getEmployeeProfileData($dbProfileValues, $profiledID, $databaseLink)) {
      return EInitPageStatus::MAX;
    }
    // Also attach employee id for good measure.
    $dbProfileValues["employee_id"] = $profiledID;

    
    // Merge last input with database values for the resulting values.
    foreach ($dbProfileValues as $key => $value) {
      // Convert keys to lower case underscore separated.
      $key = strtolower(preg_replace(EInputPatterns::PASCAL_CASE, '$1_$2', $key));
      if (isPresent($lastInput[$key] ?? "")) {
        $outLastInput[$key] = $lastInput[$key];
      } else {
        $outLastInput[$key] = $value;
      }
    }

    // Get data about the valid branches and roles.
    if (
      ManagerViews::getAllBranches($branches, $databaseLink)
      && ManagerViews::getAllRoles($roles, $databaseLink)
    ) {
      $outEmployeePageData["branches"] = $branches;
      $outEmployeePageData["roles"] = $roles;
    }

    return EInitPageStatus::SUCCESS;
  }

  /**
   * Load data for the Manager Add Employee page.
   *
   * @param array $outFormErrors Array of form errors.
   * @param array $outLastInput Array of input from last submit.
   * @param array $outEmployeePageData Array of employee data.
   * @param array $sessionArray Value of $_SESSION variable.
   * @return integer Status code of page loading.
   */
  public static function initAddEmployeePage(
    &$outFormErrors,
    &$outLastInput,
    &$outEmployeePageData,
    $sessionArray
  ): int {
    $outFormErrors = array();
    $outLastInput = array();
    $outEmployeePageData = array();

    // Get the employee id of this manager.
    if (!isEmployeeLoggedIn($sessionArray)) {
      // Staff members must be logged in to view this page.
      return EInitPageStatus::NOT_LOGGED_IN;
    }

    $employeeID = $sessionArray["employeeID"];
    $role = $sessionArray["role"];

    if ($role != STAFF_ROLE_MANAGER) {
      // Only managers are permitted to access this page.
      return EInitPageStatus::NOT_PERMITTED;
    }

    // Get input field values from last form submission.
    $outFormErrors = loadFormErrors();
    $outLastInput = loadLastFormInput();

    // Get data about the valid branches and roles.
    $databaseLink = connectToNeatTreats(STAFF_ROLE_MANAGER, "Password123");
    if (
      ManagerViews::getAllBranches($branches, $databaseLink)
      && ManagerViews::getAllRoles($roles, $databaseLink)
    ) {
      $outEmployeePageData["branches"] = $branches;
      $outEmployeePageData["roles"] = $roles;
    }
    $databaseLink->close();

    return EInitPageStatus::SUCCESS;
  }

  /**
   * Load data for the Manager Stock page.
   *
   * @param array $outStockTableData Loaded to use to create stock
   * table.
   * @param array $sessionArray Value of $_SESSION variable.
   * @return integer Status code of page loading.
   */
  public static function initStockPage(
    &$outStockTableData,
    $sessionArray
  ): int
  {
    // Get the employee id of this manager.
    if (!isEmployeeLoggedIn($sessionArray)) {
      // Staff members must be logged in to view this page.
      return EInitPageStatus::NOT_LOGGED_IN;
    }

    $employeeID = $sessionArray["employeeID"];
    $role = $sessionArray["role"];

    if ($role != STAFF_ROLE_MANAGER) {
      // Only managers are permitted to access this page.
      return EInitPageStatus::NOT_PERMITTED;
    }

    $databaseLink = connectToNeatTreats(STAFF_ROLE_MANAGER, "Password123");

    // Branch to get stock information for.
    $branch = EmployeeViews::getWorkingBranch($employeeID, $databaseLink);

    // Load the stock table data.
    if (!ManagerViews::getStockTableData($branch, $outStockTableData, $databaseLink)) {
      // Something went wrong fetching the stock table data.
      $databaseLink->close();
      return EInitPageStatus::MAX;
    }

    $databaseLink->close();
    return EInitPageStatus::SUCCESS;
  }

  /**
   * Delete an employee's records from the database
   *
   * @param [type] $employeeID Employee to be deleted.
   * @param mysqli $databaseLink Connection to database. Must be able to
   * DELETE from Person and Employee tables.
   * @return boolean Whether deletion was successful.
   */
  public static function deleteEmployeeProfile(
    $employeeID,
    mysqli $databaseLink
  ): bool
  {
    // EmployeeID depends on PersonID so get the value before removing it!
    $getPersonIDQuery = "SELECT PersonID FROM Employee WHERE EmployeeID = $employeeID";
    $getPersonIDResult = $databaseLink->query($getPersonIDQuery);
    if (empty($databaseLink->error) && $getPersonIDResult->num_rows > 0) {

      $personID = $getPersonIDResult->fetch_assoc()["PersonID"];

      // Now that we have the PersonID and nothing depends on EmployeeID,
      // we can safely delete the employee account row.
      $employeeQuery = "DELETE FROM Employee WHERE EmployeeID = $employeeID;";
      $wasEmployeeSuccessful = $databaseLink->query($employeeQuery);
      if (empty($databaseLink->error) && $wasEmployeeSuccessful) {

        // Finally delete the PersonID row that was waiting for EmployeeID to go.
        $personQuery = "DELETE FROM Person WHERE PersonID = $personID;";
        $wasPersonSuccessful = $databaseLink->query($personQuery);
        if (empty($databaseLink->error) && $wasPersonSuccessful) {
          return true;
        }
      }
    }
    return false;
  }
}

/**
 * Functions to allow output of information from ManagerViews.
 */
class ManagerOutput
{
  /** 
   * Return HTML code for a single row of the employee table.
   */
  public static function makeEmployeeTableRow($employeeRowData): string
  {
    $employeeID = $employeeRowData["EmployeeID"];
    $name = "$employeeRowData[FirstName] $employeeRowData[SecondName]";
    $roleName = $employeeRowData["RoleName"];
    $profileUrl = HREF_ROOT . "staff/manager/employee_profile.php?employee_id=$employeeID";
    $buttonClass = "mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--primary";

    return ('<tr>' .
      '<td>' . $employeeID . '</td>' .
      '<td class="mdl-data-table__cell--non-numeric">' . $name . '</td>' .
      '<td class="mdl-data-table__cell--non-numeric">' . $roleName . '</td>' .
      '<td><a href="' . $profileUrl . '" class="' . $buttonClass . '">' .
      'Details' .
      '</a></td>' .
      '</tr>');
  }

  /**
   * Return HTML code for the employees table.
   *
   * @param array $employeeTableData Data to create table with. 
   * @return string HTML code for the employees table.
   */
  public static function makeEmployeeTable(array $employeeTableData): string
  {
    $rowsString = "";
    foreach ($employeeTableData as $rowData) {
      $rowsString .= ManagerOutput::makeEmployeeTableRow($rowData);
    }

    return ('<table class="mdl-data-table">' .
      '<thead>' .
      '<th class="mdl-data-table__cell--non-numeric">Employee ID</th>' .
      '<th class="mdl-data-table__cell--non-numeric">Name</th>' .
      '<th class="mdl-data-table__cell--non-numeric">Role</th>' .
      '<th class="mdl-data-table__cell--non-numeric">View Details</th>' .
      '</thead>' .
      '<tbody>' .
      $rowsString .
      '</tbody>' .
      '</table>');
  }

  /**
   * Return content for the employee profile subheading.
   *
   * @param array $lastInput Data to create content with. Should
   * have specified default values from database.
   * @return string Plain text to put inside subheading.
   */
  public static function makeEmployeeProfileSubhead(array &$lastInput): string
  {
    $employeeID = $lastInput["employee_id"];
    $name = "$lastInput[first_name] $lastInput[second_name]";
    return "Editing $name, Employee ID $employeeID";
  }

  /**
   * Return HTML code for `option`s to go in Branch `select` element.
   *
   * Does not return a redundant (SELECT AN OPTION) option.
   * 
   * @param array $employeePageData Data to create values with.
   * @param array $employeePageData Optionally preselect the last
   * selected options with lastInput data.
   * @return string HTML code with `option` elements.
   */
  public static function makeBranchOptions(
    array &$employeePageData,
    array &$lastInput = NULL
  ): string {
    $branches = $employeePageData["branches"];

    $returnHTML = "";
    foreach ($branches as $branchID => $name) {
      if ($lastInput != NULL) {
        $selectedStatus = getLastSelectedState($lastInput, "branch", $branchID);
      } else {
        $selectedStatus = "";
      }      
      $returnHTML .= "<option value=\"$branchID\" $selectedStatus> $name</option>";
    }
    return $returnHTML;
  }

  /** 
   * Return HTML code for `option`s to go in Role `select` element.
   *
   * Does not return a redundant (SELECT AN OPTION) option.
   *
   * @param array $employeePageData Data to create values with.
   * @param array $employeePageData Optionally preselect the last
   * selected options with lastInput data.
   * @return string HTML code with `option` elements.
   */
  public static function makeRoleOptions(
    array &$employeePageData,
    array &$lastInput = NULL
  ): string {
    $roles = $employeePageData["roles"];

    $returnHTML = "";
    foreach ($roles as $roleID => $name) {
      if ($lastInput != NULL) {
        $selectedStatus = getLastSelectedState($lastInput, "role", $roleID);
      } else {
        $selectedStatus = "";
      }
      $returnHTML .= "<option value=\"$roleID\" $selectedStatus>$name</option>";
    }
    return $returnHTML;
  }

  /**
   * Return HTML code for a single row of the stock table.
   *
   * @param array $stockTableRowData Data to create values with.
   * Expected keys are IngredientID, IngredientName, CurrentQuantity,
   * MaxQuantity, RestockQuantity.
   * @return string HTML code of single row.
   */
  public static function makeStockTableRow(array $stockTableRowData): string
  {
    $ingredientID = $stockTableRowData["IngredientID"];
    $name = $stockTableRowData["IngredientName"];
    $quantity = $stockTableRowData["CurrentQuantity"];
    $capacity = $stockTableRowData["MaxQuantity"];
    $restock = $stockTableRowData["RestockQuantity"];
    $expiredQuantity = 0;

    // Create a pretty simple table row.
    return (
      '<tr>' .
        '<td class="mdl-data-table__cell--non-numeric">' . $name . '</td>' .
        '<td>' . $quantity . '</td>' .
        '<td>' . $capacity . '</td>' .
        '<td><input class="mdl-textfield__input" type="text" style="float:right;width:3em;" pattern="[0-9]*\.[0-9]*"' .
          'value=' . $expiredQuantity . ' name="' . $ingredientID . '"></td>' .
        '<td>' . $restock . '</td>' .
      '</tr>');
  }

  /**
   * Return HTML code for Manager stock table.
   *
   * @param array $stockTableData Data from loading the page.
   * @return string HTML code for the whole table.
   */
  public static function makeStockTable(array $stockTableData): string
  {
    $rowsString = "";
    foreach ($stockTableData as $rowData) {
      $rowsString .= ManagerOutput::makeStockTableRow($rowData);
    }

    return (
      '<table class="mdl-data-table">' .
        '<thead>' .
          '<th class="mdl-data-table__cell--non-numeric">Ingredient</th>' .
          '<th class="mdl-data-table__cell--non-numeric">Held Quantity</th>' .
          '<th class="mdl-data-table__cell--non-numeric">Storage Capacity</th>' .
          '<th class="mdl-data-table__cell--non-numeric">Expired Quantity</th>' .
          '<th class="mdl-data-table__cell--non-numeric">To Restock</th>' .
        '</thead>' .
        '<tbody>' .
          $rowsString .
        '</tbody>' .
      '</table>');
  }
}

/**
 * Static function library for the admin user to interact with the database.
 */
class AdminViews
{

  /**
   * Get the admin details.
   */
  public static function initAdminLandingPage(&$outEmployeeData, $sessionArray): int
  {

    $databaseLink = connectToNeatTreats(STAFF_ROLE_ADMIN, "");
    if ($databaseLink === NULL) {
      return EInitPageStatus::MAX;
    }

    $outEmployeeData = getLandingPageDetails(
      STAFF_ROLE_ADMIN,
      $databaseLink,
      $sessionArray
    );

    if (!isset($outEmployeeData)) {
      // Redirect to login if failed to get employee details.
      return EInitPageStatus::NOT_LOGGED_IN;
    }

    return EInitPageStatus::SUCCESS;
  }
}

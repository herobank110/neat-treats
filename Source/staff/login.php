<?php
require_once("../config.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "staff/database_views.php");


if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}
if (isEmployeeLoggedIn($_SESSION)) {
  $role = $_SESSION["role"];
  $landingPage = getStaffLandingPageUrl($role);
  redirect($landingPage);
}

$formErrors = loadFormErrors();
$lastInput = loadLastFormInput();

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Staff Login</title>
  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Neat Treats Staff
          </h1>
          <p class="mdl-typography--subhead">
            Log in to access your account panel
          </p>
        </div>

        <p class="mdc-typography--headline5">
          Log In
        </p>
        <div style="margin-left: 20px;">

          <form method="post" action="<?php echo HREF_ROOT . "staff/on_login.php"; ?>">

            <!-- Employee ID -->
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "employee_id"); ?>">
              <input class="mdl-textfield__input" type="text" name="employee_id" id="employee_id" title="Enter your employee id" value="<?php echo getLastInput($lastInput, "employee_id"); ?>">
              <label class="mdl-textfield__label" for="employee_id">Employee ID</label>
              <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "employee_id", "Could not find in database"); ?></span>
            </div>

            <br><br>

            <!-- Pass Code -->
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "pass_code"); ?>">
              <input class="mdl-textfield__input" type="password" name="pass_code" id="pass_code" title="Enter your pass code" value="<?php echo getLastInput($lastInput, "pass_code"); ?>">
              <label class="mdl-textfield__label" for="pass_code">Pass Code</label>
              <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "pass_code"); ?></span>
            </div>

            <br><br>

            <!-- "submit" button for form -->
            <div>
              <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary">
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>  
  <!--endregion -->
</body>

</html>

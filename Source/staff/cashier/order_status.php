<?php

/**
 * Webpage for Cashier Order Status page.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");

/**
 * Prepare data for the cashier check order page.
 */
function initCashierCheckOrderPage($sessionArray, &$orderData): int
{
  if (!isEmployeeLoggedIn($sessionArray, STAFF_ROLE_CASHIER)) {
    return EInitPageStatus::NOT_LOGGED_IN;
  }

  $databaseLink = connectToNeatTreats(STAFF_ROLE_CASHIER, "Password123");
  if ($databaseLink === NULL) {
    return EInitPageStatus::MAX;
  }

  $orderID = $sessionArray["cashierOrderID"] ?? NULL;
  if ($orderID === NULL) {
    return EInitPageStatus::MAX;
  }

  $query = "SELECT Status FROM CustomerOrder WHERE OrderID = $orderID";
  $result = $databaseLink->query($query);
  if (empty($databaseLink->error)) {
    $orderData["order_id"] = $orderID;
    
    $orderData["status"] = strtolower($result->fetch_assoc()["Status"]);
    return EInitPageStatus::SUCCESS;
  }
}

$orderData = array();
if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}
switch(initCashierCheckOrderPage($_SESSION, $orderData)) {
  case EInitPageStatus::SUCCESS:
    break;
  case EInitPageStatus::NOT_LOGGED_IN:
    redirect(HREF_ROOT . "staff/login.php");
    break;
  case EInitPageStatus::MAX:
    redirect(HREF_ROOT . "staff/login.php");
    break;
}

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Order Status</title>
  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Order Status Result
          </h1>
          <p class="mdl-typography--subhead">
          </p>
        </div>

        <div style="margin-left: 20px;">

          <p class="mdl-typography--body-2">
            <?php echo "Order $orderData[order_id] is current $orderData[status]"; ?>
          </p>

          <?php
          // Add a button to mark as collected as the customer is here.
          if (strtoupper($orderData["status"]) == EOrderStatus::FINISHED) {
            // Only show button if status is FINISHED.
            $markAsCollectedUrl = HREF_ROOT . "staff/cashier/on_set_order_collected.php?order_id=" . $orderData["order_id"];
            echo '<a href="' . $markAsCollectedUrl . '" class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--colored">' .
              'Mark as collected</a>';
          }
            ?>

          <div style="height:80px"></div>

          <a href=<?php echo HREF_ROOT . "staff/cashier/check_order.php"; ?> class="mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--primary">
            Check another order
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>
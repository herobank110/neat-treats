<?php

/**
 * Webpage for Cashier Check Order page.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "common/form_helper.php");

$formErrors = loadFormErrors();
$lastInput = loadLastFormInput();
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Check Order</title>
  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Check Order Status
          </h1>
          <p class="mdl-typography--subhead">
            See how a customer's order is going
          </p>
        </div>

        <div style="margin-left: 20px;">

          <form method="post" action="<?php echo HREF_ROOT . "staff/cashier/on_check_order.php"; ?>">

            <!-- Order ID -->
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "order_id"); ?>">
              <input class="mdl-textfield__input" type="text" name="order_id" id="order_id" title="Enter the customer's order number" value="<?php echo getLastInput($lastInput, "order_id"); ?>">
              <label class="mdl-textfield__label" for="order_id">Order Number</label>
              <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "order_id", "Could not find in database"); ?></span>
            </div>

            <br><br>

            <!-- "submit" button for form -->
            <div>
              <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary">
                Submit
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>
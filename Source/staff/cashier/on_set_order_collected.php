<?php

/**
 * Processes a submitted Cashier Set Order Collected form.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");

function onSubmitCashierSetOrderCollected($inputArray, $sessionArray)
{
  // Declare form local constants.
  /** Url the form came from to send back any errors. */
  $formPageUrl = HREF_ROOT . "staff/cashier";
  /** Url the form intends to go if successful. */
  $landingPageUrl = HREF_ROOT . "staff/cashier";

  /** Whether form has terminated with success. */
  $wasFormProcessSuccessful = false;

  $databaseLink = connectToNeatTreats(STAFF_ROLE_CASHIER, "Password123");
  $formErrors = array();

  // Sanitize and prepare inputs for validation.
  sanitizeFormInputArray($inputArray);

  // Extract each field as required.
  $orderID = $inputArray["order_id"] ?? "";

  // Validate orderID field.
  if (!isPresent($orderID)) {
    // Order id wasn't supplied.
    $formErrors["order_id"] = EFormErrors::EMPTY_FIELD;
  } else if (!isInDatabase($orderID, $databaseLink, "CustomerOrder", "OrderID")) {
    // Order isn't in the database.
    $formErrors["order_id"] = EFormErrors::DOESNT_EXIST;
  } else if (!isRowInDatabase(array("OrderID" => $orderID, "Status" => EOrderStatus::FINISHED), $databaseLink, "CustomerOrder")) {
    // Order is not marked as finished, so we can't collect it yet!
    $formErrors["order_id"] = EFormErrors::NOT_PERMITTED;
  }
  
  // Ensure only logged in cashiers can proceed.
  if (!isEmployeeLoggedIn($sessionArray, STAFF_ROLE_CASHIER)) {
    $formErrors["form_overall"] = EFormErrors::NOT_PERMITTED;
  }

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
  onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return;
  } else {
    // No errors, so update the database

    // Mark the customer's order as collected in the database.
    $collectedStatus = EOrderStatus::COLLECTED;
    $query = (
      "UPDATE CustomerOrder " .
      "SET Status = '$collectedStatus' " .
      "WHERE OrderID = $orderID " .
      "LIMIT 1;"
    );

    $result = $databaseLink->query($query);
    if (empty($databaseLink->error) && $result) {
      // If the outcome was successful, set the form status.
      $wasFormProcessSuccessful = true;
    }
  }

  if ($wasFormProcessSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Cashier Set Order Collected</title>
</head>

<body>
  Please wait while you are redirected...
  <?php
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  onSubmitCashierSetOrderCollected($_GET, $_SESSION);
  ?>
</body>

</html>

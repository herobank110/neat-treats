<?php

/**
 * Webpage for Cashier Landing page.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "staff/database_views.php");

/**
 * Get the cashier details and set the global variable $employeeDetails.
 */
function getCashierLandingPageDetails()
{
  $databaseLink = connectToNeatTreats(STAFF_ROLE_CASHIER, "Password123");
  if ($databaseLink === NULL) {
    return;
  }

  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  global $employeeDetails;
  $employeeDetails = getLandingPageDetails(STAFF_ROLE_CASHIER,
    $databaseLink, $_SESSION);
  if (!isset($employeeDetails)) {
    // Redirect to login if failed to get employee details.
    redirect(HREF_ROOT . "staff/login.php");
  }

  // Close the database connection.
  $databaseLink->close();
}

getCashierLandingPageDetails();
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Cashier Home</title>

  <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href="<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?>" rel="stylesheet" type="text/css" />
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Welcome, <?php echo $employeeDetails["FirstName"]; ?>
          </h1>
          <p class="mdl-typography--subhead">
            Cashier at <?php echo $employeeDetails["BranchName"]; ?>
            since <?php echo date_format(date_create($employeeDetails["StartDate"]), "j F Y"); ?>
          </p>
        </div>

        <div style="margin-left: 20px;">
          <p class="mdl-typography--body-1">
            Choose an option:
          </p>

          <!-- Button to go to check order status page -->
          <a href="<?php echo HREF_ROOT . "staff/cashier/check_order.php"; ?>" class="staff-landing-action mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--raised mdl-button--accent">
            Check order status
          </a>

          <div style="height: 80px"></div>

          <!-- Button to log out -->
          <a href="<?php echo HREF_ROOT . "staff/on_logout.php"; ?>" class="staff-landing-action mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--primary">
            Log Out
          </a>

        </div>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>

<?php

/**
 * Processes an order status change then redirect to staff/baker/orders.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "common/cart_helper.php");
require_once(SITE_ROOT . "staff/database_views.php");

function onSubmitBakerSetOrderStatus($inputArray)
{
  // Declare form local constants.
  /** Url the form came from to send back any errors. */
  $formPageUrl = HREF_ROOT . "staff/baker/orders.php";
  /** Url the form intends to go if successful. */
  $landingPageUrl = HREF_ROOT . "staff/baker/orders.php";
  /** Url to go to if not logged in. */
  $loginUrl = HREF_ROOT . "staff/login.php";

  /** Whether form has terminated with success. */
  $wasFormProcessSuccessful = false;

  $databaseLink = connectToNeatTreats(STAFF_ROLE_BAKER, "Password123");
  $formErrors = array();

  // Sanitize and prepare inputs for validation.
  sanitizeFormInputArray($inputArray);
  // Extract each field as required.
  $orderID = $inputArray["order_id"] ?? "";
  $newStatus = $inputArray["new_status"] ?? "";
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  $isLoggedInAsBaker = isEmployeeLoggedIn($_SESSION, STAFF_ROLE_BAKER);

  // Only bakers are allowed to update orders.
  if (!$isLoggedInAsBaker) {
    onFormFail($inputArray, $loginUrl, $databaseLink);
  }
  $employeeID = $_SESSION["employeeID"];

  // Validate orderID field.
  if (!isPresent($orderID)) {
    $formErrors["order_id"] = EFormErrors::EMPTY_FIELD;
  }  
  
  // Validate newStatus field.
  if (!isPresent($newStatus)) {
    $formErrors["new_status"] = EFormErrors::EMPTY_FIELD;
  } else if (!isValidOption($newStatus, ELookupOptions::ORDER_STATUS)) {
    $formErrors["new_status"] = EFormErrors::NOT_IN_LOOKUP;
  }

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return;
  } else {
    // No errors, so do update order in database

    $query = (
      "UPDATE CustomerOrder " .
      "SET Status = '$newStatus' " .
      "WHERE OrderID = $orderID " .
      "LIMIT 1;");

    $result = $databaseLink->query($query);

    if (empty($databaseLink->error) && $result) {

      switch ($newStatus) {
        case EOrderStatus::STARTED:
          // The order when from unstarted to started.
          // Subtract the ingredients from the database.

          $branch = EmployeeViews::getWorkingBranch($employeeID, $databaseLink);

          if (getConfirmedCart($orderID, $cartItems, $databaseLink)
            && consumeIngredients($cartItems, $branch, $databaseLink)
          ) {
            // Ingredients successfully consumed!
            $wasFormProcessSuccessful = true;
          }
          break;

        case EOrderStatus::FINISHED:
          // The order went from started to finished.
          // Send the customer an email to say their order can be collected.
          // Temporarily log in as the customer user.
          $customerLink = connectToNeatTreats("Customer", "Password123");
          if (
            getCollectOrderEmailData($orderID, $emailData, $customerLink)
            && sendCollectOrderEmail($emailData)
          ) {
            // If the outcome was successful, set the form status.
            $wasFormProcessSuccessful = true;
          }
          $customerLink->close();
          break;
        default:
          // There is no other state that we could be in right now!
          // Leave this case without setting wasFormProcessSuccessful.
          break;
      }
    }
  }

  if ($wasFormProcessSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Baker Set Order Status</title>
</head>

<body>
  Please wait while you are redirected...
<?php onSubmitBakerSetOrderStatus($_GET); ?>
</body>

</html>

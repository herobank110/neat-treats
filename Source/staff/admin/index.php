<?php

/**
 * Webpage for Admin Landing page.
 */

require_once("../../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "staff/database_views.php");


// Initialise global variables so the script can access them.
$formErrors = loadFormErrors();
$lastInput = loadLastFormInput();
$employeeDetails = array();

if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}
switch (AdminViews::initAdminLandingPage($employeeDetails, $_SESSION)) {
  case EInitPageStatus::SUCCESS:
    break;
  case EInitPageStatus::NOT_LOGGED_IN:
    redirect(HREF_ROOT . "staff/login.php");
    break;
  case EInitPageStatus::MAX:
    redirect(HREF_ROOT);
}
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Admin Home</title>

  <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href="<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?>" rel="stylesheet" type="text/css" />
  
  <script src=<?php echo HREF_ROOT . "assets/scripts/cancel_menu.js"; ?>></script>
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Welcome, <?php echo $employeeDetails["FirstName"]; ?>
          </h1>
          <p class="mdl-typography--subhead">
            Admin at <?php echo $employeeDetails["BranchName"]; ?>
            since <?php echo date_format(date_create($employeeDetails["StartDate"]), "j F Y"); ?>
          </p>
        </div>

        <div style="margin-left: 20px;">
          <p class="mdl-typography--body-1">
            Choose an option:
          </p>

          <div style="height: 30px"></div>

          <!-- Delete employee profile -->

          <div style="margin-left: 20px;">
            <p class="mdl-typography--body-1">Delete employee profile</p>
            <!-- Employee ID -->
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "employee_id"); ?>">
              <input class="mdl-textfield__input" id="employee_id" name="employee_id" type="text" title="Enter employee id to delete" pattern="[0-9]*" value="<?php echo getLastInput($lastInput, "employee_id"); ?>">
              <label class="mdl-textfield__label" for="employee_id">Employee ID</label>
              <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "employee_id", "Must be a number"); ?></span>
            </div>

            <!-- "submit" button for form -->
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" onclick="showCancelMenu('cancel-menu__delete-employee');">
              Delete
            </button>
            <div style="display:flex; flex-direction:row; align-items:center;">
              <div style="width:20px;"></div>
              <div class="cancel-menu mdl-typography--body-1" id="cancel-menu__delete-employee">
                <span>Are you sure?</span>
                <a href="#" onclick="window.location.href='<?php echo HREF_ROOT . "staff/manager/on_profile_delete.php"; ?>' + '?employee_id=' + document.getElementById('employee_id').value;">Yes</a>
                <a onclick="hideCancelMenu('cancel-menu__delete-employee');">No</a>
              </div>
            </div>
          </div>

          <div style="height: 30px"></div>

          <!-- Delete customer profile -->

          <div style="margin-left: 20px;">
            <p class="mdl-typography--body-1">Delete customer profile</p>
            <!-- Employee ID -->
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "customer_id"); ?>">
              <input class="mdl-textfield__input" id="customer_id" name="customer_id" type="text" title="Enter employee id to delete" pattern="[0-9]*" value="<?php echo getLastInput($lastInput, "customer_id"); ?>">
              <label class="mdl-textfield__label" for="customer_id">Customer ID</label>
              <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "customer_id", "Must be a number"); ?></span>
            </div>

            <!-- "submit" button for form -->
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary" onclick="showCancelMenu('cancel-menu__delete-customer');">
              Delete
            </button>
            <div style="display:flex; flex-direction:row; align-items:center;">
              <div style="width:20px;"></div>
              <div class="cancel-menu mdl-typography--body-1" id="cancel-menu__delete-customer">
                <span>Are you sure?</span>
                <a href="#" onclick="window.location.href='<?php echo HREF_ROOT . "customer/on_profile_delete.php"; ?>' + '?customer_id=' + document.getElementById('customer_id').value;">Yes</a>
                <a onclick="hideCancelMenu('cancel-menu__delete-customer');">No</a>
              </div>
            </div>
          </div>

          <div style="height: 80px"></div>

          <!-- Button to log out -->
          <a href="<?php echo HREF_ROOT . "staff/on_logout.php"; ?>" class="staff-landing-action mdl-button mdl-js-button mdl-js-ripple-effect mdl-button--primary">
            Log Out
          </a>

        </div>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>
<?php
// Update this path to match the relative path of this page.
require_once("../config.php");

require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/cart_helper.php");

if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Cart</title>

  <!--Use Material Design templates-->

  <!-- Do not use mdc going forward -->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <!-- Use mdl instead -->
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />
  <script src=<?php echo HREF_ROOT . "assets/scripts/cookie_helper.js"; ?>></script>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <script type="text/javascript">
    getCart = () => {
      var cart = getCookie("cart");
      return cart ? JSON.parse(cart) : {};
    };

    setCart = (value) => {
      setCookie("cart", JSON.stringify(value), 30);
    };

    function getCartItemCount() {
      var cart = getCart();
      var sum = 0;
      Object.keys(cart).forEach((cartID) => {
        sum += Number(cart[cartID]);
      });
      return sum;
    }

    function refreshCartItemCount() {
      var newItemCount = getCartItemCount();

      var headerLink = document.getElementById("header-cart-link__text");
      if (headerLink) {
        headerLink.innerHTML = newItemCount == 0 ? "Empty" :
          newItemCount + " Cake" + (newItemCount == 1 ? "" : "s");
      }

      var cartSubtitle = document.getElementById("cart-page-subtitle");
      if (cartSubtitle) {
        cartSubtitle.innerHTML = "Currently " +
          (newItemCount == 0 ? "empty." :
            "contains " + newItemCount + " neat treat" +
            (newItemCount == 1 ? "" : "s") + "!");
      }
    }

    function onQuantityChanged(cakeID, newQuantity) {
      var cartItemCountLimit = 3;
      var cart = getCart();

      if (cart[cakeID] !== undefined) {
        newQuantity = Number(newQuantity);
        var currentQuantity = Number(cart[cakeID]);
        if (currentQuantity !== undefined &&
          getCartItemCount() + (newQuantity - currentQuantity) > cartItemCountLimit) {
          // Attempted to go over the cake limit.
          alert("Sorry, customers can only order up to " + cartItemCountLimit + " items.");
          var quantitySelect = document.getElementById("cart-quantity-select__" + cakeID);
          if (quantitySelect != null) {
            quantitySelect.value = currentQuantity;
          }
          // Stop further processing of the cart.
          return;
        }

        cart[cakeID] = newQuantity;
        setCart(cart);

        // Show updated price in table.
        var unitPrice = document.getElementById("cart-unit-price__" + cakeID).innerHTML;
        var newPrice = "£" + (unitPrice.slice(2, 1000) * newQuantity).toFixed(2);
        document.getElementById("cart-price__" + cakeID).innerHTML = newPrice;
      }

      refreshCartItemCount()
    }

    function onRemoveItem(cakeID) {
      var cart = getCart();
      delete cart[cakeID];
      setCart(cart);

      var oldRow = document.getElementById("cart-table-row__" + cakeID);
      oldRow.remove();

      refreshCartItemCount()
    }
  </script>
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">

        <h2 class="mdl-typography--display-1">
          Your Cart
        </h2>

        <p id="cart-page-subtitle" class="mdl-typography--body-1">
          Current contains 3 neat treats!
        </p>

        <div style="margin: 4em 0 4em 20px;">
          <?php makeCartTable($isEditable = true); ?>
        </div>

        <a class="mdl-button mdl-button--colored mdl-button--raised mdl-button--primary mdl-js-button mdl-js-ripple-effect" style="margin-top: 3em" href=<?php echo HREF_ROOT . "shop/checkout.php" ?>>
          CHECKOUT
        </a>

      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <script type="text/javascript">
    refreshCartItemCount()
  </script>
  <!--endregion -->
</body>

</html>
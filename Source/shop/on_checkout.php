<?php

/**
 * Processes a submitted Customer Checkout Form.
 */

require_once("../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "common/cart_helper.php");


function onSubmitCheckout($inputArray)
{
  // Declare local error message local constants.
  $emptyFieldError = "Please fill in this field";
  $notNumberError = "Only numbers allowed";
  $cardNumberLengthError = "Invalid length of card number";
  $invalidMonthError = "Invalid expiry month";
  $invalidYearError = "Invalid expiry year";
  $cartEmptyError = "An empty cart can't be checked out";
  $cartOverCountLimitError = "Customers can only order up to 3 products";

  // Declare other form local constants.
  $formPageUrl = HREF_ROOT . "shop/checkout.php";
  $landingPageUrl = HREF_ROOT . "shop/invoice.php";
  $cartItemCountLimit = 3;
  $branchID = 1; // The only branch currently.

  $wasOrderSuccessful = false;
  $databaseLink = connectToNeatTreats("Customer", "Password123");
  $formErrors = array();

  // Sanitize and prepare inputs for validation.
  sanitizeFormInputArray($inputArray);
  $cardNumber = $inputArray["card_number"];
  $expiryMonth = $inputArray["expiry_month"] ?? "";
  $expiryYear = $inputArray["expiry_year"];
  $cart = getCart();
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  $customerID = $_SESSION["customerID"] ?? NULL;

  #region validation

  // Only non empty carts can be checked out.
  if ($cart == null) {
    $formErrors["form_overall"] = $cartEmptyError;
  } else {
    if (getCartItemCount() > $cartItemCountLimit) {
      $formErrors["form_overall"] = $cartOverCountLimitError;
    }
  }

  // Only logged in customers can checkout.
  if (!isset($customerID)) {
    $formErrors["form_overall"] = (
      "Please " .
      '<a href="' . HREF_ROOT . "customer/signin.php?tabindex=login" . '">log in</a>' .
      " or " .
      '<a href="' . HREF_ROOT . "customer/signin.php?tabindex=register" . '">register</a>' .
      " an account first");
    onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    // Don't process any further errors.
    return;
  }

  // Validate card number input.
  if (!isPresent($cardNumber)) {
    $formErrors["card_number"] = $emptyFieldError;
  } else {
    if (!isTypeInt($cardNumber)) {
      $formErrors["card_number"] = $notNumberError;
    } else if (!isLengthBetween($cardNumber, 12, 16)) {
      $formErrors["card_number"] = $cardNumberLengthError;
    }
  }

  // Validate expiry month input.
  if (!isPresent($expiryMonth)) {
    $formErrors["expiry_month"] = $emptyFieldError;
  } else {
    if (!isTypeInt($expiryMonth)) {
      $formErrors["expiry_month"] = $invalidMonthError;
    } else if (!isInRange($expiryMonth, 1, 12)) {
      $formErrors["expiry_month"] = $invalidMonthError;
    }
  }

  // Validate expiry year input.
  if (!isPresent($expiryYear)) {
    $formErrors["expiry_year"] = $emptyFieldError;
  } else {
    if (!isTypeInt($expiryYear)) {
      $formErrors["expiry_year"] = $notNumberError;
    } else if (!isInRange($expiryYear, 2019, 3000)) {
      $formErrors["expiry_year"] = $invalidYearError;
    }
  }

  #endregion validation

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return;
  } else {
    // No errors, so add the order to the database.

    // Get next auto_increment index in cart table.
    $databaseLink->query("INSERT INTO Cart(CakeID) VALUES(4);");
    $newCartID = $databaseLink->insert_id;
    // Remove the temporary row.
    $databaseLink->query("DELETE FROM Cart WHERE CartID = $newCartID;");

    // Add items to the database as a cart.
    $wasCartInsertionSuccessful = true;
    foreach ($cart as $cakeID => $quantity) {
      $query = (
        "INSERT INTO Cart " .
        " (CartID, CakeID, Quantity) " .
        "VALUES " .
        " ($newCartID, $cakeID, $quantity);");
      $databaseLink->query($query);
      if (!empty($databaseLink->error)) {
        // An error occurred during the insert operation.
        $wasCartInsertionSuccessful = false;
        break;
      }
    }

    // Add further order details to database.
    $wasOrderInsertionSuccessful = false;
    if ($wasCartInsertionSuccessful) {
      $subTotal = getCartSubtotal($databaseLink);
      $lastCardDigits = substr($cardNumber, -4);
      $query = (
        "INSERT INTO CustomerOrder " .
        " (CustomerID, CartID, BranchID, SubTotal, LastCardDigits) " .
        "VALUES " .
        " ($customerID, $newCartID, $branchID, $subTotal, $lastCardDigits);");

      $databaseLink->query($query);
      if (empty($databaseLink->error)) {
        $newOrderID = $databaseLink->insert_id;
        $wasOrderInsertionSuccessful = true;
      }
    }

    // Clear the cart.
    if ($wasOrderInsertionSuccessful) {
      clearCart();
      $emailResult = $databaseLink->query("SELECT Email FROM Customer WHERE CustomerID = $customerID;");
      if (empty($databaseLink->error)) {
        $email = $emailResult->fetch_assoc()["Email"];

        // Get details about the invoice - use standard invoice function
        // rather than form information so it is consistent with the 
        // invoice page.
        $databaseLink = connectToNeatTreats("Customer", "Password123");
        $invoiceData = array();
        if (!getInvoiceDetails($newOrderID, $invoiceData, $databaseLink)) {
          // Failed to get details for the invoice.
          return false;
        }
        
        if (sendInvoiceEmail($email, $newOrderID, $invoiceData)) {
          // Mark order as successful.
          $wasOrderSuccessful = true;
        }
      }
    }

  }

  if ($wasOrderSuccessful) {
    // Go to the specific invoice page for this new order.
    onFormSuccess($landingPageUrl . "?order_id=$newOrderID", $databaseLink);
    return;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Checkout</title>
</head>

<body>
  Please wait while you are redirected...
  <?php onSubmitCheckout($_POST); ?>
</body>

</html>
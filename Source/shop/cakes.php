<?php
// Update this path to match the relative path of this page.
require_once("../config.php");
require_once(SITE_ROOT . "common/cart_helper.php");

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Cakes</title>

  <!-- Stylesheets and javascripts. -->
  <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <script src=<?php echo HREF_ROOT . "assets/scripts/cookie_helper.js"; ?>></script>

  <script type="text/javascript">
    // Cart functions to refresh cart item count dynamically

    getCart = () => {
      var cart = getCookie("cart");
      return cart ? JSON.parse(cart) : {};
    };

    setCart = (value) => {
      setCookie("cart", JSON.stringify(value), 30);
    };

    function getCartItemCount() {
      var cart = getCart();
      var sum = 0;
      Object.keys(cart).forEach((cartID) => {
        sum += Number(cart[cartID]);
      });
      return sum;
    }

    function refreshCartItemCount() {
      var newItemCount = getCartItemCount();

      var headerLink = document.getElementById("header-cart-link__text");
      if (headerLink) {
        headerLink.innerHTML = newItemCount == 0 ? "Empty" :
          newItemCount + " Cake" + (newItemCount == 1 ? "" : "s");
      }

      var cartSubtitle = document.getElementById("cart-page-subtitle");
      if (cartSubtitle) {
        cartSubtitle.innerHTML = "Currently " +
          (newItemCount == 0 ? "empty." :
            "contains " + newItemCount + " neat treat" +
            (newItemCount == 1 ? "" : "s") + "!");
      }
    }

    function onCakeCardCartActionClicked(cakeID) {

      var cart = getCart();

      if (cart[cakeID] === undefined) {
        // Check if exceeding the cart item limit.
        var cartItemCountLimit = 3;
        var itemCount = getCartItemCount();
        if (itemCount >= cartItemCountLimit) {
          alert("Sorry, customers can only order up to " + cartItemCountLimit + " items.");
          return;
        }
        // Only add if not already in cart.
        cart[cakeID] = 1;
        var btn = document.getElementById("cart-button--" + cakeID);
        btn.innerHTML = "shopping_cart"; // Filled in cart.
        btn.title = "Go to cart";
      } else {
        // Link to cart page.
        window.location.href = '<?php echo HREF_ROOT . "shop/cart.php"; ?>';
      }
      setCart(cart);
      refreshCartItemCount();
    }

    function onSortMethodChanged() {
      var searchForm = document.getElementById("cake-search-form");
      if (searchForm != null) {
        searchForm.submit();
      }
    }
  </script>

</head>

<body>

  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>

  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div style="height: 120px;">
          <div style="float:left; margin-right: 30px;">
            <h1 class="mdl-typography--headline" style="margin-bottom:15px">
              Browse Cakes
            </h1>
            <p class="mdl-typography--subhead">
              Only the neatest of treats
            </p>
          </div>

          <!-- Refinement Bar -->
          <div class="mdc-card mdc-theme--surface-bg mdc-theme--on-secondary" style="min-width:65%; height: 85px; float:left; padding: 10px; margin-top: 20px; display: flow-root">
            <form id="cake-search-form">
              <div class="mdc-text-field mdc-text-field--with-leading-icon" style="float: left; height: 40px; margin: 5px">
                <i class="material-icons mdc-text-field__icon" tabindex="0">sort</i>
                <select class="mdl-textfield__input" style="padding-left:48px" name="sort_method" onchange="onSortMethodChanged();">
                  <?php
                  $sortMethod = $_GET["sort_method"] ?? "";
                  $options = array(
                    "Relevance" => "relevance", "Price (Low-High)" => "price_asc",
                    "Price (High-Low)" => "price_desc", "Alphabetic (A-Z)" => "alpha_asc",
                    "Alphabetic (Z-A)" => "alpha_desc"
                  );
                  foreach ($options as $displayName => $value) {
                    echo '<option value="' . $value . '" '
                      . ($value == $sortMethod ? " selected" : "") .
                      ">$displayName</option>";
                  }
                  ?>
                </select>
              </div>

              <div class="mdc-text-field mdc-text-field--with-leading-icon" style="float:left; height: 40px; width: 200px; margin: 5px; margin-left: 20px;">
                <input type="text" id="search-term-input" name="search_term" class="mdl-textfield__input" style="padding-left: 48px" placeholder="Cake Search" value="<?php echo htmlspecialchars($_GET["search_term"] ?? ""); ?>">
                <i class="material-icons mdc-text-field__icon" tabindex="0" role="button">search</i>
              </div>

              <div style="float:left; height: 40px; margin: 5px; margin-left: 20px;">
                <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" title="Run search"">
                  <i class=" material-icons">search</i>
                </button>

              </div>
            </form>
          </div>
        </div>

        <!-- Cake grid -->
        <ul class="card-grid mdl-list">
          <?php
          populateCakeGrid(
            $_GET["search_term"] ?? "",
            $_GET["sort_method"] ?? ""
          ); ?>
        </ul>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js"; ?>></script>
  <script src=<?php echo HREF_ROOT . "assets/scripts/cake_card_setup.js"; ?>></script>
  <!--endregion -->
</body>

</html>
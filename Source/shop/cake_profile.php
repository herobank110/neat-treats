<?php
// Update this path to match the relative path of this page.
require_once("../config.php");
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Cake Profile</title>
  <!-- Stylesheets and javascripts. -->
  <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <script src=<?php echo HREF_ROOT . "assets/scripts/cookie_helper.js"; ?>></script>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <script type="text/javascript">
    getCart = () => {
      var cart = getCookie("cart");
      return cart ? JSON.parse(cart) : {};
    };

    setCart = (value) => {
      setCookie("cart", JSON.stringify(value), 30);
    };

    function getCartItemCount() {
      var cart = getCart();
      var sum = 0;
      Object.keys(cart).forEach((cartID) => {
        sum += Number(cart[cartID]);
      });
      return sum;
    }

    function refreshCartItemCount() {
      var newItemCount = getCartItemCount();

      var headerLink = document.getElementById("header-cart-link__text");
      if (headerLink) {
        headerLink.innerHTML = newItemCount == 0 ? "Empty" :
          newItemCount + " Cake" + (newItemCount == 1 ? "" : "s");
      }

      var cartSubtitle = document.getElementById("cart-page-subtitle");
      if (cartSubtitle) {
        cartSubtitle.innerHTML = "Currently " +
          (newItemCount == 0 ? "empty." :
            "contains " + newItemCount + " neat treat" +
            (newItemCount == 1 ? "" : "s") + "!");
      }
    }


    function onCartActionClicked(cakeID) {

      var cart = getCart();

      if (cart[cakeID] === undefined) {
        // Check if exceeding the cart item limit.
        var cartItemCountLimit = 3;
        var itemCount = getCartItemCount();
        if (itemCount >= cartItemCountLimit) {
          alert("Sorry, customers can only order up to " + cartItemCountLimit + " items.");
          return;
        }
        // Only add if not already in cart.
        var quantitySelector = document.getElementById("cart-quantity");
        var quantity = quantitySelector.value;
        cart[cakeID] = quantity;
        setCart(cart);
        var btn = document.getElementById("cart-action-button");
        btn.innerHTML = "GO TO CART";
        // Disable quantity selector now cake is added.
        quantitySelector.disabled = true;
        refreshCartItemCount();
      } else {
        // Link to cart page.
        window.location.href = '<?php echo HREF_ROOT . "shop/cart.php"; ?>';
      }
    }
  </script>

  <?php

  function getCakeDetails()
  {
    $cakeID = $_GET["cakeID"];

    if ($cakeID !== null) {
      $databaseLink = connectToNeatTreats("Customer", "Password123");
      if ($databaseLink != null) {
        $result = $databaseLink->query("SELECT Name, Price, IsVegan FROM Cake WHERE CakeID = $cakeID");
        if (empty($databaseLink->error)) {
          $rowData = $result->fetch_assoc();
          return $rowData;
        }
      }
    }
  }

  ?>
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>



  <!-- #region Page Content-->

  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            <?php
            $details = getCakeDetails();
            echo $details["Name"];
            ?>
          </h1>
          <p class="mdl-typography--subhead">
          </p>
        </div>

        <div style="margin-left: 20px; margin-bottom:3em">
          <div class="mdc-elevation--z1" style="background-image:url(&quot; <?php $cakeID = $_GET["cakeID"];
                                                                            echo HREF_ROOT . "assets/images/cake_$cakeID.png"; ?> &quot;); width:300px;height:300px; border-radius: 4px; border: 8px solid white; background-position: top; background-size: cover;">
          </div>


          <h6 class="mdl-typography--subhead">Details</h6>
          <p class="mdl-typography--body-1">Suitable for vegans:
            <span class="mdl-typography--font-bold">
              <?php echo $details["IsVegan"] ? "Yes" : "No"; ?>
            </span>
          </p>
        </div>


        <p class="mdl-typography--subhead">
          Cake Price:
          <span class="mdl-typography--font-bold">
            <?php echo "£" . number_format($details["Price"], 2); ?>
          </span>
        </p>

        <div style="display:flex; flex-direction:row;">

          <div>
            <span class="mdl-typography--body-1">
              Quantity
            </span>

            <select id="cart-quantity" class="mdl-textfield__input" style="width: 3em;" value=1
              <?php
              $cakeID = $_GET["cakeID"];
              $cart = json_decode($_COOKIE["cart"] ?? "", true);
              $isInCart = $cakeID && $cart && isset($cart[$cakeID]) && is_numeric($cart[$cakeID]);

              if ($isInCart) {
                // Don't allow editing quantity if already added.
                echo "disabled";
              }
            ?>
            >
            
              <option selected>1</option>
              <option>2</option>
              <option>3</option>
            </select>
          </div>

          <button id="cart-action-button" class="mdl-button mdl-button--colored mdl-button--raised mdl-button--primary mdl-js-button mdl-js-ripple-effect" style="margin:1em; margin-left: 1em;" onclick="onCartActionClicked(<?php echo $_GET["cakeID"]; ?>)">

            <?php

            if ($isInCart) {
              echo "GO TO CART";
            } else {
              echo "ADD TO CART";
            }
            ?>

          </button>

        </div>

      </div>
    </div>
  </div>

  <!--endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>
<?php
// Update this path to match the relative path of this page.
require_once("../config.php");

require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/cart_helper.php");
require_once(SITE_ROOT . "common/form_helper.php");



if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}

// A customer must be logged in first.
if (!isCustomerLoggedIn($_SESSION)) {
  redirect(SITE_ROOT . "customer/signin.php");
} else {
  // Order id must be specified in url.
  $orderID = $_GET["order_id"] ?? NULL;
  if ($orderID === NULL) {
    // If order id not specified go to home page.
    // Should never happen organically.
    redirect(SITE_ROOT);
  } else {
    // Customer owning the order must be logged in.
    $databaseLink = connectToNeatTreats("Customer", "Password123");
    if (getOrderOwner($orderID, $databaseLink) != $_SESSION["customerID"]) {
      redirect(SITE_ROOT . "customer/customer_profile.php");
    } else {
      // Get full details as global variable, so whole script can access it.
      $invoiceData = array();
      if (!getInvoiceDetails($orderID, $invoiceData, $databaseLink)) {
        // Failed to get data.
        redirect(SITE_ROOT);
      }
    }
  }
}

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Checkout</title>

  <!--Use Material Design templates-->

  <!-- Do not use mdc going forward -->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <!-- Use mdl instead -->
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />
  <script src=<?php echo HREF_ROOT . "assets/scripts/cookie_helper.js"; ?>></script>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  
  <script type="text/javascript">
    getConfirmedCart = () => {
      var cart = getCookie("confirmed_cart");
      return cart ? JSON.parse(cart) : {};
    };

    function getInvoiceSubtotal() {
      var cart = getConfirmedCart();
      var sum = 0;
      Object.keys(cart).forEach((cakeID) => {
        tableData = document.getElementById("cart-price__" + cakeID);
        if (tableData != null) {
          sum += Number(tableData.innerHTML.slice(2, 1000));
        }
      });
      return sum;
    }
  </script>
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Invoice
          </h1>
          <p class="mdl-typography--subhead">
            Thanks for your order
          </p>
        </div>

        <div style="margin-left: 20px;">
          <h2 class="mdl-typography--body-1" style="margin-top: 3em; margin-bottom: 0.5em">
            <?php echo "Order #$orderID at " . date("H:i a", $invoiceData["OrderTime"]) . " on " . date("j F Y", $invoiceData["OrderTime"]); ?>
          </h2>

          <h2 class="mdl-typography--subhead" style="margin-top: 3em; margin-bottom: 0.5em">
            Purchased Items
          </h2>

          <div style="margin: 2em 0 2em 20px;">
            <?php makeInvoiceTable($orderID); ?>
          </div>

          <p class="mdl-typography--body-1" style="margin-left: 20px">
            Total Price:
            <span class="mdl-typography--font-bold">
              <?php echo "£" . number_format($invoiceData["SubTotal"], 2); ?>
            </span>
          </p>

          <h2 class="mdl-typography--subhead">Payment</h2>
          <div style="margin-left: 20px;">
            <p class="mdl-typography--body-1">
              Payment Method:
              <span class="mdl-typography--font-bold">
                Card
              </span>
              <br>
              Card Number:
              <span class="mdl-typography--font-bold">
                <?php echo "xxxxxxxxxxxx$invoiceData[LastCardDigits]" ?>
              </span>
            </p>
          </div>

          <h2 class="mdl-typography--subhead">Collection</h2>
          <div style="margin-left: 20px;">
            <p class="mdl-typography--body-1">              
              Please come the branch specified on the collection day and show your
              order number to the cashier. For further details please read our 
              <a href="<?php echo HREF_ROOT . "info/faq.php";?>">FAQ</a> page.
              <br>
              Branch to collect from:
              <span class="mdl-typography--font-bold">
                <?php echo $invoiceData["BranchName"] ?>
              </span>
              <br>
              Estimated collection date:
              <span class="mdl-typography--font-bold">
                <?php
                echo date("j F Y", $invoiceData["CollectionTime"]);
                ?>
              </span>
            </p>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>
<?php
// Update this path to match the relative path of this page.
require_once("../config.php");

require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/cart_helper.php");
require_once(SITE_ROOT . "common/form_helper.php");

if (session_status() != PHP_SESSION_ACTIVE) {
  session_start();
}

$formErrors = loadFormErrors();
$lastInput = loadLastFormInput();

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Checkout</title>

  <!--Use Material Design templates-->

  <!-- Do not use mdc going forward -->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <!-- Use mdl instead -->
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />
  <script src=<?php echo HREF_ROOT . "assets/scripts/cookie_helper.js"; ?>></script>

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  
  <script type="text/javascript">
    getCart = () => {
      var cart = getCookie("cart");
      return cart ? JSON.parse(cart) : {};
    };

    setCart = (value) => {
      setCookie("cart", JSON.stringify(value), 30);
    };

    function getCartSubtotal() {
      var cart = getCart();
      var sum = 0;
      Object.keys(cart).forEach((cakeID) => {
        tableData = document.getElementById("cart-price__" + cakeID);
        if (tableData != null) {
          sum += Number(tableData.innerHTML.slice(2, 1000));
        }
      });
      return sum;
    }

    function getCartItemCount() {
      var cart = getCart();
      var sum = 0;
      Object.keys(cart).forEach((cartID) => {
        sum += Number(cart[cartID]);
      });
      return sum;
    }

    function refreshCartItemCount() {
      var newItemCount = getCartItemCount();

      var headerLink = document.getElementById("header-cart-link__text");
      if (headerLink) {
        headerLink.innerHTML = newItemCount == 0 ? "Empty" :
          newItemCount + " Cake" + (newItemCount == 1 ? "" : "s");
      }

      var cartSubtitle = document.getElementById("cart-page-subtitle");
      if (cartSubtitle) {
        cartSubtitle.innerHTML = "Currently " +
          (newItemCount == 0 ? "empty." :
            "contains " + newItemCount + " neat treat" +
            (newItemCount == 1 ? "" : "s") + "!");
      }
    }

    function proto_confirmOrder() {
      var cart = getCart();
      if (cart == null || Object.keys(cart).length == 0) {
        // Do not confirm an empty order.
        alert("cart is empty");
        document.getElementById("payment-form").action =
          "<?php echo HREF_ROOT . "shop/cart.php"; ?>";
      }
      // Copy cart into a new cookie for the invoice.
      setCookie("confirmed_cart", JSON.stringify(cart), 30);
      // Clear the cart out.
      setCart({});
    }
  </script>
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">

        <h2 class="mdl-typography--headline">
          Checkout
        </h2>

        <!-- Payment Form section -->
        <form id="payment-form" action="<?php echo HREF_ROOT . "shop/on_checkout.php"; ?>" method="post">

        <span class="mdc-theme--error"><?php echo getErrorMessage($formErrors, "form_overall"); ?></span>

          <h3 class="mdl-typography--subhead">
            Payment Details            
          </h3>
          <div style="margin-left: 20px;">


            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "card_number"); ?>">
              <input class="mdl-textfield__input" type="text" id="card_number" name="card_number" pattern="[0-9]*" title="Enter your card number" value="<?php echo getLastInput($lastInput, "card_number"); ?>">
              <label class="mdl-textfield__label" for="card_number">Card Number</label>
              <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "card_number", "Numbers only"); ?></span>
            </div>

            <br><br>

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "expiry_month"); ?>" style="width: 10em;">
              <select class="mdl-textfield__input" type="text" id="expiry_month" name="expiry_month" pattern="[0-9]*" title="Enter your card expiration month" value="<?php echo getLastInput($lastInput, "expiry_month"); ?>">
                <option disabled selected></option>
                <option value="0" <?php echo getLastSelectedState($lastInput, "expiry_month", "0"); ?>>January</option>
                <option value="1" <?php echo getLastSelectedState($lastInput, "expiry_month", "1"); ?>>February</option>
                <option value="2" <?php echo getLastSelectedState($lastInput, "expiry_month", "2"); ?>>March</option>
                <option value="3" <?php echo getLastSelectedState($lastInput, "expiry_month", "3"); ?>>April</option>
                <option value="4" <?php echo getLastSelectedState($lastInput, "expiry_month", "4"); ?>>May</option>
                <option value="5" <?php echo getLastSelectedState($lastInput, "expiry_month", "5"); ?>>June</option>
                <option value="6" <?php echo getLastSelectedState($lastInput, "expiry_month", "6"); ?>>July</option>
                <option value="7" <?php echo getLastSelectedState($lastInput, "expiry_month", "7"); ?>>August</option>
                <option value="8" <?php echo getLastSelectedState($lastInput, "expiry_month", "8"); ?>>September</option>
                <option value="9" <?php echo getLastSelectedState($lastInput, "expiry_month", "9"); ?>>October</option>
                <option value="10" <?php echo getLastSelectedState($lastInput, "expiry_month", "10"); ?>>November</option>
                <option value="11" <?php echo getLastSelectedState($lastInput, "expiry_month", "11"); ?>>December</option>
              </select>
              <label class="mdl-textfield__label" for="expiry_month">Expiry Month</label>
              <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "expiry_month", "Not a valid month"); ?></span>
            </div>

            <span style="margin-left: 1em"></span>

            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label <?php echo getErrorClass($formErrors, "expiry_year"); ?>" style="width: 10em;">
              <input class="mdl-textfield__input" type="text" id="expiry_year" name="expiry_year" title="Enter your card expiration year" pattern="[0-9]*" min="2019" max="3000" value="<?php echo getLastInput($lastInput, "expiry_year"); ?>">
              <label class="mdl-textfield__label" for="expiry_year">Expiry Year</label>
              <span class="mdl-textfield__error"><?php echo getErrorMessage($formErrors, "expiry_year", "Not a valid year"); ?></span>
            </div>

          </div>

          <!-- Order Confirmation section -->

          <h2 class="mdl-typography--subhead" style="margin-top: 3em; margin-bottom: 0.5em">
            Please confirm your order:
          </h2>


          <div style="margin: 2em 0 2em 20px;">
            <?php makeCartTable($isEditable = false); ?>
          </div>

          <p class="mdl-typography--subhead" style="margin-left: 20px">
            Total Price:
            <span class="mdl-typography--font-bold">
              <script>
                document.write("£" + getCartSubtotal().toFixed(2));
              </script>
            </span>
          </p>

          <button class="mdl-button mdl-button--colored mdl-button--raised mdl-button--primary mdl-js-button mdl-js-ripple-effect" style="margin-left: 1em;">
            CONFIRM
          </button>

        </form>


      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <script type="text/javascript">
    refreshCartItemCount()
  </script>
  <!--endregion -->
</body>

</html>
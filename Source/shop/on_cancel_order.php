<?php

/**
 * Processes a submitted Customer Cancel Order form.
 */

require_once("../config.php");
require_once(SITE_ROOT . "common/db_connect.php");
require_once(SITE_ROOT . "common/form_helper.php");
require_once(SITE_ROOT . "common/cart_helper.php");
require_once(SITE_ROOT . "customer/database_views.php");

function onSubmitCustomerCancelOrder($inputArray, $sessionArray)
{
  // Declare form local constants.
  /** Url the form came from to send back any errors. */
  $formPageUrl = HREF_ROOT . "customer/customer_profile.php?tabindex=orders";
  /** Url the form intends to go if successful. */
  $landingPageUrl = HREF_ROOT . "customer/customer_profile.php?tabindex=orders";

  /** Whether form has terminated with success. */
  $wasFormProcessSuccessful = false;

  $databaseLink = connectToNeatTreats("Customer", "Password123");
  $formErrors = array();

  // Sanitize and prepare inputs for validation.
  sanitizeFormInputArray($inputArray);
  // Extract each field as required.
  $orderID = $inputArray["order_id"] ?? "";

  // Validate orderID field.
  if (!isPresent($orderID)) {
    $formErrors["order_id"] = EFormErrors::EMPTY_FIELD;
  } else if (!isInDatabase($orderID, $databaseLink, "CustomerOrder", "OrderID")) {
    $formErrors["order_id"] = EFormErrors::DOESNT_EXIST;
  }

  // Only allow cancelling an order if the same customer is logged in who
  // made the order.
  $customerID = $sessionArray["customerID"] ?? NULL;
  if ($customerID == NULL || $customerID != getOrderOwner($orderID, $databaseLink)) {
    $formErrors["form_overall"] = EFormErrors::NOT_PERMITTED;
  }

  if (count($formErrors) > 0) {
    // There was at least one error, so send the form back.
    onFormValidationFail($formErrors, $inputArray, $formPageUrl, $databaseLink);
    return;
  } else {
    // No errors, so set cancelled status in database
    if (CustomerViews::cancelOrder($orderID, $databaseLink)) {
      // If the outcome was successful, set the form status.
      $wasFormProcessSuccessful = true;
    }
  }

  if ($wasFormProcessSuccessful) {
    onFormSuccess($landingPageUrl, $databaseLink);
    return;
  } else {
    onFormFail($inputArray, $formPageUrl, $databaseLink);
    return;
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Neat Treats - Customer Cancel Order</title>
</head>

<body>
  Please wait while you are redirected...
  <?php
  if (session_status() != PHP_SESSION_ACTIVE) {
    session_start();
  }
  onSubmitCustomerCancelOrder($_GET, $_SESSION);
  ?>
</body>

</html>
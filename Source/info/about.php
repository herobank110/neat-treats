<?php
// Update this path to match the relative path of this page.
require_once("../config.php");
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - About Us</title>
  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>



  <!-- #region Page Content-->

  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div>
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            About Us
          </h1>
          <p class="mdl-typography--subhead">
            Learn more about our website and company
          </p>
        </div>


        <div>
          <h3 class="mdl-typography--subhead">
            Neat Treats
          </h3>
          <div style="margin-left: 20px;">
            <p class="mdl-typography--body-1">
              We are a local Cardiff based bakery with a small dedicated team
              who specialises in birthday cakes. We cater to a wide variety of
              birthdays, including children, adults and seniors.
            </p>
          </div>
        </div>

        <div>
          <p class="mdl-typography--subhead">
            Our Team
          </p>

          <div style="margin-left: 20px;">

            <p class="mdl-typography--body-1">
              We are a small and dedicated team of three bakers, a cashier and a manager.
              Whether we are making cakes or managing a business, we always put our best
              effort into everything we do.
              When our team assembled for the first time in 2017, we had a shared passion
              for baking cakes and we wanted to share it with the world.
            </p>

            <img style="max-width:450px;" src="<?php echo HREF_ROOT . "assets/images/grand_opening.png"; ?>" alt="Cutting ribbon at store opening"></img>
            <p class="mdl-typography--caption" style="margin: 0.5em 1em;">
              Neat Treats Cardiff Grand Opening - March 2017
            </p>

            <p class="mdl-typography--body-1">
              After perfecting our recipes and gathering experience, we decided to set up
              our own shop in Cardiff. The only question was, what would our brand be called?
              Based on our unique baking style of prioritising the neatness of our cakes,
              Neat Treats became a perfect fit.
              Once word had spread of our neat products, we grew into one of Cardiff's most
              loved cake bakeries.
            </p>


          </div>
        </div>

        <div>
          <p class="mdl-typography--subhead">
            Company Style
          </p>

          <div style="margin-left: 20px;">
            <p class="mdl-typography--body-1">
              Our mission is to make every celebration cake perfect. If a cake doesn't
              reach our standards, it simply does not leave the bakery. This requires us
              to maintain a professional and high quality level of service. Our values are
              to always put the customer first, manufacture our cakes with love and care,
              and keep the environment tidy and neat.
            </p>
          </div>
        </div>

        <div>
          <p class="mdl-typography--subhead">
            Our Site
          </p>

          <div style="margin-left: 20px;">
            <p class="mdl-typography--body-1">
              In order to fulfil our company mission, we must do everything we
              can to ensure the cake gets made on time. This is why we have made
              a website so that customers can order in advance and collect on
              the day. The benefit is the cake will still be fresh and you can
              be sure it will be ready on time.
            </p>
            <p class="mdl-typography--body-1">
              With our new website, our customers can access our entire cake
              repertoire from the comfort of their homes and using our ordering
              service, finding the perfect cake is easier than ever. The features
              our website offers are sustained communication with our customers
              about their orders, login functionality and profile management,
              cart operations, and more. To learn more about our service,
              our
              <a href="<?php echo HREF_ROOT . "info/faq.php"; ?>">Frequently Asked Questions</a>
              page is available to give you the best shopping experience.
            </p>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!--endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>
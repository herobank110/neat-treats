<?php
// Update this path to match the relative path of this page.
require_once("../config.php");
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Privacy Policy</title>

  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Privacy Policy
          </h1>
          <p class="mdl-typography--subhead">

          </p>
        </div>

        <div style="margin-left: 20px;">
          <p class="mdl-typography--body-1">
            We are always concerned about the privacy of our customers.
            This is why we store all data on a secure server. We never
            sell any sensitive information about you or your purchases
            to advertisers.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>

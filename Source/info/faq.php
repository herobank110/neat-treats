<?php
// Update this path to match the relative path of this page.
require_once("../config.php");
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - FAQ</title>

  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="min-height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Frequently Asked Questions
          </h1>
          <p class="mdl-typography--subhead">

          </p>
        </div>

        <div style="margin-left: 20px;">
          <p class="mdl-typography--body-1">
            We always aim to provide a simple and helpful cake ordering
            solution to our customers. Please read our answers below to
            ensure you feel safe using our system. We take great care to
            protect our customers against being uncertain of how they will
            be using our website. If you have any further questions, please
            get in touch with us at
            <a href=<?php echo HREF_ROOT . "info/contact.php"; ?>>Contact Us</a>.
          </p>
        </div>

        <div class="faq-question">
          <span class="faq-question__title mdl-typography--subhead">
            <i class="faq-question__icon material-icons">keyboard_arrow_down</i>
            How do I order cakes?
          </span>

          <div class="faq-question__answer">
            Browse our selection of cakes, add between 1 and 3 cakes to your cart,
            click checkout, enter our payment details and finally confirm your order.
          </div>
        </div>

        <div class="faq-question">
          <span class="faq-question__title mdl-typography--subhead">
            <i class="faq-question__icon material-icons">keyboard_arrow_down</i>
            How will I receive my order?
          </span>

          <div class="faq-question__answer mdl-animation--default">
            You can collect your order when it is ready from the branch stated
            in your order invoice. We send an email to notify you when your order
            is ready. We aim to fulfil orders within 1-2 working days after the
            order is confirmed.
          </div>
        </div>

        <div class="faq-question">
          <span class="faq-question__title mdl-typography--subhead">
            <i class="faq-question__icon material-icons">keyboard_arrow_down</i>
            How do I see my orders?
          </span>

          <div class="faq-question__answer mdl-animation--default">
            Log in to your account <a href="<?php echo HREF_ROOT . "customer/signin.php"?>">here</a>
            and access the <b>Orders</b> tab to see your orders. Click <b>Details</b> to access
            the full details about an order.
          </div>
        </div>

        <div class="faq-question">
          <span class="faq-question__title mdl-typography--subhead">
            <i class="faq-question__icon material-icons">keyboard_arrow_down</i>
            Can I cancel an order?
          </span>

          <div class="faq-question__answer">
            Of course you can cancel an order. Simply go to
            <a href="<?php echo HREF_ROOT . "customer/customer_profile.php"?>">your account</a>
            , find the order you wish you to cancel, click <b>More</b> and <b>Cancel</b> to cancel
            the order. Please note that you can only cancel orders that are awaiting baking or
            have not finished baking yet. You will receive a full refund if the baking has not
            been started yet.
          </div>
        </div>

        <div class="faq-question">
          <span class="faq-question__title mdl-typography--subhead">
            <i class="faq-question__icon material-icons">keyboard_arrow_down</i>
            Are the cakes vegan-friendly?
          </span>

          <div class="faq-question__answer mdl-animation--default">
            Some of our cakes are suitable for vegan consumption. They are marked
            with a <strong>Vegan</strong> badge on the cake browsing page and
            show their <b>Vegan Friendly</b> status on the details page.
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>

  <script type="text/javascript">
    const faqQuestion = document.querySelectorAll(".faq-question");
    faqQuestion.forEach(question => {
      var title = question.querySelector(".faq-question__title");
      var answer = question.querySelector(".faq-question__answer");
      if (title != null && answer != null) {
        // Initially hide the answers.
        answer.style.display = "none";

        title.addEventListener("click", () => {
          if (question.classList.contains("opened")) {
            question.classList.remove("opened");
            setTimeout(() => {
              answer.style.display = "none";
            }, 200);
          } else {
            answer.style.display = "block";
            setTimeout(() => {
              question.classList.add("opened")
            }, 50);
          }
        });
      }
    });
  </script>
  <!--endregion -->
</body>

</html>
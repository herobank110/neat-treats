<?php
// Update this path to match the relative path of this page.
require_once("../config.php");
?>

<!DOCTYPE HTML>
<html>

<head>
    <title>Neat Treats - Contact Us</title>

    <!--Use Material Design templates-->

    <!-- Do not use mdc going forward -->
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <!-- Use mdl instead -->
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <!-- Material icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--Override elements with custom theme-->
    <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />
</head>

<body>
    <!--Navigation Header-->
    <?php include SITE_ROOT . "common/nav_header.php" ?>


    <!-- #region Page Content -->
    <div class="main-container mdc-card">
        <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
            <div style="height:500px;">
                <div class="content-headline-container">
                    <h1 class="mdl-typography--headline">
                        Contact Us
                    </h1>
                    <p class="mdl-typography--subhead">
                        Please do not hesitate to contact us
                    </p>
                </div>

                <table class="contact-table">
                    <tbody>
                        <tr class="contact-table__row">
                            <td class="mdl-typography--body-1"><strong>Email</strong></td>
                            <td class="mdl-typography--body-1">customer-support@neat-treats.com</td>
                        </tr>
                        <tr class="contact-table__row">
                            <td class="mdl-typography--body-1"><strong>In Store</strong></td>
                            <td class="mdl-typography--body-1"> Branch Name<br>
                                30 Bakers Row<br>
                                Cardiff<br>
                                CF10 1AL
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- endregion -->


    <!--Navigation Footer-->
    <?php include SITE_ROOT . "common/nav_footer.php" ?>

    <!--#region Scripts-->
    <!--Setup material design interactive components.-->
    <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
    <!--endregion -->
</body>

</html>
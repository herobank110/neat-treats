<?php
// Update this path to match the relative path of this page.
require_once("../config.php");
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Neat Treats - Terms and Conditions</title>

  <!-- Stylesheets and javascripts. -->
    <!--Use Material Design templates-->
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
  <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.orange-red.min.css" />
  <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  <!-- Material icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <!--Override elements with custom theme-->
  <link href=<?php echo HREF_ROOT . "assets/styles/neat_treats_main_style.css"; ?> rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
  <!--Navigation Header-->
  <?php include SITE_ROOT . "common/nav_header.php" ?>


  <!-- #region Page Content -->
  <div class="main-container mdc-card">
    <div class="inner-content-central main-container-inner mdc-card__media mdc-elevation--z1">
      <div style="height:500px;">
        <div class="content-headline-container">
          <h1 class="mdl-typography--headline">
            Terms and Conditions
          </h1>
          <p class="mdl-typography--subhead">

          </p>
        </div>
        <div style="margin-left: 20px;">
          <p class="mdl-typography--body-1">
            To use our online cake ordering service, you must agree with
            the following terms and conditions:
          </p>
        </div>
        <ol>
          <li>
            Any orders you fail to collect within the specified time of
            2 days after the completion or else the order will be immediately
            dropped with no refund.
          </li>
          <li>
            After you place an order, you may cancel it at any time before
            collecting it. We will issue a full refund in the case that no
            ingredients have been consumed by the time of cancellation for
            the order. The refund will be made to the same payment method
            that was used to make the order. However, if the order was in
            the process of being made by our bakers at the time of cancellation,
            we reserve the right to charge you, the customer, to the full
            extent of the order.
          </li>
        </ol>
      </div>
    </div>
  </div>
  <!-- endregion -->


  <!--Navigation Footer-->
  <?php include SITE_ROOT . "common/nav_footer.php" ?>

  <!--#region Scripts-->
  <!--Setup material design interactive components.-->
  <script src=<?php echo HREF_ROOT . "assets/scripts/mdc_page_setup.js" ?>></script>
  <!--endregion -->
</body>

</html>
